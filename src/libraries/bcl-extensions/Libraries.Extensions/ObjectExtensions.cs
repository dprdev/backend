﻿using System.Globalization;

namespace System
{
    public static class ObjectExtensions
    {
        public static string ToLowerString(this object value)
        {
            if (value is null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            return value.ToString().ToLower();
        }

        public static string ToLowerString(this object value, CultureInfo culture)
        {
            if (value is null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            if (culture is null)
            {
                throw new ArgumentNullException(nameof(culture));
            }

            return value.ToString().ToLower(culture);
        }
    }
}
