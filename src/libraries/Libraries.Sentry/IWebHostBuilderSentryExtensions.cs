﻿namespace Microsoft.AspNetCore.Hosting
{
    public static class IWebHostBuilderSentryExtensions
    {
        public static bool CanUseSentry(this IWebHostBuilder builder)
        {
            return builder.GetSetting("ASPNETCORE_USESENTRY")?.ToLower() == "true"; 
        }
    }
}
