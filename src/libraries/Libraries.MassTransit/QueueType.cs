﻿namespace Libraries.MassTransit
{
    public enum QueueType
    {
        RabbitMQ,
        AzureServiceBus,
    }
}
