﻿using System;
using System.Collections.Generic;

using Libraries.MassTransit;

using MassTransit;
using MassTransit.ExtensionsDependencyInjectionIntegration;

using Microsoft.Extensions.Configuration;

namespace Microsoft.AspNetCore.Hosting
{
    public static class IWebHostBuilderMassTransitExtensions
    {
        public static IWebHostBuilder UseMassTransit(
            this IWebHostBuilder builder,
            Action<IServiceCollectionBusConfigurator> mtCfg,
            Dictionary<QueueType, IBusConfiguration> cfg)
        {
            if (builder is null)
            {
                throw new ArgumentNullException(nameof(builder));
            }

            if (cfg is null)
            {
                throw new ArgumentNullException(nameof(cfg));
            }

            builder.ConfigureServices((host, services) =>
            {
                string rawType = host.Configuration.GetValue<string>("queueType");
                if (string.IsNullOrEmpty(rawType)
                    || !Enum.TryParse(value: rawType, ignoreCase: true, result: out QueueType value))
                {
                    throw new InvalidOperationException($"Unknow queue type: {rawType}");
                }

                if (!cfg.TryGetValue(value, out IBusConfiguration configurator))
                {
                    throw new InvalidOperationException($"Configurator does not registered for queue {value}");
                }

                services.AddMassTransitHostedService();
                services.AddMassTransit((mtCfg) =>
                {
                    configurator.Configure(new()
                    {
                        Cfg = host.Configuration,
                        Env = host.HostingEnvironment,
                        Services = services,
                        MtCfg = mtCfg,
                    });
                });
            });
            return builder;
        }
    }
}
