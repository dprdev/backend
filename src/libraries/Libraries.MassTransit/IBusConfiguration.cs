﻿using MassTransit.ExtensionsDependencyInjectionIntegration;

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Libraries.MassTransit
{
    public class RabbitSettings
    {
        public string Host { get; set; }
        public ushort Port { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
    }

    public record BusConfigOptions
    {
        public IConfiguration Cfg { get; init; }
        public IWebHostEnvironment Env { get; init; }
        public IServiceCollectionBusConfigurator MtCfg { get; init; }
        public IServiceCollection Services { get; init; }
    }

    public interface IBusConfiguration
    {
        void Configure(BusConfigOptions options);
    }
}
