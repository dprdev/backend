﻿namespace Microsoft.AspNetCore.Mvc
{
    [ApiController]
    public class ApiController : ControllerBase
    {
        /// <summary>
        /// Creates a <see cref="JsonResult"/> object that serializes the specified <paramref name="data"/> object
        /// to JSON.
        /// </summary>
        /// <param name="data">The object to serialize.</param>
        /// <returns>The created <see cref="JsonResult"/> that serializes the specified <paramref name="data"/>
        /// to JSON format for the response.</returns>
        [NonAction]
        public virtual JsonResult Json(object data)
        {
            return new JsonResult(data);
        }

        [NonAction]
        public virtual JsonResult Json(object data, object serializerSettings)
        {
            return new JsonResult(data, serializerSettings);
        }
    }
}
