﻿namespace Microsoft.AspNetCore.Mvc
{
    public class ApiVerV1Attribute : ApiVersionAttribute
    {
        public ApiVerV1Attribute() : base("1.0")
        {

        }
    }
}
