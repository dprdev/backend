﻿namespace Microsoft.AspNetCore.Mvc
{
    public class ApiVerV3Attribute : ApiVersionAttribute
    {
        public ApiVerV3Attribute() : base("3.0")
        {

        }
    }
}
