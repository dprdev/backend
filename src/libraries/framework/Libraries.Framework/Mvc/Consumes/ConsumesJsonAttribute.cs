﻿using System.Net.Mime;

namespace Microsoft.AspNetCore.Mvc
{
    public sealed class ConsumesJsonAttribute : ConsumesAttribute
    {
        public ConsumesJsonAttribute() : base(MediaTypeNames.Application.Json)
        {

        }
    }
}
