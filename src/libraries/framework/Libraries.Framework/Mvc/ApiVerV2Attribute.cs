﻿namespace Microsoft.AspNetCore.Mvc
{
    public class ApiVerV2Attribute : ApiVersionAttribute
    {
        public ApiVerV2Attribute() : base("2.0")
        {

        }
    }
}
