﻿using System;
using System.Collections.Generic;

using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace Microsoft.AspNetCore.Http
{
    public static class HttpContextExtensions
    {
        #region Service collection extensions
        public static IServiceScope CreateScope(this HttpContext context)
        {
            return context.RequestServices.CreateScope();
        }

        public static object GetRequiredService(this HttpContext context, Type serviceType)
        {
            return context.RequestServices.GetRequiredService(serviceType);
        }

        public static T GetRequiredService<T>(this HttpContext context) where T : notnull
        {
            return context.RequestServices.GetRequiredService<T>();
        }

        public static IEnumerable<object> GetServices(this HttpContext context, Type serviceType)
        {
            return context.RequestServices.GetServices(serviceType);
        }

        public static IEnumerable<T> GetServices<T>(this HttpContext context)
        {
            return context.RequestServices.GetServices<T>();
        }

        public static T GetService<T>(this HttpContext context)
        {
            return context.RequestServices.GetService<T>();
        }
        #endregion
    }
}
