﻿
using System;

using Microsoft.Extensions.Configuration;

namespace Microsoft.Extensions.DependencyInjection
{
    internal static class ConfigurationExtensions
    {
        public static string GetAuthority(this IConfiguration configuration, string authority = null)
        {
            string value = configuration.GetValue<string>(authority ?? "identityUrl");
            if (string.IsNullOrEmpty(value))
            {
                throw new InvalidOperationException("Authority is undefined");
            }

            return value;
        }
    }
}
