﻿
using Microsoft.AspNetCore.Authorization;

namespace Microsoft.AspNetCore.Authentication.JwtBearer
{
    public sealed class JwtAuthorizeAttribute : AuthorizeAttribute
    {
        public JwtAuthorizeAttribute()
        {
            AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme;
        }
    }
}
