﻿
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class AuthenticationServicesCollectionExtensions
    {
        public static AuthenticationBuilder AddJwtAuthentication(
            this IServiceCollection services, string authority = null)
        {
            AuthenticationBuilder builder = services
                .AddAuthentication(JwtBearerDefaults.AuthenticationScheme);

            IConfiguration cfg =
                builder.Services.BuildServiceProvider()
                                .GetRequiredService<IConfiguration>();

            builder.AddJwtBearer(
                JwtBearerDefaults.AuthenticationScheme,
                jwtCfg =>
                {
                    jwtCfg.Authority = cfg.GetAuthority(authority);

                    jwtCfg.RequireHttpsMetadata = false;
                    jwtCfg.TokenValidationParameters = new()
                    {
                        ValidateAudience = false
                    };
                });

            return builder;
        }
    }
}
