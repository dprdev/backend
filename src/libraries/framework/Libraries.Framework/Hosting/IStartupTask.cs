﻿using System.Threading.Tasks;

namespace Microsoft.Extensions.Hosting
{
    public interface IStartupTask
    {
        Task ExecuteAsync();
    }
}
