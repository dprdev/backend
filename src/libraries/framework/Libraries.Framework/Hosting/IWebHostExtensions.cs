﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Microsoft.Extensions.Hosting
{
    public static class IWebHostExtensions
    {
        public static IServiceScope CreateServiceScope(this IHost host)
        {
            return host.Services.CreateScope();
        }

        public static async Task<IHost> ExecuteStartupTask(this IHost host, bool breakOnFailure = true, params Assembly[] assemblies)
        {
            ILogger logger = host.Services
                .GetRequiredService<ILoggerFactory>()
                .CreateLogger(nameof(IWebHostExtensions));

            IEnumerable<Type> tasks =
                assemblies.SelectMany(e => e.GetTypes())
                          .Where(e =>
                          {
                              return e.IsClass && !e.IsAbstract && e.IsAssignableTo(typeof(IStartupTask));
                          })
                          .ToList();

            logger.LogInformation($"Collected startup tasks: {tasks.Count()}");

            using (IServiceScope scope = host.Services.CreateScope())
            {
                foreach (Type type in tasks)
                {
                    logger.LogInformation($"Execute startup task: {type.FullName}");
                    try
                    {
                        await ((IStartupTask)ActivatorUtilities.CreateInstance(scope.ServiceProvider, type)).ExecuteAsync();
                        logger.LogInformation("Execution completed");
                    }
                    catch (Exception ex)
                    {
                        logger.LogError(ex, "Task execution failed");
                        if (breakOnFailure)
                        {
                            throw;
                        }
                    }
                }
            }

            logger.LogInformation("All startup tasks executed");

            return host;
        }

        public static async Task StartAsync(this Task<IHost> hostTask)
        {
            await hostTask.ContinueWith(task =>
            {
                if (task.Exception is not null)
                {
                    return Task.FromException(task.Exception);
                }

                return task.Result.StartAsync();
            });
        }
    }
}
