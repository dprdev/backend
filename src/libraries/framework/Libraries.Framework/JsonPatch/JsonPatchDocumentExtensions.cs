﻿using System;
using System.Reflection;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Microsoft.AspNetCore.JsonPatch
{
    public static class JsonPatchDocumentExtensions
    {
        public static object ApplyToPrivateSetters(
            this JsonPatchDocument doc, object target, Action<JsonPatchError> logErrorAction = null)
        {
            var copy = new JsonPatchDocument(doc.Operations, CustomContractResolver.Instance);
            if (logErrorAction is not null)
            {
                copy.ApplyTo(target, logErrorAction);
            }
            else
            {
                copy.ApplyTo(target);
            }

            return target;
        }

        public static TModel ApplyToPrivateSetters<TModel>(
            this JsonPatchDocument<TModel> doc, TModel target, Action<JsonPatchError> logErrorAction = null) where TModel : class
        {
            var copy = new JsonPatchDocument<TModel>(doc.Operations, CustomContractResolver.Instance);
            if (logErrorAction is not null)
            {
                copy.ApplyTo(target, logErrorAction);
            }
            else
            {
                copy.ApplyTo(target);
            }

            return target;
        }

        private sealed class CustomContractResolver : DefaultContractResolver
        {
            protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
            {
                JsonProperty jsonProperty = base.CreateProperty(member, memberSerialization);
                if (!jsonProperty.Writable)
                {
                    if (member is PropertyInfo propertyInfo)
                    {
                        jsonProperty.Writable = propertyInfo.GetSetMethod(true) != null;
                    }
                }

                return jsonProperty;
            }


            public static DefaultContractResolver Instance { get; } = new CustomContractResolver();
        }
    }
}
