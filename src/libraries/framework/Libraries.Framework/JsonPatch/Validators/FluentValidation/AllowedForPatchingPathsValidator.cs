﻿
using FluentValidation;

namespace Microsoft.AspNetCore.JsonPatch.Validators.FluentValidation
{
    public class AllowedForPatchingPathsValidator : AbstractValidator<JsonPatchDocument>
    {
        public AllowedForPatchingPathsValidator(string[] paths)
        {
            RuleForEach(e => e.Operations)
               .SetValidator(new OperationValidator(paths));
        }
    }
}
