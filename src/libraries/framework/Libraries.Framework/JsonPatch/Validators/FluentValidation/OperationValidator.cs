﻿
using System.Linq;
using System.Text.RegularExpressions;

using FluentValidation;

using Microsoft.AspNetCore.JsonPatch.Operations;

namespace Microsoft.AspNetCore.JsonPatch.Validators.FluentValidation
{
    internal sealed class OperationValidator : AbstractValidator<OperationBase>
    {
        public OperationValidator(string[] paths)
        {
            RuleFor(e => e.path).Custom((path, ctx) =>
            {
                if (!paths.Any(p => Regex.IsMatch(path, p, RegexOptions.IgnoreCase)))
                {
                    ctx.AddFailure(path, "Property does not allowed for change");
                }
            });
        }
    }
}
