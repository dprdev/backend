﻿
using FluentValidation;

namespace Microsoft.AspNetCore.JsonPatch.Validators.FluentValidation
{
    public abstract class AllowedForPatchingPathsValidator<T>
        : AbstractValidator<JsonPatchDocument<T>> where T : class
    {
        public AllowedForPatchingPathsValidator(string[] paths)
        {
            RuleForEach(e => e.Operations)
               .SetValidator(new OperationValidator(paths));
        }
    }
}
