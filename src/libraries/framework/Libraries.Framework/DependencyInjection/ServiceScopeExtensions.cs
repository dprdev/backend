﻿using System;
using System.Collections.Generic;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceScopeExtensions
    {
        public static IServiceScope CreateScope(this IServiceScope scope)
        {
            return scope.ServiceProvider.CreateScope();
        }

        public static object GetRequiredService(this IServiceScope scope, Type serviceType)
        {
            return scope.ServiceProvider.GetRequiredService(serviceType);
        }

        public static T GetRequiredService<T>(this IServiceScope scope) where T : notnull
        {
            return scope.ServiceProvider.GetRequiredService<T>();
        }

        public static IEnumerable<object> GetServices(this IServiceScope scope, Type serviceType)
        {
            return scope.ServiceProvider.GetServices(serviceType);
        }

        public static IEnumerable<T> GetServices<T>(this IServiceScope scope)
        {
            return scope.ServiceProvider.GetServices<T>();
        }

        public static T GetService<T>(this IServiceScope scope)
        {
            return scope.ServiceProvider.GetService<T>();
        }
    }
}
