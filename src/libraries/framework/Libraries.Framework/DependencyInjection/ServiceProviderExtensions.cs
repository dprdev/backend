﻿using System;

using Microsoft.Extensions.Logging;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceProviderExtensions
    {
        public static ILogger GetLogger(this IServiceProvider services)
        {
            return services.GetRequiredService<ILogger>();
        }

        public static ILogger<T> GetLogger<T>(this IServiceProvider services)
        {
            return services.GetRequiredService<ILogger<T>>();
        }
    }
}
