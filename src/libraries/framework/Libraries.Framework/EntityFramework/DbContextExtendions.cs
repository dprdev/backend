﻿using System.Threading.Tasks;

namespace Microsoft.EntityFrameworkCore
{
    public static class DbContextExtendions
    {
        public static Task<int> AddWithSaveChanges<T>(this DbContext context, T value) where T : class
        {
            context.Add(value);
            return context.SaveChangesAsync();
        }
    }
}
