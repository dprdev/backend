﻿using System;
using System.Collections.Generic;
using System.Reflection;

using FluentValidation;
using FluentValidation.AspNetCore;

namespace Microsoft.Extensions.DependencyInjection
{
    public sealed class CustomFluentValidationMvcConfiguration
    {
        private readonly FluentValidationMvcConfiguration _cfg;

        public CustomFluentValidationMvcConfiguration(
            FluentValidationMvcConfiguration cfg)
        {
            _cfg = cfg ?? throw new ArgumentNullException(nameof(cfg));
        }


        //
        // Summary:
        //     Options that are used to configure all validators.
        public ValidatorConfiguration ValidatorOptions => _cfg.ValidatorOptions;

        //
        // Summary:
        //     The type of validator factory to use. Uses the ServiceProviderValidatorFactory
        //     by default.
        public Type ValidatorFactoryType
        {
            get => _cfg.ValidatorFactoryType;
            set => _cfg.ValidatorFactoryType = value;
        }

        //
        // Summary:
        //     The validator factory to use. Uses the ServiceProviderValidatorFactory by default.
        public IValidatorFactory ValidatorFactory
        {
            get => _cfg.ValidatorFactory;
            set => _cfg.ValidatorFactory = value;
        }

        //
        // Summary:
        //     Whether to run MVC's default validation process (including DataAnnotations) after
        //     FluentValidation is executed. True by default.
        public bool RunDefaultMvcValidationAfterFluentValidationExecutes
        {
            get => _cfg.RunDefaultMvcValidationAfterFluentValidationExecutes;
            set => _cfg.RunDefaultMvcValidationAfterFluentValidationExecutes = value;
        }


        //
        // Summary:
        //     Enables or disables localization support within FluentValidation
        public bool LocalizationEnabled
        {
            get => ValidatorOptions.LanguageManager.Enabled;
            set => ValidatorOptions.LanguageManager.Enabled = value;
        }

        //
        // Summary:
        //     Whether or not child properties should be implicitly validated if a matching
        //     validator can be found. By default this is false, and you should wire up child
        //     validators using SetValidator.
        public bool ImplicitlyValidateChildProperties
        {
            get => _cfg.ImplicitlyValidateChildProperties;
            set => _cfg.ImplicitlyValidateChildProperties = value;
        }

        //
        // Summary:
        //     Gets or sets a value indicating whether the elements of a root model should be
        //     implicitly validated when the root model is a collection type and a matching
        //     validator can be found for the element type. By default this is false, and you
        //     will need to create a validator for the collection type (unless FluentValidation.AspNetCore.FluentValidationMvcConfiguration.ImplicitlyValidateChildProperties
        //     is true.
        public bool ImplicitlyValidateRootCollectionElements
        {
            get => _cfg.ImplicitlyValidateRootCollectionElements;
            set => _cfg.ImplicitlyValidateRootCollectionElements = true;
        }

        //
        // Summary:
        //     Registers all validators derived from AbstractValidator within the assembly containing
        //     the specified type
        //
        // Parameters:
        //   filter:
        //     Optional filter that allows certain types to be skipped from registration.
        //
        //   lifetime:
        //     The service lifetime that should be used for the validator registration. Defaults
        //     to Scoped
        public CustomFluentValidationMvcConfiguration RegisterValidatorsFromAssemblyContaining<T>(
            Func<AssemblyScanner.AssemblyScanResult, bool> filter = null,
            ServiceLifetime lifetime = ServiceLifetime.Scoped)
        {
            _cfg.RegisterValidatorsFromAssemblyContaining<T>(filter, lifetime);
            return this;
        }

        //
        // Summary:
        //     Registers all validators derived from AbstractValidator within the assembly containing
        //     the specified type
        //
        // Parameters:
        //   type:
        //     The type that indicates which assembly that should be scanned
        //
        //   filter:
        //     Optional filter that allows certain types to be skipped from registration.
        //
        //   lifetime:
        //     The service lifetime that should be used for the validator registration. Defaults
        //     to Scoped
        public CustomFluentValidationMvcConfiguration RegisterValidatorsFromAssemblyContaining(Type type, Func<AssemblyScanner.AssemblyScanResult, bool> filter = null, ServiceLifetime lifetime = ServiceLifetime.Scoped)
        {
            _cfg.RegisterValidatorsFromAssemblyContaining(type, filter, lifetime);
            return this;
        }

        //
        // Summary:
        //     Registers all validators derived from AbstractValidator within the specified
        //     assembly
        //
        // Parameters:
        //   assembly:
        //     The assembly to scan
        //
        //   filter:
        //     Optional filter that allows certain types to be skipped from registration.
        //
        //   lifetime:
        //     The service lifetime that should be used for the validator registration. Defaults
        //     to Scoped
        public CustomFluentValidationMvcConfiguration RegisterValidatorsFromAssembly(Assembly assembly, Func<AssemblyScanner.AssemblyScanResult, bool> filter = null, ServiceLifetime lifetime = ServiceLifetime.Scoped)
        {
            _cfg.RegisterValidatorsFromAssembly(assembly, filter, lifetime);
            return this;
        }

        public CustomFluentValidationMvcConfiguration RegisterValidatorsFromAssemblies(IEnumerable<Assembly> assemblies, Func<AssemblyScanner.AssemblyScanResult, bool> filter = null, ServiceLifetime lifetime = ServiceLifetime.Scoped)
        {
            _cfg.RegisterValidatorsFromAssemblies(assemblies, filter, lifetime);
            return this;
        }

        //
        // Summary:
        //     Configures clientside validation support
        //
        // Parameters:
        //   clientsideConfig:
        //
        //   enabled:
        //     Whether clientisde validation integration is enabled
        public CustomFluentValidationMvcConfiguration ConfigureClientsideValidation(
            Action<FluentValidationClientModelValidatorProvider> clientsideConfig = null,
            bool enabled = true)
        {
            _cfg.ConfigureClientsideValidation(clientsideConfig, enabled);
            return this;
        }
    }
}
