﻿using System;

using FluentValidation;
using FluentValidation.AspNetCore;
using FluentValidation.Internal;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class MvcBuilderFluentValidationExtensions
    {
        // TODO: replace FluentValidationMvcConfiguration with LinguaFluentValidationMvcConfiguration
        public static IMvcBuilder AddDefaultFluentValidation(
               this IMvcBuilder mvcbuilder,
               Action<FluentValidationMvcConfiguration> cfgCallback = null)
        {
            return mvcbuilder.AddFluentValidation(cfg =>
            {
                cfg.DisableDataAnnotationsValidation = false; 
                cfgCallback?.Invoke(cfg);
                ValidatorOptions.Global.PropertyNameResolver = (_, memberInfo, expression) =>
                {
                    if (expression != null)
                    {
                        var chain = PropertyChain.FromExpression(expression);
                        if (chain.Count > 0)
                        {
                            return chain.ToString().ToCamelCase();
                        }
                    }

                    if (memberInfo != null)
                    {
                        return memberInfo.Name.ToCamelCase();
                    }

                    return null;
                };
            });
        }

        public static IServiceCollection AddDefaultFluentValidation(
              this IServiceCollection mvcbuilder,
              Action<FluentValidationMvcConfiguration> cfgCallback = null)
        {
            return mvcbuilder.AddFluentValidation(cfg =>
            {
                cfg.DisableDataAnnotationsValidation = false;
                cfgCallback?.Invoke(cfg);
                ValidatorOptions.Global.PropertyNameResolver = (_, memberInfo, expression) =>
                {
                    if (expression != null)
                    {
                        var chain = PropertyChain.FromExpression(expression);
                        if (chain.Count > 0)
                        {
                            return chain.ToString().ToCamelCase();
                        }
                    }

                    if (memberInfo != null)
                    {
                        return memberInfo.Name.ToCamelCase();
                    }

                    return null;
                };
            });
        }
    }
}
