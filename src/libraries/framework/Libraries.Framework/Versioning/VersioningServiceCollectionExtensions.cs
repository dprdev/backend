﻿
using Microsoft.AspNetCore.Mvc.Versioning;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class VersioningServiceCollectionExtensions
    {
        public static IServiceCollection AddDefaultApiVersioning(this IServiceCollection services)
        {
            return services.AddApiVersioning(cfg =>
            {
                cfg.ApiVersionReader = new UrlSegmentApiVersionReader();
            });
        }
    }
}
