﻿using System;

namespace Agenda.Application
{

    public sealed class QueryContext
    {
        public QueryContext(
            string userId,
            string userTimezone,
            DateTime to,
            DateTime from)
        {
            if (string.IsNullOrEmpty(userId))
            {
                throw new ArgumentException($"'{nameof(userId)}' cannot be null or empty.", nameof(userId));
            }

            if (string.IsNullOrEmpty(userTimezone))
            {
                throw new ArgumentException($"'{nameof(userTimezone)}' cannot be null or empty.", nameof(userTimezone));
            }

            UserId = userId;
            UserTimezone = userTimezone;
            To = to;
            From = from;
        }

        public string UserId { get; private set; }
        public string UserTimezone { get; private set; }

        public DateTime To { get; private set; }
        public DateTime From { get; private set; }
    }
}
