﻿
using System;

using MediatR;

namespace Agenda.Application.GetDetailed
{
    public sealed class GetDetailedRequest : IRequest<GetDetailedResponse>
    {
        public GetDetailedRequest(QueryContext queryContext)
        {
            if (queryContext is null)
            {
                throw new ArgumentNullException(nameof(queryContext));
            }

            QueryContext = queryContext;
        }

        public QueryContext QueryContext { get; private set; }
    }
}
