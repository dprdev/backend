﻿using System;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

namespace Agenda.Application.GetDetailed
{
    internal sealed class GetDetailedRequestHandler
        : IRequestHandler<GetDetailedRequest, GetDetailedResponse>
    {
        public Task<GetDetailedResponse> Handle(
            GetDetailedRequest request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
