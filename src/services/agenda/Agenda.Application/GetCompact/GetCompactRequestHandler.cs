﻿using System;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

namespace Agenda.Application.GetCompact
{
    internal sealed class GetCompactRequestHandler
        : IRequestHandler<GetCompactRequest, GetCompactResponse>
    {
        public Task<GetCompactResponse> Handle(
            GetCompactRequest request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
