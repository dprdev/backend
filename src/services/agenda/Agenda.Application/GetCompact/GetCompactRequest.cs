﻿
using System;

using MediatR;

namespace Agenda.Application.GetCompact
{
    public sealed class GetCompactRequest : IRequest<GetCompactResponse>
    {
        public GetCompactRequest(QueryContext queryContext)
        {
            if (queryContext is null)
            {
                throw new ArgumentNullException(nameof(queryContext));
            }

            QueryContext = queryContext;
        }

        public QueryContext QueryContext { get; private set; }
    }
}
