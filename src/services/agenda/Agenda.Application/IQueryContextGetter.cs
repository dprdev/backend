﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using OneOf;

namespace Agenda.Application
{
    public sealed class QueryContextGetterResult : OneOfBase<QueryContext, IEnumerable<ValidationResult>>
    {
        public QueryContextGetterResult(OneOf<QueryContext, IEnumerable<ValidationResult>> input) : base(input)
        {
        }
    }

    public interface IQueryContextGetter
    {
        QueryContextGetterResult Get();
    }
}
