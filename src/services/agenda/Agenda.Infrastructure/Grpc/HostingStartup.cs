﻿
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Agenda.Infrastructure.Grpc
{
    using AppointmentsServiceClient = AppointmentsService.AppointmentsServiceClient;
    using ScheduleServiceClient = ScheduleService.ScheduleServiceClient;

    public sealed class HostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((ctx, services) =>
            {
                services.AddGrpcClient<ScheduleServiceClient>(cfg =>
                {
                    cfg.Address = new(ctx.Configuration.GetValue<string>("scheduleGrpc"));
                });

                services.AddGrpcClient<AppointmentsServiceClient>(cfg =>
                {
                    cfg.Address = new(ctx.Configuration.GetValue<string>("appointmentsGrpc"));
                });
            });
        }
    }
}
