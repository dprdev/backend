﻿
using System;

using Agenda.Application;

using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;

namespace Agenda.Infrastructure.Services
{
    internal sealed class QueryContextGetter : IQueryContextGetter
    {
        private readonly HttpContext _httpContext;

        public QueryContextGetter(HttpContext httpContext)
        {
            if (httpContext is null)
            {
                throw new ArgumentNullException(nameof(httpContext));
            }

            _httpContext = httpContext;
        }

        public QueryContextGetterResult Get()
        {
            HttpRequest req = _httpContext.Request;
            if (req.Query.TryGetValue("date-from", out StringValues dateFrom)
                && req.Query.TryGetValue("date-to", out StringValues dateTo)
                && req.Query.TryGetValue("timezone", out StringValues timezone))
            {

            }

            throw new NotImplementedException(nameof(Get));
        }
    }
}
