﻿
using System;

using Agenda.Application;

using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace Agenda.Infrastructure.Services
{
    public sealed class HostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((host, services) =>
            {
                services.AddMvcCore();
                services.AddHttpContextAccessor();
                services.AddScoped((provider) =>
                {
                    IHttpContextAccessor accessor = provider.GetRequiredService<IHttpContextAccessor>();
                    if (accessor.HttpContext is null)
                    {
                        throw new InvalidOperationException("Cann't resolve HttpContext outside request scope");
                    }

                    return accessor.HttpContext;
                });

                services.AddScoped<IQueryContextGetter, QueryContextGetter>();
            });
        }
    }
}
