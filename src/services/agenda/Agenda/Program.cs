﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

using Agenda.Application;
using Agenda.Application.GetCompact;
using Agenda.Application.GetDetailed;

using Hellang.Middleware.ProblemDetails;

using MediatR;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

[assembly: HostingStartup(typeof(Agenda.Infrastructure.MediatR.HostingStartup))]
[assembly: HostingStartup(typeof(Agenda.Infrastructure.Services.HostingStartup))]

IHostBuilder builder = Host
    .CreateDefaultBuilder(args)
    .ConfigureWebHostDefaults(webBuilder =>
    {
        webBuilder.ConfigureServices((s) =>
        {
            s.AddProblemDetails();
        });

        webBuilder.Configure((host, app) =>
        {
            app.UseProblemDetails();
            app.UseRouting();
            app.UseEndpoints((endpoints) =>
            {
                endpoints.MapGet("/api/v1/users/{user}/agenda/compact", async (context) =>
                {
                    IMediator mediator = context.RequestServices.GetService<IMediator>();
                    QueryContextGetterResult queryContext =
                        context.RequestServices.GetService<IQueryContextGetter>().Get();

                    await queryContext.Match(
                        (query) => Handle(new GetCompactRequest(query)),
                        (error) => BadRequest(context, error));

                    async Task Handle(GetCompactRequest request)
                    {
                        GetCompactResponse result =
                            await mediator.Send(request);

                        await context.Response.WriteAsJsonAsync(new { result });
                    }
                });

                endpoints.MapGet("/api/v1/users/{user}/agenda/detailed", async (context) =>
                {
                    IMediator mediator = context.RequestServices.GetService<IMediator>();
                    QueryContextGetterResult queryContext =
                        context.RequestServices.GetService<IQueryContextGetter>().Get();

                    await queryContext.Match(
                        (query) => Handle(new GetDetailedRequest(query)),
                        (error) => BadRequest(context, error));

                    async Task Handle(GetDetailedRequest request)
                    {
                        GetDetailedResponse result =
                            await mediator.Send(request);

                        await context.Response.WriteAsJsonAsync(new { result });
                    }
                });
            });
        });
    });

async Task BadRequest(
    HttpContext context,
    IEnumerable<ValidationResult> validationResults)
{
    var modelState = new ModelStateDictionary();
    foreach (ValidationResult result in validationResults)
    {
        foreach (string key in result.MemberNames)
        {
            modelState.AddModelError(key, result.ErrorMessage);
        }
    }

    var resp = new BadRequestObjectResult(
        new ValidationProblemDetails(modelState));

    await resp.ExecuteResultAsync(
        new() { HttpContext = context });
}

await builder.StartAsync();
