﻿using System.Threading.Tasks;

namespace Teachers.Infrastructure.EfCore.Seeders
{
    internal interface ISeeder
    {
        Task Seed();
    }
}
