﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using Teachers.Domain.Entities;

namespace Teachers.Infrastructure.EfCore.EntityTypeConfigurations
{
    public class ContactConfiguration : IEntityTypeConfiguration<Contact>
    {
        public void Configure(EntityTypeBuilder<Contact> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Value).HasMaxLength(256);
        }
    }
}
