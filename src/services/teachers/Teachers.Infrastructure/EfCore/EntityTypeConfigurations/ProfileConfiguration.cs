﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using Teachers.Domain.Entities;

namespace Teachers.Infrastructure.EfCore.EntityTypeConfigurations
{
    internal sealed class ProfileConfiguration : IEntityTypeConfiguration<Profile>
    {
        public void Configure(EntityTypeBuilder<Profile> builder)
        {
            builder.HasKey(e => e.Id);
            builder.HasIndex(e => e.State);

            builder.Property(e => e.Id).HasMaxLength(256);
            builder.Property(e => e.Email).HasMaxLength(256);
            builder.Property(e => e.Name).HasMaxLength(256);
            builder.Property(e => e.Surname).HasMaxLength(256);
            builder.Property(e => e.Timezone).HasMaxLength(128);
            builder.Property(e => e.Avatar).HasMaxLength(256);
            builder.Property(e => e.Video).HasMaxLength(256);
            builder.Property(e => e.AboutMe).HasMaxLength(500);
            builder.Property(e => e.AboutMyLessons).HasMaxLength(500);
            builder.Property(e => e.AboutMyTutorSkills).HasMaxLength(500);
            builder.Property(e => e.Country).HasMaxLength(2);
            builder.Property(e => e.LivingCountry).HasMaxLength(2);

            builder.OwnsOne(e => e.Preferences, cfg =>
            {
                cfg.Property(e => e.Ages).HasColumnType("jsonb");
            });

            builder.HasMany(e => e.Contacts).WithOne().HasForeignKey("ProfileId").OnDelete(DeleteBehavior.Cascade);
            builder.HasMany(e => e.Languages).WithOne().HasForeignKey("ProfileId").OnDelete(DeleteBehavior.Cascade);
            builder.HasMany(e => e.Documents).WithOne().HasForeignKey("ProfileId").OnDelete(DeleteBehavior.Cascade);
        }
    }
}
