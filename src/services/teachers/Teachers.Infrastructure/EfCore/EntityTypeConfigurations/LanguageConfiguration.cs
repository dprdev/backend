﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using Teachers.Domain.Entities;

namespace Teachers.Infrastructure.EfCore.EntityTypeConfigurations
{
    internal sealed class LanguageConfiguration : IEntityTypeConfiguration<Language>
    {
        public void Configure(EntityTypeBuilder<Language> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).HasMaxLength(256);
            builder.Property(e => e.Code).HasMaxLength(2);
        }
    }
}
