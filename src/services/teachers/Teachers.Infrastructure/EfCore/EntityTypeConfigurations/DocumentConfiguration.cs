﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using Teachers.Domain.Entities;

namespace Teachers.Infrastructure.EfCore.EntityTypeConfigurations
{
    public class DocumentConfiguration : IEntityTypeConfiguration<Document>
    {
        public void Configure(EntityTypeBuilder<Document> builder)
        {
            builder.HasKey(e => e.Id);
            builder.HasIndex(e => e.ProfileId);
            builder.HasIndex(e => e.DocumentType);

            builder.Property(e => e.Id).HasMaxLength(256);
            builder.Property(e => e.ProfileId).HasMaxLength(256);

            builder.Property(e => e.Name).HasMaxLength(512);
            builder.Property(e => e.Url).HasMaxLength(512);
        }
    }
}
