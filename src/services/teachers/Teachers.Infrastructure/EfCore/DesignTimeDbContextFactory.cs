﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

using Teachers.Domain;

namespace Teachers.Infrastructure.EfCore
{
    internal sealed class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<DataContext>
    {
        DataContext IDesignTimeDbContextFactory<DataContext>.CreateDbContext(string[] args)
        {
            DbContextOptionsBuilder<DataContext> optBuilder
                = new DbContextOptionsBuilder<DataContext>()
                    .UseNpgsql((cfg) =>
                    {
                        cfg.MigrationsAssembly("Teachers.Infrastructure");
                    });

            return new DataContext(optBuilder.Options);
        }
    }
}
