﻿
using System.Reflection;

using Hellang.Middleware.ProblemDetails;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

// configure sentry first
[assembly: HostingStartup(typeof(Teachers.Infrastructure.Sentry.HostingStartup))]
[assembly: HostingStartup(typeof(Teachers.Infrastructure.EfCore.HostingStartup))]
[assembly: HostingStartup(typeof(Teachers.Infrastructure.Masstransit.HostingStartup))]
[assembly: HostingStartup(typeof(Teachers.Infrastructure.MediatR.HostingStartup))]

IHostBuilder builder = Host
    .CreateDefaultBuilder(args)
    .ConfigureWebHostDefaults(webBuilder =>
    {
        webBuilder.ConfigureServices((host, services) =>
        {
            services.AddProblemDetails();
            services.AddDefaultApiVersioning();
            services.AddControllers()
                .AddDefaultFluentValidation((cfg) =>
                {
                    cfg.RegisterValidatorsFromAssembly(Assembly.Load("Teachers"));
                    cfg.RegisterValidatorsFromAssembly(Assembly.Load("Teachers.Application"));
                    cfg.RegisterValidatorsFromAssembly(Assembly.Load("Teachers.Domain"));
                });
        });

        webBuilder.Configure((host, app) =>
        {
            app.UseProblemDetails();
            app.UseRouting(); 
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        });
    });

await builder.StartAsync();
