﻿
using System;
using System.Threading;
using System.Threading.Tasks;

using AutoMapper;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Teachers.Domain;

namespace Teachers.Application.Profile.JoinStage2
{
    internal sealed class JoinStage2RequestHandler
        : IRequestHandler<JoinStage2Request, JoinStage2Response>
    {
        private readonly IMapper _mapper;
        private readonly DataContext _context;

        public JoinStage2RequestHandler(IMapper mapper, DataContext context)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<JoinStage2Response> Handle(
            JoinStage2Request request, CancellationToken cancellationToken)
        {
            Domain.Entities.Profile profile =
                await _context.Profiles.Include(e => e.Languages)
                    .FirstOrDefaultAsync(e => e.Id == request.Id, cancellationToken: cancellationToken);

            _mapper.Map(request.Payload, profile);
            try
            {
                await _context.SaveChangesAsync(cancellationToken);
            }
            catch (Exception)
            {
                // TODO: do something
            }

            return new();
        }
    }

}
