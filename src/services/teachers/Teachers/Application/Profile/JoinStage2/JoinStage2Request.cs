﻿using System;

using MediatR;

namespace Teachers.Application.Profile.JoinStage2
{
    public sealed class JoinStage2Request : IRequest<JoinStage2Response>
    {
        public JoinStage2Request(string id, JoinStage2Payload payload)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentException($"'{nameof(id)}' cannot be null or whitespace.", nameof(id));
            }

            if (payload is null)
            {
                throw new ArgumentNullException(nameof(payload));
            }

            Id = id;
            Payload = payload;
        }

        public string Id { get; private set; }
        public JoinStage2Payload Payload { get; private set; }
    }
}
