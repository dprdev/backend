﻿namespace Teachers.Application.Profile.JoinStage2
{
    public sealed class JoinStage2Payload
    {
        public string Video { get; private set; }
        public string AboutMe { get; private set; }
        public string AboutMyTutorSkills { get; private set; }
        public string AboutMyLessons { get; private set; }
        public string[] TeachedLanguages { get; private set; }
    }
}
