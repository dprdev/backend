﻿
using System;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Teachers.Domain;

namespace Teachers.Application.Profile.Get
{
    internal sealed class GetProfileRequestHandler
        : IRequestHandler<GetProfileRequest, GetProfileResponse>
    {
        private readonly DataContext _context;

        public GetProfileRequestHandler(DataContext context)
        {
            if (context is null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            _context = context;
        }

        public Task<GetProfileResponse> Handle(
            GetProfileRequest request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }

}
