﻿using System;

using MediatR;

namespace Teachers.Application.Profile.Get
{
    public class GetProfileRequest : IRequest<GetProfileResponse>
    {
        public GetProfileRequest(string id, string[] fields)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentException($"'{nameof(id)}' cannot be null or whitespace.", nameof(id));
            }

            if (fields is null)
            {
                throw new ArgumentNullException(nameof(fields));
            }

            if (fields.Length == 0)
            {
                throw new ArgumentException($"'{nameof(fields)}' cannot have zero length.", nameof(fields));
            }

            Id = id;
            Fields = fields;
        }

        public string Id { get; private set; }
        public string[] Fields { get; private set; }
    }
}
