﻿
using System;
using System.Threading;
using System.Threading.Tasks;

using AutoMapper;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Teachers.Domain;

namespace Teachers.Application.Profile.JoinStage1
{
    internal sealed class JoinStage1RequestHandler
        : IRequestHandler<JoinStage1Request, JoinStage1Response>
    {
        private readonly IMapper _mapper;
        private readonly DataContext _context;

        public JoinStage1RequestHandler(IMapper mapper, DataContext context)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        // TODO: maybe we should use transactions ?
        public async Task<JoinStage1Response> Handle(
            JoinStage1Request request, CancellationToken cancellationToken)
        {
            Domain.Entities.Profile profile =
                await _context.Profiles.Include(e => e.Languages)
                    .FirstOrDefaultAsync(e => e.Id == request.Id, cancellationToken: cancellationToken);

            _mapper.Map(request.Payload, profile);
            try
            {
                await _context.SaveChangesAsync(cancellationToken);
            }
            catch (Exception)
            {
                // TODO: do something
            }

            return new();
        }
    }
}
