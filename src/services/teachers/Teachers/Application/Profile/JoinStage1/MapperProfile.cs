﻿namespace Teachers.Application.Profile.JoinStage1
{
    internal class MapperProfile : AutoMapper.Profile
    {
        public MapperProfile()
        {
            CreateMap<JoinStage1Payload, Domain.Entities.Profile>()
                .ForMember(e => e.Name, cfg => cfg.MapFrom(e => e.Name))
                .ForMember(e => e.Surname, cfg => cfg.MapFrom(e => e.Surname))
                .ForMember(e => e.Avatar, cfg => cfg.MapFrom(e => e.Avatar))
                .ForMember(e => e.Country, cfg => cfg.MapFrom(e => e.Country))
                .ForMember(e => e.LivingCountry, cfg => cfg.MapFrom(e => e.LivingCountry))
                .ForMember(e => e.Timezone, cfg => cfg.MapFrom(e => e.Timezone))
                .ForMember(e => e.Gender, cfg => cfg.MapFrom(e => e.Gender))
                .ForMember(e => e.DateOfBirth, cfg => cfg.MapFrom(e => e.DateOfBirth))
                .AfterMap((source, profile) =>
                {
                    profile.ClearLanguages();
                    foreach (JoinStage1Language language in source.Languages)
                    {
                        profile.CreateLanguage(language.Code, language.LangLevel);
                    }
                })
                .ForAllOtherMembers(e => e.Ignore());
        }
    }
}
