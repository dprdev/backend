﻿using System;

using MediatR;

namespace Teachers.Application.Profile.JoinStage1
{
    public sealed class JoinStage1Request : IRequest<JoinStage1Response>
    {
        public JoinStage1Request(string id, JoinStage1Payload payload)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentException($"'{nameof(id)}' cannot be null or whitespace.", nameof(id));
            }

            if (payload is null)
            {
                throw new ArgumentNullException(nameof(payload));
            }

            Id = id;
            Payload = payload;
        }

        public string Id { get; private set; }
        public JoinStage1Payload Payload { get; private set; }
    }
}
