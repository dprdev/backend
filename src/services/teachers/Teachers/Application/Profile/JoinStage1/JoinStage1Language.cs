﻿
using Teachers.Domain.Entities;

namespace Teachers.Application.Profile.JoinStage1
{
    public sealed class JoinStage1Language
    {
        public string Code { get; private set; }
        public LangLevel LangLevel { get; private set; }
    }
}
