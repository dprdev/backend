﻿
using System;
using System.Collections.Generic;

using Teachers.Domain.Entities;

namespace Teachers.Application.Profile.JoinStage1
{
    public sealed class JoinStage1Payload
    {
        public string Name { get; private set; }
        public string Surname { get; private set; }
        public string Avatar { get; private set; }
        public string Country { get; private set; }
        public string LivingCountry { get; private set; }
        public string Timezone { get; private set; }
        public Gender Gender { get; private set; }
        public DateTime DateOfBirth { get; private set; }
        public IReadOnlyList<JoinStage1Language> Languages { get; private set; }
    }
}
