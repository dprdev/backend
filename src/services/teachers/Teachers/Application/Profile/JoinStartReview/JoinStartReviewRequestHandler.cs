﻿
using System;
using System.Threading;
using System.Threading.Tasks;

using Contracts.Events.Moderation.JoinRequest;

using MassTransit;

using MediatR;

using Teachers.Domain;

namespace Teachers.Application.Profile.JoinStartReview
{
    internal sealed class JoinStartReviewRequestHandler
        : IRequestHandler<JoinStartReviewRequest, JoinStartReviewResponse>
    {
        private readonly IBusControl _bus;
        private readonly DataContext _context;

        public JoinStartReviewRequestHandler(IBusControl bus, DataContext context)
        {
            if (bus is null)
            {
                throw new ArgumentNullException(nameof(bus));
            }

            if (context is null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            _bus = bus;
            _context = context;
        }

        public async Task<JoinStartReviewResponse> Handle(
            JoinStartReviewRequest request,
            CancellationToken cancellationToken)
        {
            Domain.Entities.Profile user = await _context.Profiles.FindAsync(request.Id);
            if (user is null)
            {
                throw new InvalidOperationException($"User ({request.Id}) not found");
            }

            user.ChangeState(Domain.Entities.ProfileState.Joining);
            try
            {
                await _context.SaveChangesAsync();
                await _bus.Publish(new JoinSubmitted
                {
                    Id = user.Id,
                    Email = user.Email,
                    Photo = user.Avatar,
                    Username = user.Username
                });
            }
            catch (Exception)
            {
                throw;
            }

            return new();
        }
    }
}
