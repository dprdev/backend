﻿using System;

using MediatR;

namespace Teachers.Application.Profile.JoinStartReview
{
    public sealed class JoinStartReviewRequest : IRequest<JoinStartReviewResponse>
    {
        public JoinStartReviewRequest(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentException($"'{nameof(id)}' cannot be null or empty.", nameof(id));
            }

            Id = id;
        }

        public string Id { get; private set; }
    }
}
