﻿using System;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;

namespace Teachers.Endpoints.EndpointRoutes.VerificationWebhooks
{
    public static class WebhookEndpoints
    {
        public static Task IdentityVerification(HttpContext context)
        {
            throw new NotImplementedException();
        }
    }

    public static class VerificationWebhookExtensions
    {
        public static void MapWebhooks(this IEndpointRouteBuilder builder)
        {
            builder.MapPost("api/webhooks/identity-verification", WebhookEndpoints.IdentityVerification);
        }
    }
}
