﻿
using System;
using System.Threading.Tasks;

using Contracts.Events.Teachers;

using MassTransit;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

using Npgsql;

using Teachers.Domain;
using Teachers.Domain.Entities;

namespace Teachers.Endpoints.Consumers.TeacherCreated
{
    internal sealed class TeacherCreatedConsumer : IConsumer<TeacherRegistered>
    {
        private readonly DataContext _context;
        private readonly ILogger<TeacherCreatedConsumer> _logger;

        public TeacherCreatedConsumer(
            DataContext context,
            ILogger<TeacherCreatedConsumer> logger)
        {
            if (context is null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (logger is null)
            {
                throw new ArgumentNullException(nameof(logger));
            }

            _context = context;
            _logger = logger;
        }

        public async Task Consume(ConsumeContext<TeacherRegistered> context)
        {
            var profile = new Profile(context.Message.Id, context.Message.Email);
            try
            {
                await _context.AddWithSaveChanges(profile);
            }
            catch (DbUpdateException ex)
                when (ex.InnerException is PostgresException pgEx && pgEx.SqlState == "23505")
            {
                _logger.LogInformation(
                    $"Skip tutor creating: " +
                    $"user ({context.Message.Id}, {context.Message.Email}) already exists");
            }
        }
    }
}
