﻿using System;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

namespace Teachers.Endpoints.Controllers.Profile
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v{version:apiVersion}/profile")]
    public sealed class ProfileController : Controller
    {
        [HttpGet]
        public Task<IActionResult> Get()
        {
            throw new NotImplementedException(nameof(Get));
        }

        [HttpPost("name")]
        public Task<IActionResult> ChangeName()
        {
            throw new NotImplementedException(nameof(ChangeName));
        }

        [HttpPost("gender")]
        public Task<IActionResult> ChangeGender()
        {
            throw new NotImplementedException(nameof(ChangeGender));
        }

        [HttpPost("birth-date")]
        public Task<IActionResult> ChangeBirthDate()
        {
            throw new NotImplementedException(nameof(ChangeBirthDate));
        }

        /// <summary>
        /// Update country, living country
        /// </summary> 
        [HttpPost("geo")]
        public Task<IActionResult> ChangeGeo()
        {
            throw new NotImplementedException(nameof(ChangeGeo));
        }

        [HttpPost("timezone")]
        public Task<IActionResult> ChangeTimezone()
        {
            throw new NotImplementedException(nameof(ChangeTimezone));
        }

        [HttpPost("about")]
        public Task<IActionResult> ChangeAbout()
        {
            throw new NotImplementedException(nameof(ChangeAbout));
        }

        [HttpPost("about-skills")]
        public Task<IActionResult> ChangeAboutSkills()
        {
            throw new NotImplementedException(nameof(ChangeAboutSkills));
        }

        [HttpPost("about-lessons")]
        public Task<IActionResult> ChangeAboutLesons()
        {
            throw new NotImplementedException(nameof(ChangeAboutLesons));
        }

        [HttpPost("photo")]
        public Task<IActionResult> ChangePhoto()
        {
            throw new NotImplementedException(nameof(ChangePhoto));
        }

        [HttpPost("education")]
        public Task<IActionResult> CreateEducation()
        {
            throw new NotImplementedException(nameof(CreateEducation));
        }

        [HttpPost("certificates")]
        public Task<IActionResult> CreateCertificate()
        {
            throw new NotImplementedException(nameof(CreateCertificate));
        }

        [HttpPost("work")]
        public Task<IActionResult> CreateWork()
        {
            throw new NotImplementedException(nameof(CreateWork));
        }

        [HttpPost("materials")]
        public Task<IActionResult> ChangeMaterials()
        {
            throw new NotImplementedException(nameof(ChangeMaterials));
        }

        [HttpPost("video")]
        public Task<IActionResult> ChangeVideo()
        {
            throw new NotImplementedException(nameof(ChangeVideo));
        }
    }
}
