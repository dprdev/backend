﻿
using System.Security.Claims;
using System.Threading.Tasks;

using MediatR;

using Microsoft.AspNetCore.Mvc;

using Teachers.Application.Profile.JoinStage1;
using Teachers.Application.Profile.JoinStage2;
using Teachers.Application.Profile.JoinStartReview;

namespace Teachers.Endpoints.Controllers.Join
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/join")]
    public sealed class JoinController : Controller
    {
        [ConsumesJson]
        [HttpPost("user-step")]
        public async Task<IActionResult> UserStep(
            [FromServices] IMediator mediator,
            [FromBody] JoinStage1Payload payload)
        {
            // TODO: handle exception
            JoinStage1Response res = await mediator.Send(
                new JoinStage1Request(User.GetId(), payload));

            return Ok();
        }

        [ConsumesJson]
        [HttpPost("tutor-step")]
        public async Task<IActionResult> TutorStep(
            [FromServices] IMediator mediator,
            [FromBody] JoinStage2Payload payload)
        {
            // TODO: handle exception
            JoinStage2Response res = await mediator.Send(
                new JoinStage2Request(User.GetId(), payload));

            return Ok();
        }

        [HttpPost("review")]
        public async Task<IActionResult> ToReview([FromServices] IMediator mediator)
        {
            JoinStartReviewResponse result =
                await mediator.Send(new JoinStartReviewRequest(User.GetId()));

            return Ok();
        }
    }
}
