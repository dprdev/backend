﻿using System;

namespace Teachers.Domain.Entities
{
    public sealed class Document
    {
        private Document()
        {

        }

        public Document(string url, string name, DocumentType type)
        {
            if (string.IsNullOrWhiteSpace(url))
            {
                throw new ArgumentException($"'{nameof(url)}' cannot be null or whitespace.", nameof(url));
            }

            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentException($"'{nameof(name)}' cannot be null or whitespace.", nameof(name));
            }

            Id = Guid
                .NewGuid()
                .ToString();

            Url = url;
            Name = name;
            DocumentType = type;
        }

        public string Id { get; private set; }
        public string ProfileId { get; private set; }

        public string Url { get; private set; }
        public string Name { get; private set; }

        public DocumentType DocumentType { get; private set; }
    }
}
