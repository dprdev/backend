﻿using System.Collections.Generic;

namespace Teachers.Domain.Entities
{
    public sealed class Preferences
    {
        public IReadOnlyList<Range> Ages { get; private set; }
    }
}
