﻿using System;

namespace Teachers.Domain.Entities
{
    public sealed class Contact
    {
        private Contact()
        {

        }

        public Contact(
            string value,
            ContactType contactType)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ArgumentException($"'{nameof(value)}' cannot be null or whitespace.", nameof(value));
            }

            Id = Guid
                .NewGuid()
                .ToString();

            Value = value;
            ContactType = contactType;
        }

        public string Id { get; private set; }
        public string Value { get; private set; }
        public ContactType ContactType { get; private set; }
    }
}
