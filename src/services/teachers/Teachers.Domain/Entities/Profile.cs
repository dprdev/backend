﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Teachers.Domain.Entities
{
    public sealed class Profile
    {
        private readonly List<Contact> _contacts;
        private readonly List<Language> _languages;
        private readonly List<Document> _documents;

        private Profile()
        {
        }

        public Profile(string id, string email)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentException($"'{nameof(id)}' cannot be null or whitespace.", nameof(id));
            }

            if (string.IsNullOrWhiteSpace(email))
            {
                throw new ArgumentException($"'{nameof(email)}' cannot be null or whitespace.", nameof(email));
            }

            Id = id;
            Email = email;
            State = ProfileState.New;
            CreatedDate = DateTime.UtcNow;
            DateOfBirth = DateTime.MinValue;
            Gender = Gender.Unset;
            Preferences = new()
            {
            };

            _contacts = new List<Contact>(0);
            _languages = new List<Language>(0);
            _documents = new List<Document>(0);
        }

        /// <summary>
        /// Same as account ID. 
        /// </summary>
        public string Id { get; private set; }
        public string Email { get; private set; }
        public string Name { get; private set; }
        public string Surname { get; private set; }

        public string Username => $"{Name} {Surname}";

        public string Country { get; private set; }
        public string LivingCountry { get; private set; }
        public string Timezone { get; private set; }
        public Gender Gender { get; private set; }
        public DateTime DateOfBirth { get; private set; }
        public string Avatar { get; private set; }

        public DateTime CreatedDate { get; private set; }

        public string Video { get; private set; }
        public string AboutMe { get; private set; }
        public string AboutMyTutorSkills { get; private set; }
        public string AboutMyLessons { get; private set; }
        public ProfileState State { get; private set; }
        public Preferences Preferences { get; private set; }
        public IReadOnlyList<Contact> Contacts => _contacts;
        public IReadOnlyList<Language> Languages => _languages;
        public IReadOnlyList<Document> Documents => _documents;


        public void ClearLanguages()
        {
            if (_languages is null)
            {
                throw new InvalidOperationException(
                    "Can not update navigation because this does not loaded");
            }

            _languages.Clear();
        }

        public void CreateLanguage(string code, LangLevel level, bool teaching = false)
        {
            if (string.IsNullOrWhiteSpace(code))
            {
                throw new ArgumentException(
                    $"'{nameof(code)}' cannot be null or whitespace.", nameof(code));
            }

            if (_languages is null)
            {
                throw new InvalidOperationException(
                    "Can not update navigation because this does not loaded");
            }

            if (_languages.Any(e => e.Code == code))
            {
                throw new InvalidOperationException("Can't add language because one exists");
            }

            _languages.Add(new Language(code, level, teaching));
        }

        public void RemoveLanguage(string code)
        {
            if (_languages is null)
            {
                throw new InvalidOperationException(
                    "Can not update navigation because this does not loaded");
            }

            int index = _languages.FindIndex(e => e.Code == code);
            if (index < 0)
            {
                throw new InvalidOperationException("Language does not exists");
            }

            _languages.RemoveAt(index);
        }

        public void SetTeachedLanguages(IReadOnlyList<string> codes)
        {
            if (_languages is null)
            {
                throw new InvalidOperationException(
                    "Can not update navigation because this does not loaded");
            }

            foreach (Language language in _languages)
            {
                if (codes.Contains(language.Code))
                {
                    language.Teach();
                }
                else
                {
                    language.UnTeach();
                }
            }
        }

        public void ChangeState(ProfileState state)
        {
            bool result = (State, state) switch
            {
                (ProfileState.New, ProfileState.Joining) => true,
                (ProfileState.Joining, ProfileState.Active) => true,
                (ProfileState.Active, ProfileState.Changing) => true,
                (ProfileState.Changing, ProfileState.Active) => true,
                (_, ProfileState.Blocked) => true,
                _ => false
            };

            if (!result)
            {
                throw new InvalidOperationException($"Can not change state from {State} to {state}");
            }

            State = state;
        }
    }
}
