﻿namespace Teachers.Domain.Entities
{
    public enum ContactType
    {
        Skype,
        Email
    }
}
