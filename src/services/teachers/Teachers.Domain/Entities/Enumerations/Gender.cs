﻿namespace Teachers.Domain.Entities
{
    public enum Gender
    {
        Unset,
        Female,
        Male,
    }
}
