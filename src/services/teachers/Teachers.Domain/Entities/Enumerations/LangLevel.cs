﻿namespace Teachers.Domain.Entities
{
    public enum LangLevel { A1, A2, B1, B2, C1, C2, Native }
}
