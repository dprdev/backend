﻿namespace Teachers.Domain.Entities
{
    public enum DocumentType
    {
        Work,
        Certificate,
        Education
    }
}
