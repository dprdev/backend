﻿namespace Teachers.Domain.Entities
{
    public enum ProfileState
    {
        New,
        Active,
        Blocked,
        Joining,
        Changing
    }
}
