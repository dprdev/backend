﻿using System;

namespace Teachers.Domain.Entities
{
    public sealed class Language
    {
        private Language()
        {

        }

        public Language(string code, LangLevel level, bool teaching)
        {
            if (string.IsNullOrWhiteSpace(code))
            {
                throw new ArgumentException($"'{nameof(code)}' cannot be null or whitespace.", nameof(code));
            }

            Id = Guid
                .NewGuid()
                .ToString();

            Code = code;
            Level = level;
            Teaching = teaching;
        }

        public string Id { get; private set; }
        public string Code { get; private set; }
        public LangLevel Level { get; private set; }
        public bool Teaching { get; private set; }

        public void Teach()
        {
            Teaching = true;
        }

        public void UnTeach()
        {
            Teaching = true;
        }
    }
}
