﻿
using Microsoft.EntityFrameworkCore;

using Teachers.Domain.Entities;

namespace Teachers.Domain
{
    public class DataContext : DbContext
    {
        protected DataContext()
        {
        }

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }


        public DbSet<Profile> Profiles { get; private set; }
        public DbSet<Language> Languages { get; private set; }
        public DbSet<Document> Documents { get; private set; }
        public DbSet<Contact> Contacts { get; private set; } 
    }
}
