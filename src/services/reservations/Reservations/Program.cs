﻿
using System.Reflection;

using Hellang.Middleware.ProblemDetails;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

// configure sentry first
[assembly: HostingStartup(typeof(Reservations.Infrastructure.Sentry.HostingStartup))]
[assembly: HostingStartup(typeof(Reservations.Infrastructure.EfCore.HostingStartup))]
[assembly: HostingStartup(typeof(Reservations.Infrastructure.Masstransit.HostingStartup))]
[assembly: HostingStartup(typeof(Reservations.Infrastructure.MediatR.HostingStartup))]

IHostBuilder builder = Host
    .CreateDefaultBuilder(args)
    .ConfigureWebHostDefaults(webBuilder =>
    {
        webBuilder.ConfigureServices((host, services) =>
        {
            services.AddProblemDetails();
            services.AddDefaultApiVersioning();

            IMvcBuilder mvcBuilder = services.AddControllers((cfg) =>
            {
            });
            mvcBuilder.AddDefaultFluentValidation((cfg) =>
            {
                cfg.RegisterValidatorsFromAssembly(Assembly.Load("Reservations"));
                cfg.RegisterValidatorsFromAssembly(Assembly.Load("Reservations.Application"));
                cfg.RegisterValidatorsFromAssembly(Assembly.Load("Reservations.Domain"));
            });

            mvcBuilder.AddJsonOptions((cfg) =>
            { 
            });

            services.AddAuthentication()
                .AddJwtBearer(cfg =>
                {
                    if (host.HostingEnvironment.IsDevelopment())
                    {
                        cfg.RequireHttpsMetadata = false;
                    }

                    cfg.Authority = host.Configuration.GetString("identityUrl");
                    cfg.TokenValidationParameters = new()
                    {
                        ValidateLifetime = false
                    }; 
                });

            services.AddAuthorization((builder) =>
            {
                builder.AddPolicy("student", cfg => cfg.RequireRole("student", "teacher"));
                builder.AddPolicy("teacher", cfg => cfg.RequireRole("student", "teacher"));
                builder.AddPolicy("customers", cfg =>
                {
                    cfg.RequireRole("student", "teacher");
                });
            });
        });

        webBuilder.Configure((host, app) =>
        {
            app.UseProblemDetails();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        });
    });

await builder.StartAsync();
