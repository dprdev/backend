﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;

using MediatR;

using Microsoft.AspNetCore.Mvc;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Reservations.Controllers.Lessons
{
    //[JwtAuthorize]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/lessons")]
    public sealed class LessonsController : ApiController
    {
        [HttpGet]
        public Task<IActionResult> Lessons([FromServices] IMediator mediator)
        {
            throw new NotImplementedException(nameof(Lessons));
        }

        [HttpGet("{lesson}")]
        public Task<IActionResult> Lesson()
        {
            throw new NotImplementedException(nameof(Lesson));
        }

        [HttpGet("invitation")]
        public Task<IActionResult> SubmitInvitation()
        {
            throw new NotImplementedException(nameof(SubmitInvitation));
        }

        [HttpPost("{lesson}/report")]
        public Task<IActionResult> SubmitReport()
        {
            throw new NotImplementedException(nameof(SubmitReport));
        }

        [HttpPost("{lesson}/report/review")]
        public Task<IActionResult> SubmitReportReview()
        {
            throw new NotImplementedException(nameof(SubmitReport));
        }

        [HttpPost("{lesson}/action")]
        public async Task<IActionResult> SubmitAction(
            [FromServices] IMediator mediator, string lesson)
        {
            using var textReader = new JsonTextReader(
                new StreamReader(HttpContext.Request.Body));

            JObject jObj = await JObject.LoadAsync(textReader);
            try
            {
                return GetActionType(jObj) switch
                {
                    ActionType.Confirm => await HandleConfirm(mediator, lesson),
                    ActionType.Decline => await HandleDecline(mediator, lesson),
                    _ => ValidationProblem(
                            new ValidationProblemDetails(
                                new Dictionary<string, string[]>
                                {
                                    { "type", new string[] { "Unknown action type" } }
                                }))
                };
            }
            catch (Exception)
            {
                return Problem(statusCode: (int)HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost("{lesson}/action/review")]
        public Task<IActionResult> SubmitActionReview()
        { 
            throw new NotImplementedException(nameof(SubmitAction));
        }



        private Task<IActionResult> HandleConfirm(IMediator mediator, string lesson)
        {
            throw new NotImplementedException(nameof(HandleConfirm));
        }

        private Task<IActionResult> HandleDecline(IMediator mediator, string lesson)
        {
            throw new NotImplementedException(nameof(HandleDecline));
        }



        private static ActionType GetActionType(JObject jObj)
        {
            if (jObj.TryGetValue("type", out JToken value)
                && Enum.TryParse(value.ToString(), out ActionType action))
            {
                return action;
            }

            return ActionType.Unknown;
        }




        public enum ActionType
        {
            Unknown = 0,
            Confirm = 1,
            Decline = 2,
            Reschedule = 3,
            Cancellation = 4,
        }
    }
}
