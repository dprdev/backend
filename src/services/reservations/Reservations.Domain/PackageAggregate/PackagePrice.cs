﻿namespace Reservations.Domain.PackageAggregate
{
    public sealed class PackagePrice
    {
        public int Size { get; private set; }
        public double OverallPrice { get; private set; }
    }
}
