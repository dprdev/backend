﻿using System.Collections.Generic;

using Reservations.Domain.LessonAggregate;

namespace Reservations.Domain.PackageAggregate
{
    public sealed class PackageReservation
    {
        private readonly List<LessonReservation> _reservations;

        private PackageReservation()
        {
        }

        public string Id { get; private set; }
        public string OrderId { get; private set; }
        public string StudentId { get; private set; }
        public string TeacherId { get; private set; }
        public PackagePrice PackagePrice { get; private set; } 

        public IReadOnlyList<LessonReservation> Reservations => _reservations;
    }
}
