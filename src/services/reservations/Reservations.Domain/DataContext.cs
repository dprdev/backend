﻿
using System.Diagnostics.CodeAnalysis;

using Microsoft.EntityFrameworkCore;

using Reservations.Domain.LessonAggregate;
using Reservations.Domain.PackageAggregate;

namespace Reservations.Domain
{
    public class DataContext : DbContext
    {
        private DataContext()
        {
        }

        public DataContext([NotNull] DbContextOptions<DataContext> options) : base(options)
        {
        }

        public virtual DbSet<LessonReservation> LessonReservations { get; private set; }
        public virtual DbSet<PackageReservation> PackageReservations { get; private set; }
    }
}
