﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Reservations.Domain.LessonAggregate
{
    [Serializable]
    public class ReservationDomainException : Exception
    {
        private static readonly DomainError[] _empty = Array.Empty<DomainError>();

        public ReservationDomainException(IEnumerable<DomainError> errors = null)
        {
            SetErrors(errors);
        }

        public ReservationDomainException(string message, IEnumerable<DomainError> errors = null)
            : base(message)
        {
            SetErrors(errors);
        }

        public ReservationDomainException(string message, Exception inner, IEnumerable<DomainError> errors = null)
            : base(message, inner)
        {
            SetErrors(errors);
        }

        protected ReservationDomainException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }


        public IReadOnlyList<DomainError> Errors { get; private set; }

        private void SetErrors(IEnumerable<DomainError> errors)
        {
            if (errors is not null)
            {
                Errors = errors.ToList();
            }
            else
            {
                Errors = _empty;
            }
        }
    }
}
