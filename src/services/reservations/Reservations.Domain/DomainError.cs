﻿namespace Reservations.Domain.LessonAggregate
{
    public record DomainError
    {
        public string Code { get; private set; }
    }
}
