﻿namespace Reservations.Domain.LessonAggregate
{
    public class ProblemRequest : ChangeRequest
    {
        private ProblemRequest()
        {
        }

        public ProblemRequest(
            string reason,
            string solution,
            string reporter)
        {
            Reason = reason;
            Solution = solution;
            Reporter = reporter;
        }

        public string Reason { get; private set; }
        public string Solution { get; private set; }
        public string Reporter { get; private set; }
    }
}
