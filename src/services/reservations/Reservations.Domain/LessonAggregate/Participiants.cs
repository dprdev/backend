﻿namespace Reservations.Domain.LessonAggregate
{
    public class Participiants
    {
        public string StudentId { get; private set; }
        public string TeacherId { get; private set; }

        public bool IsTeacher(string user)
        {
            return user == TeacherId;
        }

        public bool IsStudent(string user)
        {
            return user == StudentId;
        }
    }
}
