﻿namespace Reservations.Domain.LessonAggregate
{
    public enum State
    {
        PendingTutorConfirmation,
        PendingStudentConfirmation,
        Declined,
        Confirmed,
        Cancelled
    }
}
