﻿namespace Reservations.Domain.LessonAggregate
{
    public sealed class LessonDetails
    {
        public string Name { get; private set; }
        public string Language { get; private set; }
        public string Description { get; private set; }
        public double Price { get; private set; }
        public string OrderId { get; private set; }
        public string PackageId { get; private set; }
    }
}
