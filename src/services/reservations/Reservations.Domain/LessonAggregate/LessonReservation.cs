﻿using System.Collections.Generic;
using System.Linq;

namespace Reservations.Domain.LessonAggregate
{
    public class LessonReservation
    {
        private readonly List<ChangeRequest> _changeRequests;

        private LessonReservation()
        {
        }

        public string Id { get; private set; }
        public string PackageId { get; private set; }
        public Schedule Schedule { get; private set; }
        public LessonDetails LessonDetails { get; private set; }
        public Participiants Participiants { get; private set; }
        public IReadOnlyList<ChangeRequest> ChangeRequests => _changeRequests;
        public State State { get; private set; }  

        public void Confirm(string user)
        {
            if (HasPendingRequests())
            {
                throw new ReservationDomainException(
                    "Lesson confirmation does not allowed when lesson has pending requests", 
                    new DomainError[]
                    {
                        new DomainError()
                    });
            }

            if (!IsParticipiant(user))
            {
                throw new ReservationDomainException(
                    "Lesson confirmation does not allowed for non-participiants", 
                    new DomainError[]
                    {
                        new DomainError()
                    });
            }

            if (Participiants.IsTeacher(user) && State != State.PendingTutorConfirmation
                || Participiants.IsStudent(user) && State != State.PendingStudentConfirmation)
            {
                throw new ReservationDomainException(
                    "The student (or teacher) can't confirm lesson if lesson state != pending", 
                    new DomainError[]
                    {
                        new DomainError()
                    }); 
            }

            State = State.Confirmed; 
        }

        public void Decline(string user)
        {
            if (HasPendingRequests())
            {
                throw new ReservationDomainException(
                    "Lesson rejection does not allowed when lesson has pending requests",
                    new DomainError[]
                    {
                        new DomainError()
                    });
            }

            if (!IsParticipiant(user))
            {
                throw new ReservationDomainException(
                    "Lesson rejection does not allowed for non-participiants",
                    new DomainError[]
                    {
                        new DomainError()
                    });
            }

            if (Participiants.IsTeacher(user) && State != State.PendingTutorConfirmation
                || Participiants.IsStudent(user) && State != State.PendingStudentConfirmation)
            {
                throw new ReservationDomainException(
                    "The student (or teacher) can't decline lesson if lesson state != pending",
                    new DomainError[]
                    {
                        new DomainError()
                    });
            }
        }

        private bool HasPendingRequests()
        {
            return ChangeRequests.All(e => !e.IsActive());
        }

        private bool IsParticipiant(string user)
        {
            return Participiants.IsStudent(user)
                || Participiants.IsTeacher(user);
        }
    }
}
