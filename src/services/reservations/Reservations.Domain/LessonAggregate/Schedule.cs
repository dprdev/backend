﻿using System;

namespace Reservations.Domain.LessonAggregate
{
    public class Schedule
    {
        public DateTime Date { get; init; }
        public TimeSpan BeginTime { get; init; }
        public TimeSpan CompleteTime { get; init; }
    }
}
