﻿using System;
using System.Threading;
using System.Threading.Tasks;

using MassTransit;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Reservations.Domain;
using Reservations.Domain.LessonAggregate;

namespace Reservations.Application.Lessons.Commands.DeclineLesson
{
    internal sealed class DeclineLessonRequestHandler
        : IRequestHandler<DeclineLessonRequest, DeclineLessonResponse>
    {
        private readonly IBusControl _messageBus;
        private readonly DataContext _context;

        public DeclineLessonRequestHandler(DataContext context, IBusControl bus)
        {
            if (context is null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (bus is null)
            {
                throw new ArgumentNullException(nameof(bus));
            }

            _context = context;
            _messageBus = bus;
        }

        public async Task<DeclineLessonResponse> Handle(
            DeclineLessonRequest request, CancellationToken cancellationToken)
        {
            LessonReservation lesson = await GetLesson(request.Lesson);
            if (lesson is null)
            {
            }

            try
            {
                lesson.Decline(request.User);
            }
            catch (ReservationDomainException ex)
            {
                return new(ex);
            }

            try
            {
                await _context.SaveChangesAsync(cancellationToken);
            }
            catch (Exception ex)
            {
                return new(ex);
            }

            return DeclineLessonResponse.Success;
        }

        private Task<LessonReservation> GetLesson(string id)
        {
            return _context.LessonReservations.FirstOrDefaultAsync(e => e.Id == id);
        }
    }
}
