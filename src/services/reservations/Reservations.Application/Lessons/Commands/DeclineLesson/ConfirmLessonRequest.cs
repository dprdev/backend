﻿using System;

using MediatR;

namespace Reservations.Application.Lessons.Commands.DeclineLesson
{
    public sealed class DeclineLessonRequest : IRequest<DeclineLessonResponse>
    {
        public DeclineLessonRequest(string user, string lesson)
        {
            if (string.IsNullOrEmpty(user))
            {
                throw new ArgumentException($"'{nameof(user)}' cannot be null or empty.", nameof(user));
            }

            if (string.IsNullOrEmpty(lesson))
            {
                throw new ArgumentException($"'{nameof(lesson)}' cannot be null or empty.", nameof(lesson));
            }

            User = user;
            Lesson = lesson;
        }

        public string User { get; private set; }
        public string Lesson { get; private set; }
    }
}
