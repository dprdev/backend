﻿
using System;

using OneOf;
using OneOf.Types;

namespace Reservations.Application.Lessons.Commands.DeclineLesson
{
    public sealed class DeclineLessonResponse : OneOfBase<Success, Exception>
    {
        public DeclineLessonResponse(OneOf<Success, Exception> input) : base(input)
        {
        }


        public static DeclineLessonResponse Success { get; } = new(new Success()); 
    }
}
