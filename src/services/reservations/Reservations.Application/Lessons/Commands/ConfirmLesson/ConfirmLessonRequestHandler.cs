﻿using System;
using System.Threading;
using System.Threading.Tasks;

using MassTransit;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Reservations.Domain;
using Reservations.Domain.LessonAggregate;

namespace Reservations.Application.Lessons.Commands.ConfirmLesson
{
    internal sealed class ConfirmLessonRequestHandler
        : IRequestHandler<ConfirmLessonRequest, ConfirmLessonResponse>
    {
        private readonly IBusControl _messageBus;
        private readonly DataContext _context;

        public ConfirmLessonRequestHandler(DataContext context, IBusControl bus)
        {
            if (context is null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (bus is null)
            {
                throw new ArgumentNullException(nameof(bus));
            }

            _context = context;
            _messageBus = bus;
        }

        public async Task<ConfirmLessonResponse> Handle(
            ConfirmLessonRequest request, CancellationToken cancellationToken)
        {
            LessonReservation lesson = await GetLesson(request.Lesson);
            if (lesson is null)
            {
            }

            try
            {
                lesson.Confirm(request.User);
            }
            catch (ReservationDomainException ex)
            {
                return new(ex);
            }

            try
            {
                await _context.SaveChangesAsync(cancellationToken);
            }
            catch (Exception ex)
            {
                return new(ex);
            }

            return ConfirmLessonResponse.Success;
        }

        private Task<LessonReservation> GetLesson(string id)
        {
            return _context.LessonReservations.FirstOrDefaultAsync(e => e.Id == id);
        }
    }
}
