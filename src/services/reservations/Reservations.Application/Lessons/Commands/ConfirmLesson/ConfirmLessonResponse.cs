﻿
using System;

using OneOf;
using OneOf.Types;

namespace Reservations.Application.Lessons.Commands.ConfirmLesson
{
    public sealed class ConfirmLessonResponse : OneOfBase<Success, Exception>
    {
        public ConfirmLessonResponse(OneOf<Success, Exception> input) : base(input)
        {
        }


        public static ConfirmLessonResponse Success { get; } = new(new Success()); 
    }
}
