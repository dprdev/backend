﻿
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace Reservations.Infrastructure.EfCore
{
    internal static class HostBuilderExtensions
    {
        public static T GetParameter<T>(this WebHostBuilderContext host, string name)
        {
            return host.Configuration.GetValue<T>(name);
        }
    }
}
