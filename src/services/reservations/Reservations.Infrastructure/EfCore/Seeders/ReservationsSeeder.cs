﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Reservations.Domain;

namespace Reservations.Infrastructure.EfCore.Seeders
{
    internal sealed class ReservationsSeeder : ISeeder
    {
        private readonly DataContext _context;

        public ReservationsSeeder(DataContext context)
        {
            if (context is null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            _context = context;
        }

        public async Task Seed()
        {
            var items = new Dictionary<string, HashSet<(DateTime, DateTime)>>();
            string[] teachers = Enumerable.Range(0, 100).Select(e => Guid.NewGuid().ToString()).ToArray();
            string[] students = Enumerable.Range(0, 1000).Select(e => Guid.NewGuid().ToString()).ToArray();
            for (int j = 0; j < teachers.Length; j++)
            {
                string teacher = teachers[j];
                DateTime s = DateTime.Today;
                for (int i = 1; i <= 100; i++)
                {
                    DateTime e = s.AddHours(1);
                    //var reservation = new LessonReservation(
                    //    s.Date, s.TimeOfDay, e.TimeOfDay, new(), Guid.NewGuid().ToString(), TryGetUser(s, e), teacher);

                    //_context.Add(reservation);

                    s = e;
                }
            }
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception)
            {
            }

            string TryGetUser(DateTime s, DateTime e)
            {
                var rnd = new Random();
                while (true)
                {
                    string val = students[rnd.Next(0, 1000)];
                    if (items.TryGetValue(val, out HashSet<(DateTime, DateTime)> dates))
                    {
                        if (dates.Contains((s, e)))
                        {
                            continue;
                        }

                        dates.Add((s, e));
                    }
                    else
                    {
                        items.Add(val, new()
                        {
                            (s, e)
                        });
                    }

                    return val;
                }
            }
        }
    }
}
