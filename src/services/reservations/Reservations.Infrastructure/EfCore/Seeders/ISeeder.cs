﻿using System.Threading.Tasks;

namespace Reservations.Infrastructure.EfCore.Seeders
{
    internal interface ISeeder
    {
        Task Seed();
    }
}
