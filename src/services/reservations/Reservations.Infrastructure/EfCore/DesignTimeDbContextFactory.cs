﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Infrastructure;

using Reservations.Domain;

namespace Reservations.Infrastructure.EfCore
{
    internal sealed class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<DataContext>
    {
        DataContext IDesignTimeDbContextFactory<DataContext>.CreateDbContext(string[] args)
        {
            DbContextOptionsBuilder<DataContext> optBuilder
                = new DbContextOptionsBuilder<DataContext>()
                    .ReplaceService<IModelCustomizer, CustomModelCustomizer>()
                    .UseNpgsql((cfg) =>
                    {
                        cfg.MigrationsAssembly("Reservations.Infrastructure");
                    });

            return new DataContext(optBuilder.Options);
        }
    }
}
