﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using Reservations.Domain.LessonAggregate;

namespace Reservations.Infrastructure.EfCore.EntityTypeConfigurations
{
    internal sealed class LessonReservationConfiguration : IEntityTypeConfiguration<LessonReservation>
    { 
        public void Configure(
            EntityTypeBuilder<LessonReservation> builder)
        {
            builder.HasKey(e => e.Id);
            builder.OwnsOne(e => e.LessonDetails, cfg =>
            {
                cfg.Property(e => e.PackageId).HasColumnName("PackageId"); 
            });

            builder.OwnsOne(e => e.Participiants, cfg =>
            {
            });

            builder.OwnsOne(e => e.Schedule, cfg =>
            {
            });

            builder.Property(e => e.ChangeRequests)
                .HasColumnType("jsonb");

            builder.ToTable("LessonReservations");
        } 
    }
}
