﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using Reservations.Domain.PackageAggregate;

namespace Reservations.Infrastructure.EfCore.EntityTypeConfigurations
{
    internal sealed class PackageReservationConfiguration : IEntityTypeConfiguration<PackageReservation>
    {
        public void Configure(
            EntityTypeBuilder<PackageReservation> builder)
        {
            builder.HasKey(e => e.Id);

            builder.Property(e => e.StudentId).IsRequired();

            builder.HasMany(e => e.Reservations)
                .WithOne()
                .HasForeignKey(e => e.PackageId);

            builder.OwnsOne(e => e.PackagePrice, cfg =>
            {
            });

            builder.Property(e => e.OrderId).IsRequired(); 
            builder.Property(e => e.TeacherId).IsRequired();

            builder.HasIndex(e => e.StudentId);
        }
    }
}
