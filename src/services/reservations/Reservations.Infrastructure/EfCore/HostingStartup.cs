﻿
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using Reservations.Domain;

namespace Reservations.Infrastructure.EfCore
{
    public sealed class HostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((host, services) =>
            {
                services.AddDbContextPool<DataContext>((opt) => ConfigureContext(opt, host));
                services.AddHostedService<MigrateDatabaseHostedService>();
                if (bool.TryParse(
                    host.Configuration.GetValue<string>("seed"),
                    out bool result) && result)
                {
                    services.AddHostedService<SeedDatabaseHostedService>();
                }
            });
        }

        private static void ConfigureContext(DbContextOptionsBuilder options, WebHostBuilderContext host)
        {
            options.ReplaceService<IModelCustomizer, CustomModelCustomizer>();
            options.UseNpgsql(
                host.GetParameter<string>("db"),
                opt =>
                {
                    opt.MigrationsAssembly("Reservations.Infrastructure");
                });
        }
    }
}
