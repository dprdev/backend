﻿using System.Reflection;

using MediatR;

using Microsoft.AspNetCore.Hosting;

namespace Reservations.Infrastructure.MediatR
{
    public sealed class HostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((services) =>
            {
                services.AddMediatR(GetAssemblies());
            });
        }

        private static Assembly[] GetAssemblies()
        {
            return new Assembly[]
            {
                Assembly.Load("Reservations.Domain"),
                Assembly.Load("Reservations.Application"),
            };
        }
    }
}
