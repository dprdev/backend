﻿
using System.Reflection;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

// configure sentry first
[assembly: HostingStartup(typeof(Notifications.Api.Infrastructure.Sentry.HostingStartup))]
[assembly: HostingStartup(typeof(Notifications.Api.Infrastructure.EfCore.HostingStartup))]
[assembly: HostingStartup(typeof(Notifications.Api.Infrastructure.Masstransit.HostingStartup))]
//[assembly: HostingStartup(typeof(Notifications.Api.Infrastructure.Authentication.HostingStartup))]
[assembly: HostingStartup(typeof(Notifications.Api.Infrastructure.MediatR.HostingStartup))]

IHostBuilder builder = Host
    .CreateDefaultBuilder(args)
    .ConfigureWebHostDefaults(webBuilder =>
    {
        webBuilder.ConfigureServices((host, services) =>
        {
            services.AddAuthorization();
            services.AddAuthentication();
            services.AddDefaultApiVersioning();
            services.AddControllers()
                .AddDefaultFluentValidation((cfg) =>
                {
                    cfg.RegisterValidatorsFromAssembly(Assembly.Load("Notifications.Api"));
                    cfg.RegisterValidatorsFromAssembly(Assembly.Load("Notifications.Api.Application"));
                    cfg.RegisterValidatorsFromAssembly(Assembly.Load("Notifications.Api.Domain"));
                });
        });


        webBuilder.Configure((host, app) =>
        {
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        });
    });

await builder.StartAsync();
