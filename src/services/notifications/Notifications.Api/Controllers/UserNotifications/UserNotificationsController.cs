﻿using System;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc;

namespace Notifications.Api.Controllers.UserNotifications
{
    [JwtAuthorize]
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/user/{id}/notifications")]
    public class NotificationsController : Controller
    {
        [HttpGet]
        public Task<IActionResult> GetUserNotifications()
        {
            throw new NotImplementedException(nameof(GetUserNotifications));
        }
    }
}
