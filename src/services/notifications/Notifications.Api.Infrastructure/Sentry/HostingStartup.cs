﻿
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace Notifications.Api.Infrastructure.Sentry
{
    /// <summary>
    /// Configure sentry
    /// </summary>
    public sealed class HostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.UseSentry((host, o) =>
            {
                o.Dsn = host.Configuration.GetValue<string>("sentry");
                if (host.HostingEnvironment.IsDevelopment())
                {
                    o.Debug = true;
                }
            });
        }
    }
}
