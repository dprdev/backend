﻿using MassTransit;
using MassTransit.ExtensionsDependencyInjectionIntegration;

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using Notifications.Api.Infrastructure.Masstransit.Configurations;

namespace Notifications.Api.Infrastructure.Masstransit
{
    internal delegate void Configure(
        IConfiguration cfg,
        IWebHostEnvironment env,
        IServiceCollection services,
        IServiceCollectionBusConfigurator mtCfg);

    /// <summary>
    /// TODO: configure <see cref="HandleManagerCreatedConsumer"/> consumer
    /// </summary>
    public sealed class HostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((host, services) =>
            {
                services.AddMassTransitHostedService();
                services.AddMassTransit((mtCfg) =>
                { 
                    if (host.HostingEnvironment.IsDevelopment())
                    {
                        MassTransitRabbitConfiguration.Configure(host.Configuration, host.HostingEnvironment, services, mtCfg);
                    }
                    else
                    {
                        MassTransitAzureConfiguration.Configure(host.Configuration, host.HostingEnvironment, services, mtCfg);

                    }
                });
            });
        }
    }
}
