﻿using System.Reflection;

using MediatR;

using Microsoft.AspNetCore.Hosting;

namespace Notifications.Api.Infrastructure.MediatR
{
    public sealed class HostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((services) => services.AddMediatR(GetAssemblies()));
        }

        private static Assembly[] GetAssemblies()
        {
            return new Assembly[]
            {
                Assembly.Load("Notifications.Api.Domain"),
                Assembly.Load("Notifications.Api.Application"),
            };
        }
    }
}
