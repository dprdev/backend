﻿using System.Threading.Tasks;

namespace Notifications.Api.Infrastructure.EfCore.Seeders
{
    internal interface ISeeder
    {
        Task Seed();
    }
}
