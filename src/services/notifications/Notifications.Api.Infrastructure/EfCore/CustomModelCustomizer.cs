﻿
using System.Reflection;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace Notifications.Api.Infrastructure.EfCore
{
    internal sealed class CustomModelCustomizer : ModelCustomizer
    {
        public CustomModelCustomizer(
            ModelCustomizerDependencies dependencies)
            : base(dependencies)
        {
        }

        public override void Customize(
            ModelBuilder modelBuilder,
            DbContext context)
        {
            base.Customize(modelBuilder, context);
            modelBuilder.ApplyConfigurationsFromAssembly(
                Assembly.GetExecutingAssembly());
        }
    }
}
