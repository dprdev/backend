﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

using Notifications.Api.Domain;

namespace Notifications.Api.Infrastructure.EfCore
{
    internal sealed class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<DataContext>
    {
        DataContext IDesignTimeDbContextFactory<DataContext>.CreateDbContext(string[] args)
        {
            DbContextOptionsBuilder<DataContext> optBuilder
                = new DbContextOptionsBuilder<DataContext>()
                    .UseNpgsql((cfg) =>
                    {
                        cfg.MigrationsAssembly("Notifications.Api.Infrastructure");
                    });

            return new DataContext(optBuilder.Options);
        }
    }
}
