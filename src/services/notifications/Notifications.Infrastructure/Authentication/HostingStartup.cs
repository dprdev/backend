﻿
using System.Threading.Tasks;

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Primitives;

namespace Notifications.Infrastructure.Authentication
{
    /// <summary>
    /// Configure sentry
    /// </summary>
    public sealed class HostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((host, services) =>
            {
                services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(cfg =>
                    {
                        cfg.Authority = host.Configuration.GetValue<string>("identityUrl");
                        cfg.Events = new()
                        {
                            OnMessageReceived = ctx =>
                            {
                                PathString path = ctx.HttpContext.Request.Path;
                                StringValues token = ctx.Request.Query["access_token"];

                                if (!string.IsNullOrEmpty(token) &&
                                    path.StartsWithSegments("/hubs"))
                                {
                                    ctx.Token = token;
                                }

                                return Task.CompletedTask;
                            }
                        };
                    });
            });
        }
    }
}
