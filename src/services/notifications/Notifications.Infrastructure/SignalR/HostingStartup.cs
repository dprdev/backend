﻿
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;

namespace Notifications.Infrastructure.SignalR
{
    /// <summary>
    /// Configure sentry
    /// </summary>
    public sealed class HostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((services) =>
            {
                services.AddSignalR();
                services.AddSingleton<IUserIdProvider, UserIdProvider>(); 
            });
        }
    }
}
