﻿
using System.Security.Claims;

using Microsoft.AspNetCore.SignalR;

namespace Notifications.Infrastructure.SignalR
{
    internal sealed class UserIdProvider : IUserIdProvider
    {
        public string GetUserId(HubConnectionContext connection)
        {
            return connection.User.FindFirstValue(ClaimTypes.NameIdentifier);
        }
    }
}
