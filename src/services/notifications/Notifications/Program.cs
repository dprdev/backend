﻿
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using Notifications.Hubs;

[assembly: HostingStartup(typeof(Notifications.Infrastructure.Sentry.HostingStartup))]
[assembly: HostingStartup(typeof(Notifications.Infrastructure.Masstransit.HostingStartup))]
[assembly: HostingStartup(typeof(Notifications.Infrastructure.SignalR.HostingStartup))]
[assembly: HostingStartup(typeof(Notifications.Infrastructure.Authentication.HostingStartup))]

IHostBuilder builder = Host
    .CreateDefaultBuilder(args)
    .ConfigureWebHostDefaults(webBuilder =>
    { 
        webBuilder.Configure((host, app) =>
        {
            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHub<NotificationHub>("/notifications");
            });
        });
    });

await builder.StartAsync();
