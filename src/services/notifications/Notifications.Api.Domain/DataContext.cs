﻿using System.Diagnostics.CodeAnalysis;

using Microsoft.EntityFrameworkCore;

using Notifications.Api.Domain.Entities;

namespace Notifications.Api.Domain
{
    public class DataContext : DbContext
    {
        protected DataContext()
        {
        }

        public DataContext([NotNull] DbContextOptions<DataContext> options) : base(options)
        {
        }

        public virtual DbSet<Notification> Notifications { get; private set; }
    }
}
