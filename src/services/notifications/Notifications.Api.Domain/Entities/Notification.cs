﻿using System;

namespace Notifications.Api.Domain.Entities
{
    public class Notification
    {
        public string Id { get; private set; }
        public string UserId { get; private set; }
        public DateTime CreatedDate { get; private set; }

        public bool IsReaded { get; private set; }
    }
}
