﻿
using Identity.Domain.Entities;

using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Identity.Domain
{
    public sealed class DataContext
        : IdentityDbContext<LinguaUser, LinguaRole, string>
    {
        private DataContext()
        {
        }

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }
    }
}
