﻿
using System;

using Microsoft.AspNetCore.Identity;

namespace Identity.Domain.Entities
{
    public class LinguaUser : IdentityUser
    {
        private LinguaUser()
        {
        }

        public LinguaUser(string id, string email) : this(email)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentException($"'{nameof(id)}' cannot be null or empty.", nameof(id));
            }

            if (string.IsNullOrEmpty(email))
            {
                throw new ArgumentException($"'{nameof(email)}' cannot be null or empty.", nameof(email));
            }

            Id = id;
        }

        public LinguaUser(string email) : base(email)
        {
            Email = email;
            UserName = email.Substring(0, email.IndexOf('@')); 
        }

        public string Name { get; private set; }
        public string Surname { get; private set; }

        public Gender Gender { get; private set; }
        public DateTime DateOfBirth { get; private set; }
        public string Country { get; private set; }
        public string LivingCountry { get; private set; }
        public string TimeZone { get; private set; }
        public string AvatarUrl { get; private set; }
        public GdprCookie GDPRCookie { get; private set; }

        public void UpdateGdpr(GdprCookie gdprCookie)
        {
            GDPRCookie = gdprCookie; 
        }
    }
}
