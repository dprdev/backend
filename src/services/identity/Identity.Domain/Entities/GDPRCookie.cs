﻿namespace Identity.Domain.Entities
{
    public enum GdprCookie
    {
        Pending, 
        CookieRejected, 
        CookieAccepted
    }
}
