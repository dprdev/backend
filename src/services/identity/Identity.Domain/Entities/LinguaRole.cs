﻿
using System;

using Microsoft.AspNetCore.Identity;

namespace Identity.Domain.Entities
{
    public class LinguaRole : IdentityRole
    {
        private LinguaRole()
        {
        }

        public LinguaRole(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentException($"'{nameof(name)}' cannot be null or empty.", nameof(name));
            }

            Name = name;
        }
    }
}
