﻿namespace Identity.Domain.Entities
{
    public enum Gender
    {
        Unset = 0,
        Female = 10,
        Male = 20,
    }
}
