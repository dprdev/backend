﻿
using System.Reflection;

using Hellang.Middleware.ProblemDetails;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

// configure sentry first
[assembly: HostingStartup(typeof(Identity.Infrastructure.Sentry.HostingStartup))]
[assembly: HostingStartup(typeof(Identity.Infrastructure.EfCore.HostingStartup))]
[assembly: HostingStartup(typeof(Identity.Infrastructure.Masstransit.HostingStartup))]
[assembly: HostingStartup(typeof(Identity.Infrastructure.Authentication.HostingStartup))]
[assembly: HostingStartup(typeof(Identity.Infrastructure.MediatR.HostingStartup))]

IHostBuilder builder = Host
    .CreateDefaultBuilder(args)
    .ConfigureWebHostDefaults(webBuilder =>
    {
        webBuilder.ConfigureServices((host, services) =>
        {
            services.AddProblemDetails();
            services.AddDefaultApiVersioning();
            services.AddControllers()
                .AddDefaultFluentValidation((cfg) =>
                {
                    cfg.RegisterValidatorsFromAssembly(Assembly.Load("Identity"));
                    cfg.RegisterValidatorsFromAssembly(Assembly.Load("Identity.Application"));
                    cfg.RegisterValidatorsFromAssembly(Assembly.Load("Identity.Domain"));
                });
        });

        webBuilder.Configure((host, app) =>
        {
            app.UseProblemDetails();
            app.UseRouting();
            app.UseIdentityServer();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        });
    });

await builder.StartAsync();
