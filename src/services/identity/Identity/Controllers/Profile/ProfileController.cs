﻿
using System;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;

using Identity.Application.Profile.Commands.UpdateGdpr;
using Identity.Application.Profile.Queries.GetProfile;
using Identity.Domain.Entities;

using MediatR;

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc;

namespace Identity.Controllers.Profile
{
    [JwtAuthorize]
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/profile")]
    public sealed class ProfileController : ApiController
    {
        [HttpGet]
        public async Task<IActionResult> Get([FromServices] IMediator mediator)
        {
            GetProfileResponse result = await mediator.Send(new GetProfileRequest(User.GetId()));
            return result.Match<IActionResult>(
                profile => Json(profile),
                exception =>
                {
                    return Problem(statusCode: (int)HttpStatusCode.InternalServerError);
                });
        }


        // TODO: exception handling
        [HttpPost("gdpr")]
        public async Task<IActionResult> GDPR(
            [FromServices] IMediator mediator,
            [FromQuery(Name = "gdpr-cookie")] GdprCookie gdpr)
        {
            try
            {
                UpdateGdprResponse result =
                    await mediator.Send(new UpdateGdprRequest(User.GetId(), gdpr));
            }
            catch (Exception)
            {
            }

            return Ok(new
            {

            });
        }
    }
}
