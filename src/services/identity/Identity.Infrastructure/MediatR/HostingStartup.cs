﻿using System.Reflection;

using MediatR;

using Microsoft.AspNetCore.Hosting;

namespace Identity.Infrastructure.MediatR
{
    public sealed class HostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((services) => services.AddMediatR(GetAssemblies()));
        }

        private static Assembly[] GetAssemblies()
        {
            return new Assembly[]
            {
                Assembly.Load("Identity.Domain"),
                Assembly.Load("Identity.Application"),
            };
        }
    }
}
