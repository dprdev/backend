﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Identity.Domain.Entities;

using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;

#pragma warning disable IDE0052 // Remove unread private members

namespace Identity.Infrastructure.EfCore.Seeders
{
    internal sealed class UserSeeder : ISeeder
    {
        private readonly ILogger<UserSeeder> _logger;
        private readonly UserManager<LinguaUser> _userManager;

        public UserSeeder(UserManager<LinguaUser> userManager, ILogger<UserSeeder> logger)
        {
            if (userManager is null)
            {
                throw new ArgumentNullException(nameof(userManager));
            }

            if (logger is null)
            {
                throw new ArgumentNullException(nameof(logger));
            }

            _userManager = userManager;
            _logger = logger;
        }

        // TODO: add logging
        public async Task Seed()
        {
            foreach ((LinguaUser user, string role) in GetUsers())
            {
                try
                {
                    IdentityResult result = await _userManager.CreateAsync(user, "medoge343");
                    if (result.Succeeded)
                    {
                        await _userManager.AddToRoleAsync(user, role);
                    }
                }
                catch (Exception)
                {
                }
            }
        }


        private static IEnumerable<(LinguaUser user, string role)> GetUsers()
        {
            yield return (new("885bcb02-cf5b-4dfb-9f2c-72c686a27727", "student@gmail.com"), "student");
            yield return (new("f7278620-fd6a-4c61-bc72-07bfc7f687e7", "teacher@gmail.com"), "teacher");
        }
    }
}
