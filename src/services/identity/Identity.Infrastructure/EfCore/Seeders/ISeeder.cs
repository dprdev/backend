﻿using System.Threading.Tasks;

namespace Identity.Infrastructure.EfCore.Seeders
{
    internal interface ISeeder
    {
        Task Seed();
    }
}
