﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Identity.Domain.Entities;

using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;

namespace Identity.Infrastructure.EfCore.Seeders
{
    internal sealed class RoleSeeder : ISeeder
    {
        private readonly ILogger<RoleSeeder> _logger;
        private readonly RoleManager<LinguaRole> _roleManager;

        public RoleSeeder(RoleManager<LinguaRole> roleManager, ILogger<RoleSeeder> logger)
        {
            if (roleManager is null)
            {
                throw new ArgumentNullException(nameof(roleManager));
            }

            if (logger is null)
            {
                throw new ArgumentNullException(nameof(logger));
            }

            _roleManager = roleManager;
            _logger = logger;
        }

        public async Task Seed()
        {
            foreach (string role in GetRoles())
            {
                try
                {
                    await _roleManager.CreateAsync(new(role));
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"An error occured in role seeder ({role})");
                }
            }
        }

        private static IEnumerable<string> GetRoles()
        {
            yield return "student";
            yield return "teacher";
            yield return "partner";
        }
    }
}
