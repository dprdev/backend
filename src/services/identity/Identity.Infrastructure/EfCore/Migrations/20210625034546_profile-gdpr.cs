﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Identity.Infrastructure.EfCore.Migrations
{
    public partial class profilegdpr : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "GDPRCookie",
                table: "AspNetUsers",
                type: "integer",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GDPRCookie",
                table: "AspNetUsers");
        }
    }
}
