﻿
using Identity.Domain;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Identity.Infrastructure.EfCore
{
    internal sealed class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<DataContext>
    {
        DataContext IDesignTimeDbContextFactory<DataContext>.CreateDbContext(string[] args)
        {
            DbContextOptionsBuilder<DataContext> optBuilder
                = new DbContextOptionsBuilder<DataContext>()
                    .UseNpgsql((cfg) =>
                    {
                        cfg.MigrationsAssembly("Identity.Infrastructure");
                    });

            return new DataContext(optBuilder.Options);
        }
    }
}
