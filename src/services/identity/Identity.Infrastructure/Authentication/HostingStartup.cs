﻿using System.Collections.Generic;

using Identity.Domain;
using Identity.Domain.Entities;

using IdentityModel;

using IdentityServer4.Models;

using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;

namespace Identity.Infrastructure.Authentication
{
    public sealed class HostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((host, services) =>
            {
                ConfigureIdentity(services);
                ConfigureIdentityServer(services);
            });
        }

        private static void ConfigureIdentity(IServiceCollection services)
        { 
            IdentityBuilder identityBuilder =
                services.AddIdentity<LinguaUser, LinguaRole>(cfg =>
                {
                    cfg.User.RequireUniqueEmail = true;
                    cfg.User.AllowedUserNameCharacters =
                        "абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪ" +
                        "ЫЬЭЮЯabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@\u002B";

                    cfg.Password.RequiredLength = 6;
                    cfg.Password.RequiredUniqueChars = 1;
                    cfg.Password.RequireNonAlphanumeric = false;
                    cfg.Password.RequireUppercase = false;
                    cfg.Password.RequireLowercase = false;
                    cfg.Password.RequireDigit = true;

                    cfg.SignIn.RequireConfirmedAccount = false;
                    cfg.SignIn.RequireConfirmedPhoneNumber = false;
                    cfg.SignIn.RequireConfirmedEmail = false;
                });

            identityBuilder.AddEntityFrameworkStores<DataContext>();
            identityBuilder.AddDefaultTokenProviders();
        }

        private static void ConfigureIdentityServer(IServiceCollection services)
        {
            IIdentityServerBuilder builder =
                services.AddIdentityServer(options =>
                {
                });

            builder.AddInMemoryClients(GetClients());
            builder.AddInMemoryIdentityResources(GetIdentityResources());
            builder.AddInMemoryApiScopes(GetApiScopes()); 

            builder.AddSigninCredentials();
            builder.AddAspNetIdentity<LinguaUser>();

            static IEnumerable<ApiScope> GetApiScopes()
            {
                yield return new ApiScope("api", new string[] { "role" });
            }

            static IEnumerable<IdentityResource> GetIdentityResources()
            {
                yield return new IdentityResources.OpenId();
                yield return new IdentityResources.Profile();
                yield return new IdentityResources.Email();
                yield return new()
                {
                    Name = "roles",
                    UserClaims =
                    {
                        JwtClaimTypes.Role
                    }
                }; 
            }

            static IEnumerable<Client> GetClients()
            {
                yield return new Client()
                {
                    ClientId = "moderation",
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                    RequireClientSecret = true,
                    AllowOfflineAccess = true,
                    ClientSecrets =
                    {
                        new Secret("moderation-secret".ToSha256())
                    },
                    AllowedScopes =
                    {
                        "openid",
                        "profile",
                        "api"
                    }
                };
                yield return new Client()
                {
                    ClientId = "spa",
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                    RequireClientSecret = false,
                    AllowOfflineAccess = true,
                    AlwaysSendClientClaims = true,
                    ClientSecrets =
                    {
                        new Secret("spa-secret".ToSha256())
                    },

                    AllowedScopes =
                    {
                        "roles",
                        "openid",
                        "profile",
                        "api"
                    }
                };
            }
        }

    }
}
