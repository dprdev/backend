﻿
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Identity.Infrastructure.Authentication
{
    internal static class IdentityServerBuilderExtensions
    {
        public static IIdentityServerBuilder AddSigninCredentials(this IIdentityServerBuilder builder)
        {
            IWebHostEnvironment env =
                builder.Services.BuildServiceProvider()
                       .GetRequiredService<IWebHostEnvironment>();

            if (env.IsDevelopment())
            {
                builder.AddDeveloperSigningCredential();
            }


            return builder;
        }
    }
}
