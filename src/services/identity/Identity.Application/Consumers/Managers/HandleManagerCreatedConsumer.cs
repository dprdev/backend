﻿using System;
using System.Threading.Tasks;

using Contracts.Events.Managers;

using MassTransit;

namespace Identity.Application.Consumers.Managers
{
    public sealed class HandleManagerCreatedConsumer : IConsumer<NotifyManagerCreated>
    {
        public async Task Consume(
            ConsumeContext<NotifyManagerCreated> context)
        {
            throw new NotImplementedException();
        }
    }
}
