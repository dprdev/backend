﻿using System;
using System.Threading;
using System.Threading.Tasks;

using Identity.Domain;

using MediatR;

using Microsoft.AspNetCore.Identity;

namespace Identity.Application.Roles.Commands.CreateRole
{
    internal sealed class CreateRoleRequestHandler : IRequestHandler<CreateRoleRequest, CreateRoleResponse>
    {
        private readonly DataContext _context;

        public CreateRoleRequestHandler(DataContext context)
        {
            if (context is null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            _context = context;
        }

        public async Task<CreateRoleResponse> Handle(
            CreateRoleRequest request, CancellationToken cancellationToken)
        {
            IdentityRole role = _context.Roles.Add(new(request.Name)).Entity;
            try
            {
                await _context.SaveChangesAsync(cancellationToken);
            }
            catch (Exception) // TODO: handle 
            {
                throw;
            }

            return new(new Role
            {
                Id = role.Id,
                Name = role.Name
            });
        }
    }
}
