﻿using System;

using OneOf;

namespace Identity.Application.Roles.Commands.CreateRole
{
    public sealed class CreateRoleResponse : OneOfBase<Role, Exception>
    {
        public CreateRoleResponse(OneOf<Role, Exception> input) : base(input)
        {
        }
    }
}
