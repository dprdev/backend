﻿using System;

using MediatR;

namespace Identity.Application.Roles.Commands.CreateRole
{

    public sealed class CreateRoleRequest : IRequest<CreateRoleResponse>
    {
        public CreateRoleRequest(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentException($"'{nameof(name)}' cannot be null or empty.", nameof(name));
            }

            Name = name;
        }

        public string Name { get; private set; }
    }
}
