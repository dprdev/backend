﻿namespace Identity.Application.Roles.Commands.CreateRole
{
    public sealed class Role
    {
        public string Id { get; init; }
        public string Name { get; init; }
    }
}
