﻿using System;
using System.Collections.Generic;

namespace Identity.Application.Roles.Queries.GetRoles
{
    public sealed class GetRolesResponse
    {
        public GetRolesResponse(IReadOnlyList<Role> roles)
        {
            if (roles is null)
            {
                throw new ArgumentNullException(nameof(roles));
            }

            Roles = roles;
        }

        public IReadOnlyList<Role> Roles { get; }
    }
}
