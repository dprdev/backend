﻿namespace Identity.Application.Roles.Queries.GetRoles
{
    public sealed class Role
    {
        public string Id { get; init; }
        public string Name { get; init; }
    }
}
