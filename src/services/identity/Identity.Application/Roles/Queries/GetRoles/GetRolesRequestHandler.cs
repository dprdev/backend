﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Identity.Domain;

using MediatR;

using Microsoft.EntityFrameworkCore;

namespace Identity.Application.Roles.Queries.GetRoles
{
    internal sealed class GetRolesRequestHandler : IRequestHandler<GetRolesRequest, GetRolesResponse>
    {
        private readonly DataContext _context;

        public GetRolesRequestHandler(DataContext context)
        {
            if (context is null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            _context = context;
        }

        public async Task<GetRolesResponse> Handle(
            GetRolesRequest request, CancellationToken cancellationToken)
        {
            IQueryable<Role> query =
                from role in _context.Roles.AsNoTracking()
                select new Role
                {
                    Id = role.Id,
                    Name = role.Name
                };

            return new(await query.ToListAsync(cancellationToken: cancellationToken));
        }
    }
}
