﻿
using MediatR;

namespace Identity.Application.Roles.Queries.GetRoles
{
    public sealed class GetRolesRequest : IRequest<GetRolesResponse>
    {
    }
}
