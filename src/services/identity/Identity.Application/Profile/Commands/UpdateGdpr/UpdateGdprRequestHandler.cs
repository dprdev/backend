﻿using System;
using System.Threading;
using System.Threading.Tasks;

using Identity.Domain;
using Identity.Domain.Entities;

using MediatR;

using Microsoft.EntityFrameworkCore;

namespace Identity.Application.Profile.Commands.UpdateGdpr
{
    internal sealed class UpdateGdprRequestHandler
        : IRequestHandler<UpdateGdprRequest, UpdateGdprResponse>
    {
        private readonly DataContext _context;

        public UpdateGdprRequestHandler(DataContext context)
        {
            if (context is null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            _context = context;
        }

        public async Task<UpdateGdprResponse> Handle(
            UpdateGdprRequest request, CancellationToken cancellationToken)
        {
            LinguaUser profile = await GetUser(request, cancellationToken);
            profile.UpdateGdpr(request.GdprCookie);
            try
            {
                await _context.SaveChangesAsync(cancellationToken);
            }
            catch (Exception)
            {
                throw;
            }

            return new();
        }

        private Task<LinguaUser> GetUser(UpdateGdprRequest request, CancellationToken cancellationToken)
        {
            return _context.Users.FirstOrDefaultAsync(e => e.Id == request.Id, cancellationToken: cancellationToken);
        }
    }
}
