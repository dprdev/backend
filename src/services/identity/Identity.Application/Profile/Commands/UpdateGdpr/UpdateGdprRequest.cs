﻿using Identity.Domain.Entities;

using MediatR;

namespace Identity.Application.Profile.Commands.UpdateGdpr
{
    public sealed class UpdateGdprRequest : IRequest<UpdateGdprResponse>
    {
        public UpdateGdprRequest(string id, GdprCookie gdprCookie)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new System.ArgumentException($"'{nameof(id)}' cannot be null or empty.", nameof(id));
            }

            Id = id;
            GdprCookie = gdprCookie;
        }

        public string Id { get; }
        public GdprCookie GdprCookie { get; }
    }
}
