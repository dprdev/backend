﻿using System;
using System.Threading;
using System.Threading.Tasks;

using Identity.Domain;
using Identity.Domain.Entities;

using MediatR;

using Microsoft.EntityFrameworkCore;

namespace Identity.Application.Profile.Commands.UpdateProfile
{
    internal sealed class UpdateProfileRequestHandler
        : IRequestHandler<UpdateProfileRequest, UpdateProfileResponse>
    {
        private readonly DataContext _context;

        public UpdateProfileRequestHandler(DataContext context)
        {
            if (context is null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            _context = context;
        }

        public async Task<UpdateProfileResponse> Handle(
            UpdateProfileRequest request, CancellationToken cancellationToken)
        {
            LinguaUser profile = await GetUser(request, cancellationToken);
            if (profile is null)
            {

            }

            throw null; 
        }

        private Task<LinguaUser> GetUser(UpdateProfileRequest request, CancellationToken cancellationToken)
        {
            return _context.Users.FirstOrDefaultAsync(e => e.Id == request.Id, cancellationToken: cancellationToken);
        }
    }
}
