﻿
using System;

using Identity.Domain.Entities;

using MediatR;

namespace Identity.Application.Profile.Commands.UpdateProfile
{
    public sealed class UpdateProfileRequest : IRequest<UpdateProfileResponse>
    {
        public string Id { get; private set; }
        public string Name { get; private set; }
        public string Surname { get; private set; }
        public Gender Gender { get; private set; }
        public DateTime DateOfBirth { get; private set; }
        public string Country { get; private set; }
        public string LivingCountry { get; private set; }
        public string TimeZone { get; private set; }
        public string AvatarUrl { get; private set; }
    }
}
