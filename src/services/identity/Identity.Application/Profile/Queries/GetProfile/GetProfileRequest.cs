﻿using System;

using MediatR;

namespace Identity.Application.Profile.Queries.GetProfile
{
    public sealed class GetProfileRequest : IRequest<GetProfileResponse>
    {
        public GetProfileRequest(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentException($"'{nameof(id)}' cannot be null or empty.", nameof(id));
            }

            Id = id;
        }

        public string Id { get; private set; }
    }
}
