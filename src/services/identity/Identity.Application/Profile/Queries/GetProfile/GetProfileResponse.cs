﻿using System;

using OneOf;

namespace Identity.Application.Profile.Queries.GetProfile
{
    public sealed class GetProfileResponse : OneOfBase<Profile, Exception>
    {
        public GetProfileResponse(OneOf<Profile, Exception> input) : base(input)
        {
        }
    }
}
