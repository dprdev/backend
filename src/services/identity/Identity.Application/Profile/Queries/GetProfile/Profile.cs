﻿using System;

using Identity.Domain.Entities;

namespace Identity.Application.Profile.Queries.GetProfile
{
    public sealed class Profile
    {
        public string Id { get; init; }
        public string Name { get; init; }
        public string Surname { get; init; }
        public string Email { get; init; }
        public string UserName { get; init; }
        public Gender Gender { get; init; }
        public DateTime DateOfBirth { get; init; }
        public string Country { get; init; }
        public string LivingCountry { get; init; }
        public string TimeZone { get; init; }
        public string AvatarUrl { get; init; }
        public GdprCookie GdprCookie { get; init; }
    }
}
