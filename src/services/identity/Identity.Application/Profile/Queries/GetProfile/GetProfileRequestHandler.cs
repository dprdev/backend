﻿using System;
using System.Threading;
using System.Threading.Tasks;

using Identity.Domain;
using Identity.Domain.Entities;

using MediatR;

using Microsoft.EntityFrameworkCore;

namespace Identity.Application.Profile.Queries.GetProfile
{
    // TODO: exception handling
    internal sealed class GetProfileRequestHandler
        : IRequestHandler<GetProfileRequest, GetProfileResponse>
    {
        private readonly DataContext _context;

        public GetProfileRequestHandler(DataContext context)
        {
            if (context is null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            _context = context;
        }

        public async Task<GetProfileResponse> Handle(
            GetProfileRequest request, CancellationToken cancellationToken)
        {
            LinguaUser profile = await GetUser(request, cancellationToken);
            if (profile is null)
            {
            }

            return new GetProfileResponse(new Profile
            {
                Id = profile.Id,
                AvatarUrl = profile.AvatarUrl,
                Country = profile.Country,
                DateOfBirth = profile.DateOfBirth,
                GdprCookie = profile.GDPRCookie,
                Gender = profile.Gender,
                LivingCountry = profile.LivingCountry,
                Name = profile.Name,
                Surname = profile.Surname,
                TimeZone = profile.TimeZone,
                UserName = profile.UserName, 
                Email = profile.Email
            });
        }

        private Task<LinguaUser> GetUser(GetProfileRequest request, CancellationToken cancellationToken)
        {
            return _context.Users.FirstOrDefaultAsync(e => e.Id == request.Id, cancellationToken: cancellationToken);
        }
    }
}
