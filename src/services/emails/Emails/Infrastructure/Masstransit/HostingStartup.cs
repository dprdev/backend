﻿
using Emails.Endpoints.Consumers.Moderation.JoinRequest;
using Emails.Infrastructure.Masstransit.Configurations;

using MassTransit;
using MassTransit.ExtensionsDependencyInjectionIntegration;

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Emails.Infrastructure.Masstransit
{
    internal delegate void Configure(
        IConfiguration cfg,
        IWebHostEnvironment env,
        IServiceCollection services,
        IServiceCollectionBusConfigurator mtCfg);

    internal sealed class HostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((host, services) =>
            {
                services.AddMassTransitHostedService();
                services.AddMassTransit((mtCfg) =>
                {
                    mtCfg.AddConsumer<JoinApprovedConsumer>();
                    mtCfg.AddConsumer<JoinRejectedConsumer>();
                    mtCfg.AddConsumer<JoinSubmissionAcceptedConsumer>();
                    mtCfg.AddConsumer<VerificationPassedConsumer>();
                    mtCfg.AddConsumer<VerificationRejectedConsumer>();

                    Configure configure =
                        host.HostingEnvironment.IsDevelopment()
                            ? MassTransitRabbitConfiguration.Configure
                            : MassTransitAzureConfiguration.Configure;

                    configure(host.Configuration, host.HostingEnvironment, services, mtCfg);
                });
            });
        }
    }
}
