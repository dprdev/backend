﻿using System.Text.Json;

using Emails.Endpoints.Consumers.Moderation.JoinRequest;

using MassTransit;
using MassTransit.ExtensionsDependencyInjectionIntegration;

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Emails.Infrastructure.Masstransit.Configurations
{
    internal static class MassTransitRabbitConfiguration
    {
        internal static void Configure(
            IConfiguration cfg,
            IWebHostEnvironment env,
            IServiceCollection services,
            IServiceCollectionBusConfigurator mtCfg)
        {
            mtCfg.UsingRabbitMq((busCfg, ctx) =>
            {
                RabbitSettings settings =
                    JsonSerializer.Deserialize<RabbitSettings>(
                        cfg.GetString("rabbitSettings"),
                        new()
                        {
                            PropertyNamingPolicy = JsonNamingPolicy.CamelCase
                        });

                ctx.Host(settings.Host, settings.Port, "/", host =>
                {
                    host.Username(settings.User);
                    host.Password(settings.Password);
                });

                ctx.ReceiveEndpoint("emails.moderation", endpoint =>
                {
                    endpoint.Consumer<JoinApprovedConsumer>(busCfg);
                    endpoint.Consumer<JoinRejectedConsumer>(busCfg);
                    endpoint.Consumer<JoinSubmissionAcceptedConsumer>(busCfg);
                    endpoint.Consumer<VerificationPassedConsumer>(busCfg);
                    endpoint.Consumer<VerificationRejectedConsumer>(busCfg);
                });
            });
        }

        private sealed class RabbitSettings
        {
            public string Host { get; set; }
            public ushort Port { get; set; }
            public string User { get; set; }
            public string Password { get; set; }
        }
    }
}
