﻿using System;
using System.Threading.Tasks;

using Contracts.Events.Moderation.JoinRequest.Notifications;

using MassTransit;

using Microsoft.Extensions.Logging;

namespace Emails.Endpoints.Consumers.Moderation.JoinRequest
{
    public sealed class JoinRejectedConsumer : IConsumer<NotifyJoinRejected>
    {
        private readonly ILogger<JoinRejectedConsumer> _logger;

        public JoinRejectedConsumer(ILogger<JoinRejectedConsumer> logger)
        {
            if (logger is null)
            {
                throw new ArgumentNullException(nameof(logger));
            }

            _logger = logger;
        }

        public async Task Consume(ConsumeContext<NotifyJoinRejected> context)
        {
            _logger.LogDebug("Send email about request rejection");
        }
    }
}
