﻿using System;
using System.Threading.Tasks;

using Contracts.Events.Moderation.JoinRequest.Notifications;

using MassTransit;

using Microsoft.Extensions.Logging;

namespace Emails.Endpoints.Consumers.Moderation.JoinRequest
{
    public sealed class JoinApprovedConsumer : IConsumer<NotifyJoinApproved>
    {
        private readonly ILogger<JoinApprovedConsumer> _logger;

        public JoinApprovedConsumer(ILogger<JoinApprovedConsumer> logger)
        {
            if (logger is null)
            {
                throw new ArgumentNullException(nameof(logger));
            }

            _logger = logger;
        }

        public async Task Consume(ConsumeContext<NotifyJoinApproved> context)
        {
            _logger.LogDebug("Sending mail about request approving");
        }
    }
}
