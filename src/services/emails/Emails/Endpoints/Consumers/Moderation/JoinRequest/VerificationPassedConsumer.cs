﻿using System;
using System.Threading.Tasks;

using Contracts.Events.Moderation.JoinRequest.Notifications;

using MassTransit;

using Microsoft.Extensions.Logging;

namespace Emails.Endpoints.Consumers.Moderation.JoinRequest
{
    public sealed class VerificationPassedConsumer : IConsumer<NotifyIdentityVerificationPassed>
    {
        private readonly ILogger<VerificationRejectedConsumer> _logger;

        public VerificationPassedConsumer(ILogger<VerificationRejectedConsumer> logger)
        {
            if (logger is null)
            {
                throw new ArgumentNullException(nameof(logger));
            }

            _logger = logger;
        }

        public async Task Consume(ConsumeContext<NotifyIdentityVerificationPassed> context)
        {
            _logger.LogDebug("Sending mail about identity verification passing");
        }
    }
}
