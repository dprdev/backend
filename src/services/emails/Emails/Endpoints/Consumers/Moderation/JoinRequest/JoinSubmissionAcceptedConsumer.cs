﻿using System;
using System.Threading.Tasks;

using Contracts.Events.Moderation.JoinRequest.Notifications;

using MassTransit;

using Microsoft.Extensions.Logging;

namespace Emails.Endpoints.Consumers.Moderation.JoinRequest
{
    public sealed class JoinSubmissionAcceptedConsumer : IConsumer<NotifyJoinAccepted>
    {
        private readonly ILogger<JoinSubmissionAcceptedConsumer> _logger;

        public JoinSubmissionAcceptedConsumer(
            ILogger<JoinSubmissionAcceptedConsumer> logger)
        {
            if (logger is null)
            {
                throw new ArgumentNullException(nameof(logger));
            }

            _logger = logger;
        }

        public async Task Consume(ConsumeContext<NotifyJoinAccepted> context)
        {
            _logger.LogDebug("Sending mail abot submission acceptance");
        }
    }
}
