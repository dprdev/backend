﻿using System;
using System.Threading.Tasks;

using Contracts.Events.Moderation.JoinRequest.Notifications;

using MassTransit;

using Microsoft.Extensions.Logging;

namespace Emails.Endpoints.Consumers.Moderation.JoinRequest
{
    public sealed class VerificationRejectedConsumer : IConsumer<NotifyIdentityVerificationRejected>
    {
        private readonly ILogger<VerificationRejectedConsumer> _logger;

        public VerificationRejectedConsumer(ILogger<VerificationRejectedConsumer> logger)
        {
            if (logger is null)
            {
                throw new ArgumentNullException(nameof(logger));
            }

            _logger = logger;
        }

        public async Task Consume(ConsumeContext<NotifyIdentityVerificationRejected> context)
        {
            _logger.LogDebug("Sending mail about identity verification rejected");
        }
    }
}
