﻿
using System;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Emails
{
    public class Startup
    {
        public Startup(
            IConfiguration cfg,
            IWebHostEnvironment env)
        {
            Cfg = cfg ?? throw new ArgumentNullException(nameof(cfg));
            Env = env ?? throw new ArgumentNullException(nameof(env));
        }

        public IConfiguration Cfg { get; }
        public IWebHostEnvironment Env { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
        }

        public void Configure(IApplicationBuilder app)
        {
            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {

            });
        }
    }
}
