﻿using System.Threading.Tasks;

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace Emails
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            IHostBuilder builder = Host
                .CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });

            await builder.Build().StartAsync();
        }
    }
}
