﻿using System.Diagnostics.CodeAnalysis;

using Microsoft.EntityFrameworkCore;

namespace Students.Domain
{
    public class DataContext : DbContext
    {
        protected DataContext()
        {
        }

        public DataContext([NotNull] DbContextOptions options) : base(options)
        {
        } 
    }
}
