﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Students.Domain.Entites
{
    public class StudentProfile
    {
        private List<StudentLanguage> _languages;

        private StudentProfile()
        {
        }

        public StudentProfile(string id, IEnumerable<StudentLanguage> languages = null)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentException($"'{nameof(id)}' cannot be null or empty.", nameof(id));
            }

            Id = id;
            _languages = languages is null
                ? new List<StudentLanguage>(0)
                : languages.ToList();
        }

        public string Id { get; private set; }

        public IReadOnlyList<StudentLanguage> Languages
        {
            get => _languages;
            private set =>
                _languages = value is List<StudentLanguage> list
                    ? list
                    : value.ToList();
        }
    }
}
