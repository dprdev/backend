﻿using System;

namespace Students.Domain.Entites
{
    public class StudentLanguage
    {
        private StudentLanguage()
        {
        }


        public StudentLanguage(string code, string studentId, LanguageLevel level, bool isStudying)
        {
            if (string.IsNullOrEmpty(code))
            {
                throw new ArgumentException($"'{nameof(code)}' cannot be null or empty.", nameof(code));
            }

            if (string.IsNullOrEmpty(studentId))
            {
                throw new ArgumentException($"'{nameof(studentId)}' cannot be null or empty.", nameof(studentId));
            }

            Code = code;
            StudentId = studentId;
            Level = level;
            IsStudying = isStudying;
        }

        public string Code { get; private set; }
        public string StudentId { get; private set; }
        public LanguageLevel Level { get; private set; }
        public bool IsStudying { get; private set; }
    }
}
