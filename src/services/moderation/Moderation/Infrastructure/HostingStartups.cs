﻿
// testing
[assembly: InternalsVisibleTo("Moderation.Tests")]

// infrastructure
[assembly: HostingStartup(typeof(Moderation.Infrastructure.EfCore.HostingStartup))]
[assembly: HostingStartup(typeof(Moderation.Infrastructure.Masstransit.HostingStartup))]
[assembly: HostingStartup(typeof(Moderation.Infrastructure.MediatR.HostingStartup))]

