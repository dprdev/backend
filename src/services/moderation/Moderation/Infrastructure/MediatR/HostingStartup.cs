﻿namespace Moderation.Infrastructure.MediatR
{
    internal sealed class HostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((host, services) =>
            {
                services.AddMediatR(Assembly.GetExecutingAssembly());
            });
        }
    }
}
