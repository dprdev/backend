﻿namespace Moderation.Infrastructure.EfCore.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Managers",
                columns: table => new
                {
                    Id = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: false),
                    Email = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    Name = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    PhotoUrl = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    Roles = table.Column<IReadOnlyList<string>>(type: "jsonb", nullable: true),
                    State = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Managers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Tickets",
                columns: table => new
                {
                    EntryId = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: false),
                    ManagerId = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Closed = table.Column<bool>(type: "boolean", nullable: false),
                    IsNew = table.Column<bool>(type: "boolean", nullable: false),
                    Tags = table.Column<string[]>(type: "jsonb", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tickets", x => new { x.EntryId, x.ManagerId });
                    table.ForeignKey(
                        name: "FK_Tickets_Managers_ManagerId",
                        column: x => x.ManagerId,
                        principalTable: "Managers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(name: "IX_Tickets_ManagerId", table: "Tickets", column: "ManagerId");

            migrationBuilder.Sql(@"
                create table moderated_entries
                (
                    id           character varying(256) primary key,
                    entry_id     character varying(256)      not null,
                    created_at   timestamp without time zone not null,
                    updated_at   timestamp without time zone not null,
                    type         character varying(32)       not null,
                    state        character varying(256)      not null,
                    requirements jsonb                       not null,
                    data         jsonb                       not null
                );

                create index on moderated_entries (type);
                create index on moderated_entries (entry_id);

                create view join_entries as
                select *
                from moderated_entries
                where type = 'joinreview';


                create view change_entries as
                select *
                from moderated_entries
                where type = 'changereview';

                create view post_entries as
                select *
                from moderated_entries
                where type = 'postreview';
            ");
            migrationBuilder.Sql("alter table \"public\".\"Tickets\" ADD FOREIGN KEY(\"EntryId\") REFERENCES moderated_entries(id);");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(name: "moderated_entries");
            migrationBuilder.DropTable(name: "join_entries");
            migrationBuilder.DropTable(name: "post_entries");
            migrationBuilder.DropTable(name: "Tickets");
            migrationBuilder.DropTable(name: "Managers");
        }
    }
}
