﻿
using Moderation.Domain.Entities;

namespace Moderation.Infrastructure.EfCore.EntityTypeConfigurations
{
    internal sealed class ModerationsConfiguration
        : IEntityTypeConfiguration<JoinModeration>
        , IEntityTypeConfiguration<ChangeModeration>
        , IEntityTypeConfiguration<PostModeration>
    {
        public void Configure(
            EntityTypeBuilder<JoinModeration> builder)
        {
            builder.Property(e => e.Id).HasColumnName("id");
            builder.Property(e => e.EntryId).HasColumnName("entry_id");
            builder.Property(e => e.CreatedDate).HasColumnName("created_at");
            builder.Property(e => e.UpdatedDate).HasColumnName("updated_at");
            builder.Property(e => e.Data).HasColumnType("jsonb").HasColumnName("data");
            builder.Property(e => e.State).HasColumnType("jsonb").HasColumnName("state");
            builder.Property(e => e.Requirements).HasColumnType("jsonb").HasColumnName("requirements");
            builder.Property(e => e.Type)
                .HasColumnName("type")
                .HasConversion(e => e.ToLowerString(),
                               e => Enum.Parse<ModerationType>(e, true));

            builder.ToTable("join_entries");
        }

        public void Configure(
            EntityTypeBuilder<ChangeModeration> builder)
        {
            builder.Property(e => e.Id).HasColumnName("id");
            builder.Property(e => e.EntryId).HasColumnName("entry_id");
            builder.Property(e => e.CreatedDate).HasColumnName("created_at");
            builder.Property(e => e.UpdatedDate).HasColumnName("updated_at");
            builder.Property(e => e.Data).HasColumnType("jsonb").HasColumnName("data");
            builder.Property(e => e.State).HasColumnType("jsonb").HasColumnName("state");
            builder.Property(e => e.Requirements).HasColumnType("jsonb").HasColumnName("requirements");
            builder.Property(e => e.Type)
                .HasColumnName("type")
                .HasConversion(e => e.ToLowerString(),
                               e => Enum.Parse<ModerationType>(e, true));
            builder.ToTable("change_entries");
        }

        public void Configure(
            EntityTypeBuilder<PostModeration> builder)
        {
            builder.Property(e => e.Id).HasColumnName("id");
            builder.Property(e => e.EntryId).HasColumnName("entry_id");
            builder.Property(e => e.CreatedDate).HasColumnName("created_at");
            builder.Property(e => e.UpdatedDate).HasColumnName("updated_at");
            builder.Property(e => e.State).HasColumnName("state");
            builder.Property(e => e.Data).HasColumnType("jsonb").HasColumnName("data");
            builder.Property(e => e.Requirements).HasColumnType("jsonb").HasColumnName("requirements");
            builder.Property(e => e.Type)
                .HasColumnName("type")
                .HasConversion(e => e.ToLowerString(),
                               e => Enum.Parse<ModerationType>(e, true));

            builder.ToTable("post_entries");
        }
    }
}
