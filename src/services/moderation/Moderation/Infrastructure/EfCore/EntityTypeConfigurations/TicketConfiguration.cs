﻿
using Moderation.Domain.Entities;

namespace Moderation.Infrastructure.EfCore.EntityTypeConfigurations
{
    internal sealed class TicketConfiguration : IEntityTypeConfiguration<Ticket>
    {
        public void Configure(EntityTypeBuilder<Ticket> builder)
        {
            builder.HasKey(e => new
            {
                e.EntryId,
                e.ManagerId
            });

            builder.Property(e => e.Tags)
                .HasColumnType("jsonb");

            builder.Property(e => e.EntryId).HasMaxLength(256);
            builder.Property(e => e.ManagerId).HasMaxLength(256);
        }
    }
}
