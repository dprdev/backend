﻿
using Moderation.Domain.Entities;

namespace Moderation.Infrastructure.EfCore.EntityTypeConfigurations
{
    internal sealed class ManagerConfiguration : IEntityTypeConfiguration<Manager>
    {
        public void Configure(EntityTypeBuilder<Manager> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).HasMaxLength(256);
            builder.Property(e => e.PhotoUrl).HasMaxLength(256);
            builder.Property(e => e.Email).HasMaxLength(256);
            builder.Property(e => e.Name).HasMaxLength(256);
            builder.Property(e => e.Roles).HasColumnType("jsonb");

            builder.HasMany(e => e.Tickets)
                .WithOne()
                .HasForeignKey(e => e.ManagerId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
