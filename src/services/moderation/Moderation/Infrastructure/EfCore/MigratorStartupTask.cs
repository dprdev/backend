﻿
using Moderation.Domain;

namespace Moderation.Infrastructure.EfCore
{
    internal sealed class MigratorStartupTask : IStartupTask
    {
        private readonly DataContext _context;

        public MigratorStartupTask(DataContext context)
        {
            if (context is null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            _context = context;
        }

        public async Task ExecuteAsync()
        {
            await _context.Database.MigrateAsync();
        }
    }
}
