﻿
using Moderation.Domain;

namespace Moderation.Infrastructure.EfCore
{
    internal sealed class DbContextFactory : IDesignTimeDbContextFactory<DataContext>
    {
        public DataContext CreateDbContext(string[] args)
        {
            IConfigurationRoot cfg = new ConfigurationBuilder()
                .AddJsonFile("appsettings.Development.json")
                .Build();

            DbContextOptionsBuilder<DataContext> optBuilder
                = new DbContextOptionsBuilder<DataContext>()
                    .UseNpgsql(cfg.GetString("DbConnection"));

            return new DataContext(optBuilder.Options);
        }
    }
}
