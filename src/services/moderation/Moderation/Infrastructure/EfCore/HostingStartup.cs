﻿
using Moderation.Domain;

namespace Moderation.Infrastructure.EfCore
{
    internal sealed class HostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((host, services) =>
            {
                services.AddDbContext<DataContext>((provider, options) =>
                {
                    options.EnableDetailedErrors();
                    options.EnableSensitiveDataLogging();
                    options.UseNpgsql(
                        host.Configuration.GetString("DbConnection"),
                        optionsBuilder =>
                        {
                        });
                });
            });
        }
    }
}
