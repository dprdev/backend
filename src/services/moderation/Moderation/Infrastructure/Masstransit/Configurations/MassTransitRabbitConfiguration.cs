﻿
using Moderation.Application.Sagas.JoinModeration;

namespace Moderation.Infrastructure.Masstransit.Configurations
{
    internal static class MassTransitRabbitConfiguration
    {
        internal static void Configure(
            IConfiguration cfg,
            IWebHostEnvironment env,
            IServiceCollection services,
            IServiceCollectionBusConfigurator mtCfg)
        {
            mtCfg.AddSagaStateMachine<JoinModerationSagaStateMachine, JoinModerationSagaState>()
                .InMemoryRepository();

            mtCfg.UsingRabbitMq((busCfg, ctx) =>
            {
                RabbitSettings settings =
                    JsonSerializer.Deserialize<RabbitSettings>(
                        cfg.GetString("rabbitSettings"),
                        new()
                        {
                            PropertyNamingPolicy = JsonNamingPolicy.CamelCase
                        });

                ctx.Host(settings.Host, settings.Port, "/", host =>
                {
                    host.Username(settings.User);
                    host.Password(settings.Password);
                });

                ctx.ReceiveEndpoint("moderations.change-requests", endpoint =>
                {
                    endpoint.ConfigureSaga<JoinModerationSagaState>(busCfg, sagaCfg =>
                    {
                        sagaCfg.UseMessageRetry(retry =>
                        {
                            retry.Incremental(5, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(5));
                        });
                    });
                });
            });
        }

        private sealed class RabbitSettings
        {
            public string Host { get; set; }
            public ushort Port { get; set; }
            public string User { get; set; }
            public string Password { get; set; }
        }
    }
}
