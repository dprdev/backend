﻿
using Moderation.Application.Sagas.JoinModeration;
using Moderation.Infrastructure.Masstransit.Configurations;

namespace Moderation.Infrastructure.Masstransit
{
    internal delegate void Configure(
        IConfiguration cfg,
        IWebHostEnvironment env,
        IServiceCollection services,
        IServiceCollectionBusConfigurator mtCfg);

    internal sealed class HostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((host, services) =>
            {
                services.AddScoped<JoinRejectedActivity>();
                services.AddScoped<JoinApprovedActivity>();
                services.AddScoped<JoinSubmittedActivity>();
                services.AddScoped<SubmissionAcceptedActivity>();
                services.AddScoped<VerificationCompletedActivity>();
                services.AddScoped<VerificationReceivedActivity>();

                services.AddMassTransitHostedService();
                services.AddMassTransit((mtCfg) =>
                {
                    Configure configure =
                        host.HostingEnvironment.IsDevelopment()
                            ? MassTransitRabbitConfiguration.Configure
                            : MassTransitAzureConfiguration.Configure;

                    configure(host.Configuration, host.HostingEnvironment, services, mtCfg);
                });
            });
        }
    }
}
