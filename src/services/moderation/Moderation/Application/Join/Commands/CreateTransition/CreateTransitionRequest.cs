﻿namespace Moderation.Application.Join.Queries.CreateTransition
{
    public sealed class CreateTransitionRequest : IRequest<CreateTransitionResponse>
    {
        public CreateTransitionRequest(string id, TransitionContext dataContext)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentException($"'{nameof(id)}' cannot be null or empty.", nameof(id));
            }

            if (dataContext is null)
            {
                throw new ArgumentNullException(nameof(dataContext));
            }

            Id = id;
            DataContext = dataContext;
        }

        public string Id { get; private set; }
        public TransitionContext DataContext { get; private set; }
    }
}
