﻿namespace Moderation.Application.Join.Queries.CreateTransition
{
    public sealed class TransitionContext
    {
        private readonly JObject _jObj;

        public TransitionContext(JObject jObj)
        {
            if (jObj is null)
            {
                throw new ArgumentNullException(nameof(jObj));
            }

            _jObj = jObj;

            State = jObj.GetValue("state", StringComparison.OrdinalIgnoreCase)
                .ToString();
        }

        public string State { get; private set; }

        public T ConvertTo<T>()
        {
            return _jObj.ToObject<T>();
        }
    }
}
