﻿
using Moderation.Application.Sagas.JoinModeration;
using Moderation.Domain;
using Moderation.Domain.Entities;

namespace Moderation.Application.Join.Queries.CreateTransition
{
    internal sealed class CreateTransitionRequestHandler
        : IRequestHandler<CreateTransitionRequest, CreateTransitionResponse>
    {
        private readonly IBusControl _bus;
        private readonly DataContext _context;

        public CreateTransitionRequestHandler(IBusControl bus, DataContext context)
        {
            if (bus is null)
            {
                throw new ArgumentNullException(nameof(bus));
            }

            if (context is null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            _bus = bus;
            _context = context;
        }

        public async Task<CreateTransitionResponse> Handle(
            CreateTransitionRequest request, CancellationToken cancellationToken)
        {
            JoinModeration model = await _context.JoinModerations.FindAsync(request.Id);
            if (model is null)
            {
                throw new InvalidOperationException("Entity does not exists");
            }

            if (request.DataContext.State != nameof(JoinModerationSagaStateMachine.OnRejeted))
            {
            }
            else if (request.DataContext.State != nameof(JoinModerationSagaStateMachine.OnApproved))
            {
                await _bus.Publish(new JoinApproved { Id = model.Id });
            }
            else if (request.DataContext.State != nameof(JoinModerationSagaStateMachine.OnSubmissionAccepted))
            {
                await _bus.Publish(new JoinSubmissionAccepted { Id = model.Id });
            }
            else
            {
                throw new InvalidOperationException($"Can not change state to {request.DataContext.State}");
            }

            return new();
        }
    }
}
