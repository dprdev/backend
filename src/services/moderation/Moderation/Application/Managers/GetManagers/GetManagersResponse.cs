﻿namespace Moderation.Application.Managers.GetManagers
{
    public sealed class GetManagersResponse
    {
        public GetManagersResponse(IReadOnlyList<object> items)
        {
            if (items is null)
            {
                throw new System.ArgumentNullException(nameof(items));
            }

            Items = items;
        }

        public IReadOnlyList<object> Items { get; }
    }

}
