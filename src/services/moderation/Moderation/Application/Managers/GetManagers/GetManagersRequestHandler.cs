﻿
using Moderation.Domain;

namespace Moderation.Application.Managers.GetManagers
{
    internal sealed class GetManagersRequestHandler
        : IRequestHandler<GetManagersRequest, GetManagersResponse>
    {
        private readonly DataContext _context;

        public GetManagersRequestHandler(DataContext context)
        {
            if (context is null)
            {
                throw new System.ArgumentNullException(nameof(context));
            }

            _context = context;
        }

        public Task<GetManagersResponse> Handle(
            GetManagersRequest request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException(nameof(Handle));
        }
    }

}
