﻿namespace Moderation.Application.Managers.GetManagers
{

    public class GetManagersRequest : IRequest<GetManagersResponse>
    {
        public GetManagersRequest(bool? isActive)
        {
            IsActive = isActive;
        }

        public bool? IsActive { get; private set; }
    }
}
