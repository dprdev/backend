﻿namespace Moderation.Application.Sagas.JoinModeration
{
    public sealed class JoinModerationSagaStateMachine
        : MassTransitStateMachine<JoinModerationSagaState>
    {
        public JoinModerationSagaStateMachine()
        {
            InstanceState(e => e.ModerationState);

            #region Events
            Event(() => OnSubmitted, e =>
            {
                e.CorrelateById(cfg => Guid.Parse(cfg.Message.Id));
            });

            Event(() => OnSubmissionAccepted, e =>
            {
                e.CorrelateById(cfg => Guid.Parse(cfg.Message.Id));
            });

            Event(() => OnVerificationCompleted, e =>
            {
                e.CorrelateById(cfg => Guid.Parse(cfg.Message.Id));
            });

            Event(() => OnVerificationResultReceived, e =>
            {
                e.CorrelateById(cfg => Guid.Parse(cfg.Message.Id));
            });

            Event(() => OnRejeted, e =>
            {
                e.CorrelateById(cfg => Guid.Parse(cfg.Message.Id));
            });

            Event(() => OnApproved, e =>
            {
                e.CorrelateById(cfg => Guid.Parse(cfg.Message.Id));
            });
            #endregion

            Initially(
                When(OnSubmitted)
                    .TransitionTo(Created)
                    .Activity((binder) => binder.OfType<JoinSubmittedActivity>()));

            During(Created,
                When(OnRejeted)
                    .TransitionTo(Rejected)
                    .Activity((binder) => binder.OfType<JoinRejectedActivity>()),
                When(OnSubmissionAccepted)
                    .TransitionTo(IdVerificationRequired)
                    .Activity((binder) => binder.OfType<SubmissionAcceptedActivity>()));

            During(IdVerificationRequired,
                When(OnRejeted)
                    .TransitionTo(Rejected)
                    .Activity((binder) => binder.OfType<JoinRejectedActivity>()),
                When(OnVerificationCompleted)
                    .TransitionTo(IdVerificationCompleted)
                    .Activity((binder) => binder.OfType<VerificationCompletedActivity>()));

            During(IdVerificationCompleted,
                When(OnRejeted)
                    .TransitionTo(Rejected)
                    .Activity((binder) => binder.OfType<JoinRejectedActivity>()),

                When(OnVerificationResultReceived)
                    .Activity((binder) => binder.OfType<VerificationReceivedActivity>())
                    .IfElse(e => e.Data.IsSuccess(),
                            a => a.TransitionTo(IdVerificationApproved),
                            a => a.TransitionTo(IdVerificationRejected)));

            During(IdVerificationRejected,
                When(OnRejeted)
                    .TransitionTo(Rejected)
                    .Activity((binder) => binder.OfType<JoinRejectedActivity>()),

                When(OnVerificationResultReceived)
                    .IfElse(e => e.Data.IsSuccess(),
                            a => a.TransitionTo(IdVerificationApproved),
                            a => a.TransitionTo(IdVerificationRejected))
                    .Activity((binder) => binder.OfType<VerificationReceivedActivity>()));

            During(IdVerificationApproved,
                When(OnRejeted)
                    .TransitionTo(Rejected)
                    .Activity((binder) => binder.OfType<JoinRejectedActivity>()),

                When(OnApproved)
                    .TransitionTo(Approved)
                    .Activity((binder) => binder.OfType<JoinApprovedActivity>()));

            DuringAny(
                When(Approved.Enter)
                    .Finalize(),

                When(Rejected.Enter)
                    .Finalize());

            SetCompletedWhenFinalized();
        }

        public Event<JoinSubmitted> OnSubmitted { get; private set; }
        public Event<JoinSubmissionAccepted> OnSubmissionAccepted { get; private set; }
        public Event<IdentityVerificationCompleted> OnVerificationCompleted { get; private set; }
        public Event<IdentityVerificationResultReceived> OnVerificationResultReceived { get; private set; }
        public Event<JoinRejected> OnRejeted { get; private set; }
        public Event<JoinApproved> OnApproved { get; private set; }

        public State Created { get; private set; }
        public State IdVerificationRequired { get; private set; }
        public State IdVerificationCompleted { get; private set; }
        public State IdVerificationApproved { get; private set; }
        public State IdVerificationRejected { get; private set; }
        public State Approved { get; private set; }
        public State Rejected { get; private set; }
    }
}
