﻿namespace Moderation.Application.Sagas.JoinModeration
{
    public class JoinModerationSagaState : SagaStateMachineInstance
    {
        /// <summary>
        /// Message correlation id. Same as a <see cref="Domain.Entities.ModerationBase.Id"/>
        /// </summary>
        public Guid CorrelationId { get; set; }

        /// <summary>
        /// Current moderation state. 
        /// </summary>
        public string ModerationState { get; set; }
    }
}
