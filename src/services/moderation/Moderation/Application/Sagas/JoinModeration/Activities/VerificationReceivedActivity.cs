﻿
using Moderation.Domain;
using Moderation.Domain.Entities;

namespace Moderation.Application.Sagas.JoinModeration
{
    internal class VerificationReceivedActivity
       : JoinActivityBase
       , Activity<JoinModerationSagaState, IdentityVerificationResultReceived>
    {
        public VerificationReceivedActivity(DataContext context)
            : base(context, nameof(VerificationReceivedActivity))
        {
        }

        public async Task Execute(
            BehaviorContext<JoinModerationSagaState, IdentityVerificationResultReceived> context,
            Behavior<JoinModerationSagaState, IdentityVerificationResultReceived> next)
        {
            Domain.Entities.JoinModeration model = await GetEntry(context.Instance.CorrelationId.ToString());
            if (model is null)
            {
                throw new InvalidOperationException(
                    $"Moderation request ({context.Data.Id}) does not exists");
            }

            model.SetState(context.Data.Id);

            Requirement requirement = model
                .GetRequirement(Requirement.IdentityVerificationName);

            if (context.Data.IsSuccess())
            {
                requirement.Close();
            }
            else
            {
                requirement.Abaddon();
            }

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw;
            }

            if (context.Data.IsSuccess())
            {
                await context.Publish(new NotifyIdentityVerificationPassed
                {
                    Id = model.EntryId
                });
            }
            else
            {
                await context.Publish(new NotifyIdentityVerificationRejected
                {
                    Id = model.EntryId
                });
            }

            await next.Execute(context);
        }

        public Task Faulted<TException>(
            BehaviorExceptionContext<JoinModerationSagaState, IdentityVerificationResultReceived, TException> context,
            Behavior<JoinModerationSagaState, IdentityVerificationResultReceived> next) where TException : Exception
        {
            return next.Faulted(context);
        }
    }
}
