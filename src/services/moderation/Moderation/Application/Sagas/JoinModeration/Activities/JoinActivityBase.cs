﻿
using Moderation.Domain;

namespace Moderation.Application.Sagas.JoinModeration
{
    internal abstract class JoinActivityBase : BaseActivity
    {
        protected readonly DataContext _context;

        public JoinActivityBase(DataContext context, string scopeName) : base(scopeName)
        {
            if (context is null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            _context = context;
        }

        public ValueTask<Domain.Entities.JoinModeration> GetEntry(string id)
        {
            return _context.JoinModerations.FindAsync(id);
        }
    }
}
