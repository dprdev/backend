﻿
using Moderation.Domain;

namespace Moderation.Application.Sagas.JoinModeration
{
    internal class VerificationCompletedActivity : JoinActivityBase, Activity<JoinModerationSagaState, IdentityVerificationCompleted>
    {
        public VerificationCompletedActivity(DataContext context)
            : base(context, nameof(VerificationCompletedActivity))
        {
        }

        public async Task Execute(
            BehaviorContext<JoinModerationSagaState, IdentityVerificationCompleted> context,
            Behavior<JoinModerationSagaState, IdentityVerificationCompleted> next)
        {
            Domain.Entities.JoinModeration model = await GetEntry(context.Instance.CorrelationId.ToString());
            if (model is null)
            {
                throw new InvalidOperationException(
                    $"Moderation request ({context.Data.Id}) does not exists");
            }

            model.SetState(context.Data.Id);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw;
            }

            await next.Execute(context);
        }

        public Task Faulted<TException>(
            BehaviorExceptionContext<JoinModerationSagaState, IdentityVerificationCompleted, TException> context,
            Behavior<JoinModerationSagaState, IdentityVerificationCompleted> next) where TException : Exception
        {
            return next.Faulted(context);
        }
    }
}
