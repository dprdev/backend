﻿
using Moderation.Domain;

namespace Moderation.Application.Sagas.JoinModeration
{
    internal sealed class JoinRejectedActivity : JoinActivityBase, Activity<JoinModerationSagaState, JoinRejected>
    {
        public JoinRejectedActivity(DataContext context)
            : base(context, nameof(JoinRejectedActivity))
        {
        }

        public async Task Execute(
            BehaviorContext<JoinModerationSagaState, JoinRejected> context,
            Behavior<JoinModerationSagaState, JoinRejected> next)
        {
            Domain.Entities.JoinModeration model = await GetEntry(context.Instance.CorrelationId.ToString());
            if (model is null)
            {
                throw new InvalidOperationException(
                    $"Moderation request ({context.Data.Id}) does not exists");
            }

            model.SetState(context.Instance.ModerationState);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw;
            }

            await context.Publish(new NotifyJoinRejected
            {
                Id = model.EntryId
            });

            await next.Execute(context);
        }

        public Task Faulted<TException>(
            BehaviorExceptionContext<JoinModerationSagaState, JoinRejected, TException> context,
            Behavior<JoinModerationSagaState, JoinRejected> next) where TException : Exception
        {
            return next.Faulted(context);
        }
    }
}
