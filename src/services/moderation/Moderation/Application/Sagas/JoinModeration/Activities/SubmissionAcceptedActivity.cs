﻿
using Moderation.Domain;
using Moderation.Domain.Entities;

namespace Moderation.Application.Sagas.JoinModeration
{
    internal sealed class SubmissionAcceptedActivity
        : JoinActivityBase
        , Activity<JoinModerationSagaState, JoinSubmissionAccepted>
    {
        public SubmissionAcceptedActivity(DataContext context)
            : base(context, nameof(SubmissionAcceptedActivity))
        {
        }

        public async Task Execute(
            BehaviorContext<JoinModerationSagaState, JoinSubmissionAccepted> context,
            Behavior<JoinModerationSagaState, JoinSubmissionAccepted> next)
        {
            Domain.Entities.JoinModeration model = await GetEntry(context.Instance.CorrelationId.ToString());
            if (model is null)
            {
                throw new InvalidOperationException(
                    $"Moderation request ({context.Data.Id}) does not exists");
            }

            model.SetState(context.Instance.ModerationState);
            model.AddRequirement(Requirement.IdentityVerification());
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw;
            }

            await context.Publish(new NotifyJoinAccepted
            {
                Id = model.EntryId
            });

            await next.Execute(context);
        }

        public Task Faulted<TException>(
            BehaviorExceptionContext<JoinModerationSagaState, JoinSubmissionAccepted, TException> context,
            Behavior<JoinModerationSagaState, JoinSubmissionAccepted> next) where TException : Exception
        {
            return next.Faulted(context);
        }
    }
}
