﻿
using Moderation.Domain;

namespace Moderation.Application.Sagas.JoinModeration
{
    internal class JoinApprovedActivity : JoinActivityBase, Activity<JoinModerationSagaState, JoinApproved>
    {
        public JoinApprovedActivity(DataContext context)
            : base(context, nameof(JoinApprovedActivity))
        {
        }

        public async Task Execute(
            BehaviorContext<JoinModerationSagaState, JoinApproved> context,
            Behavior<JoinModerationSagaState, JoinApproved> next)
        {
            Domain.Entities.JoinModeration model = await GetEntry(context.Instance.CorrelationId.ToString());
            if (model is null)
            {
                throw new InvalidOperationException(
                    $"Moderation request ({context.Data.Id}) does not exists");
            }

            model.SetState(context.Instance.ModerationState);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw;
            }

            await context.Publish(new NotifyJoinApproved
            {
                Id = model.EntryId
            });

            await next.Execute(context);
        }

        public Task Faulted<TException>(
            BehaviorExceptionContext<JoinModerationSagaState, JoinApproved, TException> context,
            Behavior<JoinModerationSagaState, JoinApproved> next) where TException : Exception
        {
            return next.Faulted(context);
        }
    }
}
