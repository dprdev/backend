﻿
using Moderation.Domain;

namespace Moderation.Application.Sagas.JoinModeration
{

    internal sealed class JoinSubmittedActivity : JoinActivityBase, Activity<JoinModerationSagaState, JoinSubmitted>
    {
        public JoinSubmittedActivity(DataContext context) : base(context, nameof(JoinSubmittedActivity))
        {
        }

        public async Task Execute(
            BehaviorContext<JoinModerationSagaState, JoinSubmitted> context,
            Behavior<JoinModerationSagaState, JoinSubmitted> next)
        {
            Domain.Entities.JoinModeration request = new Domain.Entities.JoinModeration(
                context.Data.Id, context.Data.Email, context.Data.Photo, context.Data.Username);

            request.SetState(context.Instance.ModerationState);
            try
            {
                await _context.AddWithSaveChanges(request);
            }
            catch (Exception)
            {
                throw;
            }

            await next.Execute(context);
        }

        public Task Faulted<TException>(
            BehaviorExceptionContext<JoinModerationSagaState, JoinSubmitted, TException> context,
            Behavior<JoinModerationSagaState, JoinSubmitted> next) where TException : Exception
        {
            return next.Faulted(context);
        }
    }
}
