﻿namespace Moderation
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            IHostBuilder builder = Host
                .CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });

            IHost host = builder.Build();
            await host.ExecuteStartupTask(
                assemblies: Assembly.GetExecutingAssembly()).StartAsync();
        }
    }
}
