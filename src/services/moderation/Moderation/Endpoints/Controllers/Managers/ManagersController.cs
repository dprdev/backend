﻿
using Moderation.Application.Managers.GetManagers;

namespace Moderation.Endpoints.Controllers.Managers
{
    [ApiController]
    [Route("api/v{version:apiVersion}/managers")]
    public class ManagersController : Controller
    {
        [HttpGet]
        public async Task<IActionResult> Get(
            [FromServices] IMediator mediator,
            [FromQuery(Name = "is-active")] bool? isActive = null)
        {
            GetManagersResponse response = await mediator.Send(new GetManagersRequest(isActive));
            return Json(new
            {
                response.Items
            });
        }
    }
}
