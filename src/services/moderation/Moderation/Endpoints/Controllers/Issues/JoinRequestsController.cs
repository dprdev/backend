﻿
using Moderation.Application.Join.Queries.CreateTransition;

namespace Moderation.Endpoints.Controllers.EntityModeration
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/join-requests")]
    public class JoinRequestsController : Controller
    {
        [HttpPost("{id}/transitions")]
        public async Task<IActionResult> Transitions(
            [FromServices] IMediator mediator, [FromRoute] string id)
        {
            JObject jObj = null;
            if (HttpContext.Request.Body.Length > 0)
            {
                using var reader = new HttpRequestStreamReader(
                    HttpContext.Request.Body,
                    Encoding.UTF8);

                using var jsonReader = new JsonTextReader(reader);

                jObj = await JObject.LoadAsync(jsonReader);
            }
            else
            {
                jObj = JObject.FromObject(new
                {
                });
            }

            TransitionContext ctx = new TransitionContext(jObj);
            try
            {
                CreateTransitionResponse res = await mediator.Send(new CreateTransitionRequest(id, ctx));
            }
            catch (Exception)
            {
                throw;
            }

            return Ok();
        }
    }
}
