﻿
using Moderation.Domain.Entities;

namespace Moderation.Domain
{
    public sealed class DataContext : DbContext
    {
        public DataContext()
        {
        }

        public DataContext(
            DbContextOptions<DataContext> options) : base(options)
        {
        }

        public DbSet<Ticket> Tickets { get; private set; }
        public DbSet<PostModeration> PostModerations { get; private set; }
        public DbSet<JoinModeration> JoinModerations { get; private set; }
        public DbSet<ChangeModeration> ChangeModerations { get; private set; }

        public DbSet<Manager> Managers { get; private set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        }
    }
}
