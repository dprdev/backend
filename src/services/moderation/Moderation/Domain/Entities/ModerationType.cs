﻿namespace Moderation.Domain.Entities
{
    public enum ModerationType
    {
        JoinReview,
        ChangeReview,
        PostReview
    }
}
