﻿namespace Moderation.Domain.Entities
{
    public class Ticket
    {
        public string EntryId { get; private set; }
        public string ManagerId { get; private set; }
        public DateTime CreatedDate { get; private set; }
        public DateTime UpdatedDate { get; private set; }
        public bool Closed { get; private set; }
        public bool IsNew { get; private set; }
        public string[] Tags { get; private set; }
    }
}
