﻿namespace Moderation.Domain.Entities
{
    public sealed class Manager
    {
        private readonly List<Ticket> _tickets;

        private Manager()
        {

        }

        public Manager(string email, IReadOnlyList<string> roles)
        {
            if (string.IsNullOrWhiteSpace(email))
            {
                throw new ArgumentException($"'{nameof(email)}' cannot be null or whitespace.", nameof(email));
            }

            if (roles is null)
            {
                throw new ArgumentNullException(nameof(roles));
            }

            Id = Guid
                .NewGuid()
                .ToString();

            Email = email;
            Roles = roles;
            State = ProfileState.Active;

            _tickets = new()
            {
            };
        }

        /// <summary>
        /// Same as account ID. 
        /// </summary>
        public string Id { get; private set; }
        public string Email { get; private set; }
        public string Name { get; private set; }
        public string PhotoUrl { get; private set; }

        public IReadOnlyList<Ticket> Tickets => _tickets;
        public IReadOnlyList<string> Roles { get; private set; }

        public ProfileState State { get; private set; }
    }
}
