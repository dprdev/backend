﻿namespace Moderation.Domain.Entities
{
    public enum ProfileState
    {
        Active,
        Blocked
    }
}
