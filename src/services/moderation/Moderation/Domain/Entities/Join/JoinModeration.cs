﻿namespace Moderation.Domain.Entities
{
    public class JoinModeration : ModerationBase
    {
        private JoinModeration()
        {
        }

        public JoinModeration(string userId, string email, string photo, string username)
        {
            if (string.IsNullOrEmpty(userId))
            {
                throw new ArgumentException($"'{nameof(userId)}' cannot be null or empty.", nameof(userId));
            }

            if (string.IsNullOrEmpty(email))
            {
                throw new ArgumentException($"'{nameof(email)}' cannot be null or empty.", nameof(email));
            }

            if (string.IsNullOrEmpty(photo))
            {
                throw new ArgumentException($"'{nameof(photo)}' cannot be null or empty.", nameof(photo));
            }

            if (string.IsNullOrEmpty(username))
            {
                throw new ArgumentException($"'{nameof(username)}' cannot be null or empty.", nameof(username));
            }

            Id = userId;
            EntryId = userId;
            CreatedDate = DateTime.UtcNow;
            UpdatedDate = DateTime.UtcNow;
            Type = ModerationType.JoinReview;
            Data = new JoinModerationData
            {
                Photo = photo,
                Username = username,
                Email = email
            };

            _requirements = new()
            {
            };
        }

        public JoinModerationData Data { get; private set; }
    }
}
