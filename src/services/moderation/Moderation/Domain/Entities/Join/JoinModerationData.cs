﻿namespace Moderation.Domain.Entities
{
    public class JoinModerationData
    {
        public string Photo { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
    }
}
