﻿namespace Moderation.Domain.Entities
{

    public class ModerationBase
    {
        protected List<Requirement> _requirements;

        public string Id { get; protected set; }
        public string EntryId { get; protected set; }
        public ModerationType Type { get; protected set; }
        public DateTime CreatedDate { get; protected set; }
        public DateTime UpdatedDate { get; protected set; }
        public string State { get; protected set; }

        public IReadOnlyList<Requirement> Requirements => _requirements;

        public void AddRequirement(string name)
        {
            if (_requirements is null)
            {
                throw new InvalidOperationException(
                    "Your must initialize requirements collection before access");
            }

            _requirements.Add(new Requirement(name, RequirementState.New));
        }

        public void AddRequirement(Requirement requirement)
        {
            if (_requirements is null)
            {
                throw new InvalidOperationException(
                    "Your must initialize requirements collection before access");
            }


            _requirements.Add(requirement);
        }


        public Requirement GetRequirement(string name)
        {
            if (_requirements is null)
            {
                throw new InvalidOperationException(
                    "Your must initialize requirements collection before access");
            }


            return _requirements.Find(e => e.Name == name);
        }


        public virtual void SetState(string state)
        {
            if (string.IsNullOrEmpty(state))
            {
                throw new ArgumentException($"'{nameof(state)}' cannot be null or empty.", nameof(state));
            }

            State = state;
        }
    }
}
