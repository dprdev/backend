﻿namespace Moderation.Domain.Entities
{
    public sealed record Requirement
    {
        public const string IdentityVerificationName = nameof(IdentityVerificationName);


        private Requirement()
        {

        }

        public Requirement(string name, RequirementState state)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentException($"'{nameof(name)}' cannot be null or empty.", nameof(name));
            }

            Name = name;
            State = state;
        }


        public string Name { get; private set; }
        public RequirementState State { get; private set; }


        public void Close()
        {
            State = RequirementState.Done;
        }

        public void Renew()
        {
            State = RequirementState.New;
        }

        public void Abaddon()
        {
            State = RequirementState.Fail;
        }


        public static Requirement IdentityVerification()
        {
            return new Requirement(IdentityVerificationName, RequirementState.New);
        }
    }
}
