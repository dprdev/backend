﻿namespace Moderation.Domain.Entities
{
    public enum RequirementState
    {
        New,
        Done,
        Fail
    }
}
