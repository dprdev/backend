﻿namespace Moderation
{
    public class Startup
    {
        public Startup(
            IConfiguration cfg,
            IWebHostEnvironment env)
        {
            Cfg = cfg ?? throw new ArgumentNullException(nameof(cfg));
            Env = env ?? throw new ArgumentNullException(nameof(env));
        }

        public IConfiguration Cfg { get; }
        public IWebHostEnvironment Env { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthorization();
            services.AddJwtAuthentication();
            services.AddDefaultApiVersioning();
            services.AddProblemDetails();
            services.AddControllers()
                .AddDefaultFluentValidation(cfg =>
                {
                    cfg.RegisterValidatorsFromAssembly(Assembly.GetExecutingAssembly());
                });

            services.AddGrpcClient<ChangeRequestsService.ChangeRequestsServiceClient>((client) =>
            {
                client.Address = new Uri(Cfg.GetString("teachersUrl"));
            });
        }

        public void Configure(IApplicationBuilder app, IBusControl busControl)
        {
            app.UseProblemDetails();

            Task.Run(async () =>
            {
                await Task.Delay(TimeSpan.FromSeconds(10));
                await busControl.Publish(new JoinSubmitted
                {
                    Id = Guid.NewGuid().ToString()
                });
            });

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }

}
