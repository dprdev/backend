﻿
using MassTransit;
using MassTransit.ExtensionsDependencyInjectionIntegration;

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using Orders.Infrastructure.Masstransit.Configurations;

namespace Orders.Infrastructure.Masstransit
{
    internal delegate void Configure(
        IConfiguration cfg,
        IWebHostEnvironment env,
        IServiceCollection services,
        IServiceCollectionBusConfigurator mtCfg);

    public sealed class HostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((host, services) =>
            { 
                services.AddMassTransitHostedService();
                services.AddMassTransit((mtCfg) =>
                {
                    Configure configure =
                        host.HostingEnvironment.IsDevelopment()
                            ? MassTransitRabbitConfiguration.Configure
                            : MassTransitAzureConfiguration.Configure;

                    configure(host.Configuration, host.HostingEnvironment, services, mtCfg);
                });
            });
        }
    }
}
