﻿using System.Threading.Tasks;

namespace Orders.Infrastructure.EfCore.Seeders
{
    internal interface ISeeder
    {
        Task Seed();
    }
}
