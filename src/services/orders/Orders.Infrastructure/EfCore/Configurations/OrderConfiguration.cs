﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

using Orders.Domain.Entities;

namespace Orders.Infrastructure.EfCore.Configurations
{
    internal sealed class OrderConfiguration : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.HasKey(e => e.Id);
            builder.HasIndex(e => e.UserId);
            builder.HasIndex(e => e.CurrentState);

            builder.Property(e => e.UserId).IsRequired();
            builder.Property(e => e.CurrentState).IsRequired(); 

            builder.Property(e => e.Product)
                .HasColumnType("jsonb")
                .IsRequired()
                .HasConversion(new ProductConverter());
        }

        internal sealed class ProductConverter : ValueConverter<Product, string>
        {
            public ProductConverter()
                : base(value => Convert(value),
                       value => Convert(value))
            {

            }

            private static string Convert(Product product)
            {
                return System.Text.Json.JsonSerializer.Serialize(product);
            }

            private static Product Convert(string value)
            {
                throw new System.Exception();
            }
        }
    }
}
