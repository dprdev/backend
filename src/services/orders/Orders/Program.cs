﻿using System.Reflection;

using Hellang.Middleware.ProblemDetails;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

// configure sentry first
[assembly: HostingStartup(typeof(Orders.Infrastructure.Sentry.HostingStartup))]
[assembly: HostingStartup(typeof(Orders.Infrastructure.EfCore.HostingStartup))]
[assembly: HostingStartup(typeof(Orders.Infrastructure.MediatR.HostingStartup))]
[assembly: HostingStartup(typeof(Orders.Infrastructure.Masstransit.HostingStartup))]


IHostBuilder builder = Host 
    .CreateDefaultBuilder(args)
    .ConfigureWebHostDefaults(webBuilder =>
    {
        webBuilder.ConfigureServices((services) =>
        {
            services.AddProblemDetails();
            services.AddDefaultApiVersioning();
            services.AddControllers()
                .AddDefaultFluentValidation((cfg) =>
                {
                    cfg.RegisterValidatorsFromAssembly(Assembly.Load("Orders"));
                    cfg.RegisterValidatorsFromAssembly(Assembly.Load("Orders.Application"));
                    cfg.RegisterValidatorsFromAssembly(Assembly.Load("Orders.Domain"));
                });

            services.AddAuthorization();
            services.AddAuthentication();
        });

        webBuilder.Configure((app) =>
        {
            app.UseProblemDetails();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints((endpoints) =>
            {
                endpoints.MapControllers();
            });
        });
    });

await builder.StartAsync();
