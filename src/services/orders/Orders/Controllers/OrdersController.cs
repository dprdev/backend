﻿using System;

using MediatR;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Orders.Controllers
{
    [ApiVerV1]
    [ApiController]
    [Route("api/v{version:apiVersion}/orders")]
    public sealed class OrdersController : ApiController
    {
        private readonly IMediator _mediator;
        private readonly ILogger<OrdersController> _logger;

        public OrdersController(IMediator mediator, ILogger<OrdersController> logger)
        {
            if (mediator is null)
            {
                throw new ArgumentNullException(nameof(mediator));
            }

            if (logger is null)
            {
                throw new ArgumentNullException(nameof(logger));
            }

            _mediator = mediator;
            _logger = logger;
        } 
    }
}
