﻿using System.Diagnostics.CodeAnalysis;

using Microsoft.EntityFrameworkCore;

using Orders.Domain.Entities;

namespace Orders.Domain
{
    public class DataContext : DbContext
    {
        protected DataContext()
        {
        }

        public DataContext([NotNull] DbContextOptions options) : base(options)
        {
        }

        public DbSet<Order> Orders { get; private set; }
    }
}
