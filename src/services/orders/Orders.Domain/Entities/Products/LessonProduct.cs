﻿using System;
using System.Collections.Generic;

namespace Orders.Domain.Entities
{
    public class LessonProduct : Product
    {
        private LessonProduct()
        {
        } 

        public LessonProduct(
            string lessonId, 
            string lessonPriceId, 
            string teacherId, 
            string language, 
            string lessonName,
            string lessonDescription, 
            int lessonPrice,
            int lessonDuration, 
            int lessonPackageSize,
            int lessonPackagePrice,
            IReadOnlyCollection<DateTime> dates)
        {
            LessonId = lessonId;
            LessonPriceId = lessonPriceId;
            TeacherId = teacherId;
            Language = language;
            LessonName = lessonName;
            LessonDescription = lessonDescription;
            LessonPrice = lessonPrice;
            LessonDuration = lessonDuration;
            LessonPackageSize = lessonPackageSize;
            LessonPackagePrice = lessonPackagePrice;
            Dates = dates;
        }

        public string LessonId { get; private set; }
        public string LessonPriceId { get; private set; }
        public string TeacherId { get; private set; }
        public string Language { get; private set; }
        public string LessonName { get; private set; }
        public string LessonDescription { get; private set; }
        public int LessonPrice { get; private set; }
        public int LessonDuration { get; private set; }
        public int LessonPackageSize { get; private set; }
        public int LessonPackagePrice { get; private set; }  

        public IReadOnlyCollection<DateTime> Dates { get; private set; }
    }
}
