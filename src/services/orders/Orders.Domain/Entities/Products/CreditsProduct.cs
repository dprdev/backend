﻿using System;

namespace Orders.Domain.Entities
{
    public class CreditsProduct : Product
    {

        private CreditsProduct()
        {
        }

        public CreditsProduct(int amount) : base(ProductType.Credits)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("Amount must be greater than zero", nameof(amount)); 
            }

            Amount = amount;
        }

        public int Amount { get; private set; }
    }
}
