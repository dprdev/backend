﻿using System;

namespace Orders.Domain.Entities
{
    public class Order
    {
        private Order()
        {
        }

        public Order(
            string userId,
            Product product)
        {
            if (string.IsNullOrEmpty(userId))
            {
                throw new ArgumentException($"'{nameof(userId)}' cannot be null or empty.", nameof(userId));
            }

            if (product is null)
            {
                throw new ArgumentNullException(nameof(product));
            }

            Id = Guid
                .NewGuid()
                .ToString();

            UserId = userId;
            Product = product;
            CreatedDate = DateTime.UtcNow;
            ExpiredDate = DateTime.UtcNow.AddMinutes(20);

            CurrentState = OrderState.Created;
        }

        public string Id { get; private set; }
        public string UserId { get; private set; }
        public DateTime CreatedDate { get; private set; }
        public DateTime ExpiredDate { get; private set; }
        public Product Product { get; private set; }
        public OrderState CurrentState { get; private set; }

        public bool IsExpired =>
            ExpiredDate <= DateTime.UtcNow
            && CurrentState == OrderState.Created;
    }
}
