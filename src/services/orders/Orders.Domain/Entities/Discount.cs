﻿using System;

namespace Orders.Domain.Entities
{
    public class Discount
    {
        private Discount()
        {
        }

        public Discount(string id, int amount, bool isPersent)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentException($"'{nameof(id)}' cannot be null or empty.", nameof(id));
            }

            if (amount <= 0)
            {
                throw new ArgumentException("Amount must be >= then 0", nameof(amount));
            }

            Id = id;
            Amount = amount;
            IsPersent = isPersent;
        }

        public string Id { get; private set; }

        public int Amount { get; private set; }
        public bool IsPersent { get; private set; }
    }
}
