﻿using System;
using System.Collections.Generic;

namespace Orders.Domain.Entities
{
    public class Product
    {
        protected List<Discount> _discounts;

        protected Product()
        {
        }

        public Product(ProductType productType)
        {
            ProductType = productType;

            _discounts = new()
            {
            };
        }

        public ProductType ProductType { get; protected set; }

        public IReadOnlyList<Discount> Discounts => _discounts;

        public void AddDiscount(Discount discount)
        {
            if (discount is null)
            {
                throw new ArgumentNullException(nameof(discount));
            }

            _discounts.Add(discount);
        }
    }
}
