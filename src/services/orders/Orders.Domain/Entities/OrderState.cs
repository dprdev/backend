﻿namespace Orders.Domain.Entities
{
    public enum OrderState
    {
        Created,  
        Processing, 
        Completed
    }
}
