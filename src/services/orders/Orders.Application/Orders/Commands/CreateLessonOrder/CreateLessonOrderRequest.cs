﻿
using System;
using System.Collections.Generic;

using MediatR;

namespace Orders.Application.Orders.Commands.CreateLessonOrder
{
    public sealed class CreateLessonOrderRequest : IRequest<CreateLessonOrderResponse>
    {
        public CreateLessonOrderRequest(
            string userId,
            string language,
            string lessonId,
            string lessonPriceId, 
            string teacherId,
            string lessonName,
            string lessonDescription,
            int lessonPrice,
            int lessonDuration,
            int lessonPackageSize,
            int lessonPackagePrice, 
            IReadOnlyList<DateTime> dates)
        {
            UserId = userId;
            Language = language;
            LessonId = lessonId;
            LessonPriceId = lessonPriceId; 
            TeacherId = teacherId;
            LessonName = lessonName;
            LessonDescription = lessonDescription;
            LessonPrice = lessonPrice;
            LessonDuration = lessonDuration;
            LessonPackageSize = lessonPackageSize;
            LessonPackagePrice = lessonPackagePrice;
            Dates = dates;
        }

        public string UserId { get; private set; }
        public string Language { get; private set; }
        public string LessonId { get; private set; }
        public string LessonPriceId { get; private set; } 
        public string TeacherId { get; private set; }
        public string LessonName { get; private set; }
        public string LessonDescription { get; private set; }
        public int LessonPrice { get; private set; }
        public int LessonDuration { get; private set; }
        public int LessonPackageSize { get; private set; }
        public int LessonPackagePrice { get; private set; }
        public IReadOnlyList<DateTime> Dates { get; private set; }
    }
}
