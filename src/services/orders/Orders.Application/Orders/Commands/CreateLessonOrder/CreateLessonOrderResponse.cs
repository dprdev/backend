﻿using System;

using OneOf;

namespace Orders.Application.Orders.Commands.CreateLessonOrder
{
    public sealed class CreateLessonOrderResponse : OneOfBase<string, Exception>
    {
        public CreateLessonOrderResponse(OneOf<string, Exception> input) : base(input)
        {
        }
    }
}
