﻿using System;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.Extensions.Logging;

using Orders.Domain;
using Orders.Domain.Entities;

namespace Orders.Application.Orders.Commands.CreateLessonOrder
{
    internal sealed class CreateLessonOrderRequestHandler
        : IRequestHandler<CreateLessonOrderRequest, CreateLessonOrderResponse>
    {
        private readonly DataContext _context;
        private readonly ILogger<CreateLessonOrderRequestHandler> _logger;

        public CreateLessonOrderRequestHandler(
            DataContext context, ILogger<CreateLessonOrderRequestHandler> logger)
        {
            if (context is null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (logger is null)
            {
                throw new ArgumentNullException(nameof(logger));
            }

            _context = context;
            _logger = logger;
        }

        public async Task<CreateLessonOrderResponse> Handle(
            CreateLessonOrderRequest request, CancellationToken cancellationToken)
        {
            var order = new Order(
                userId: request.UserId,
                product: new LessonProduct(
                    lessonId: request.LessonId,
                    lessonPriceId: request.LessonPriceId,
                    teacherId: request.TeacherId,
                    language: request.Language,
                    lessonName: request.LessonName,
                    lessonDescription: request.LessonDescription,
                    lessonPrice: request.LessonPrice,
                    lessonDuration: request.LessonDuration,
                    lessonPackageSize: request.LessonPackageSize,
                    lessonPackagePrice: request.LessonPackagePrice,
                    dates: request.Dates));

            _context.Add(order);
            try
            {
                await _context.SaveChangesAsync(cancellationToken);
            }
            catch (Exception ex)
            {
                return new(ex);
            }

            return new(order.Id);
        }
    }
}
