﻿
using System;

using MediatR;

namespace Orders.Application.Orders.Commands.CreateMoneyOrder
{
    public sealed class CreateMoneyOrderRequest : IRequest<CreateMoneyOrderResponse>
    {
        public CreateMoneyOrderRequest(string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                throw new ArgumentException($"'{nameof(userId)}' cannot be null or empty.", nameof(userId));
            }

            UserId = userId;
        }

        public string UserId { get; private set; }
    }
}
