﻿using System;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.Extensions.Logging;

using Orders.Domain;
using Orders.Domain.Entities;

namespace Orders.Application.Orders.Commands.CreateMoneyOrder
{
    internal sealed class CreateMoneyOrderRequestHandler
        : IRequestHandler<CreateMoneyOrderRequest, CreateMoneyOrderResponse>
    {
        private readonly DataContext _context;
        private readonly ILogger<CreateMoneyOrderRequestHandler> _logger;

        public CreateMoneyOrderRequestHandler(
            DataContext context, ILogger<CreateMoneyOrderRequestHandler> logger)
        {
            if (context is null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (logger is null)
            {
                throw new ArgumentNullException(nameof(logger));
            }

            _context = context;
            _logger = logger;
        }

        public async Task<CreateMoneyOrderResponse> Handle(
            CreateMoneyOrderRequest request, CancellationToken cancellationToken)
        {
            var order = new Order(
                userId: request.UserId,
                product: new CreditsProduct(100));

            _context.Add(order);
            try
            {
                await _context.SaveChangesAsync(cancellationToken);
            }
            catch (Exception ex)
            {
                return new(ex);
            }

            return new(order.Id);
        }
    }
}
