﻿using System;

using OneOf;

namespace Orders.Application.Orders.Commands.CreateMoneyOrder
{
    public sealed class CreateMoneyOrderResponse : OneOfBase<string, Exception>
    {
        public CreateMoneyOrderResponse(OneOf<string, Exception> input) : base(input)
        {
        }
    }
}
