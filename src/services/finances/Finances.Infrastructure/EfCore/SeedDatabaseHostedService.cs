﻿using System;
using System.Threading;
using System.Threading.Tasks;

using Finances.Infrastructure.EfCore.Seeders;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Finances.Infrastructure.EfCore
{
    internal sealed class SeedDatabaseHostedService : IHostedService
    {
        private readonly IServiceProvider _provider;

        public SeedDatabaseHostedService(IServiceProvider provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            _provider = provider;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            using IServiceScope scope = _provider.CreateScope();
            var seeders = new ISeeder[]
            {
            };

            foreach (ISeeder seeder in seeders)
            {
                await seeder.Seed();
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}
