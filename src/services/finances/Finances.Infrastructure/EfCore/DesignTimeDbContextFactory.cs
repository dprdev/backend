﻿
using Finances.Domain;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace Finances.Infrastructure.EfCore
{
    internal sealed class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<DataContext>
    {
        DataContext IDesignTimeDbContextFactory<DataContext>.CreateDbContext(string[] args)
        {
            DbContextOptionsBuilder<DataContext> optBuilder
                = new DbContextOptionsBuilder<DataContext>()
                    .ReplaceService<IModelCustomizer, CustomModelCustomizer>()
                    .UseNpgsql((cfg) =>
                    {
                        cfg.MigrationsAssembly("Finances.Infrastructure");
                    });

            return new DataContext(optBuilder.Options);
        }
    }
}
