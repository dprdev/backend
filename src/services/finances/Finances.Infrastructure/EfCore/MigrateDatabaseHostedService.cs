﻿using System;
using System.Threading;
using System.Threading.Tasks;

using Finances.Domain;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using Polly;
using Polly.Retry;

namespace Finances.Infrastructure.EfCore
{
    internal sealed class MigrateDatabaseHostedService : IHostedService
    {
        private readonly IServiceProvider _provider;

        public MigrateDatabaseHostedService(IServiceProvider provider)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            _provider = provider;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            using IServiceScope scope = _provider.CreateScope();

            DataContext db = scope.ServiceProvider.GetService<DataContext>();
            AsyncRetryPolicy policy = Policy
                .Handle<Exception>()
                .WaitAndRetryAsync(5, (count) =>
                {
                    return TimeSpan.FromSeconds(count + count * 0.5);
                });

            await policy.ExecuteAsync(() => db.Database.MigrateAsync());
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}
