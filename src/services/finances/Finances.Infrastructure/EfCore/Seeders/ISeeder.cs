﻿using System.Threading.Tasks;

namespace Finances.Infrastructure.EfCore.Seeders
{
    internal interface ISeeder
    {
        Task Seed();
    }
}
