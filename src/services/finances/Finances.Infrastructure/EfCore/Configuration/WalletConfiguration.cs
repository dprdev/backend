﻿
using System;

using Finances.Domain.Entities;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Finances.Infrastructure.EfCore.Configuration
{
    internal sealed class WalletConfiguration
        : IEntityTypeConfiguration<Wallet>
        , IEntityTypeConfiguration<StudentWallet>
        , IEntityTypeConfiguration<TeacherWallet>
        , IEntityTypeConfiguration<Operation>
    {
        public void Configure(EntityTypeBuilder<Wallet> builder)
        {
            builder.HasKey(e => e.Id);
            builder.HasIndex(e => e.UserId);

            builder.Property(e => e.UserId).HasMaxLength(256);

            builder.HasDiscriminator(e => e.WalletType)
                .HasValue<StudentWallet>(WalletType.StudentWallet)
                .HasValue<TeacherWallet>(WalletType.TeacherWallet);

            builder.Property(e => e.WalletType)
                .HasMaxLength(64)
                .HasConversion(e => e.ToString(),
                               e => Enum.Parse<WalletType>(e));

            builder.HasMany(e => e.Operations)
                .WithOne()
                .HasForeignKey(e => e.WalletId);
        }

        public void Configure(EntityTypeBuilder<StudentWallet> builder)
        {
        }

        public void Configure(EntityTypeBuilder<TeacherWallet> builder)
        {
        }

        public void Configure(EntityTypeBuilder<Operation> builder)
        {
            builder.HasKey(e => e.Id);
            builder.HasMany(e => e.Operations)
                .WithOne()
                .HasForeignKey(e => e.ParentId); 
        }
    }
}
