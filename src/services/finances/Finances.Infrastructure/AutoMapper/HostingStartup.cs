﻿using System.Reflection;

using AutoMapper;

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace Finances.Infrastructure.AutoMapper
{
    public sealed class HostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((host, services) =>
            {
                services.AddAutoMapper(Configure, GetAssemblies());
            });
        }

        private static Assembly[] GetAssemblies()
        {
            return new Assembly[]
            {
                Assembly.Load("Finances.Domain"),
                Assembly.Load("Finances.Application"),
            };
        }

        private static void Configure(IMapperConfigurationExpression cfg)
        {

        }
    }
}
