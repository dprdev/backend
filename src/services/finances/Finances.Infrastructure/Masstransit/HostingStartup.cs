﻿
using Finances.Application.Consumers.Registrations;
using Finances.Infrastructure.Masstransit.Configurations;

using MassTransit;
using MassTransit.ExtensionsDependencyInjectionIntegration;

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Finances.Infrastructure.Masstransit
{
    internal delegate void Configure(
        IConfiguration cfg,
        IWebHostEnvironment env,
        IServiceCollection services,
        IServiceCollectionBusConfigurator mtCfg);

    public sealed class HostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((host, services) =>
            {
                services.AddMassTransitHostedService();
                services.AddMassTransit((mtCfg) =>
                {
                    mtCfg.AddConsumer<StudentRegisteredConsumer>();
                    mtCfg.AddConsumer<TeacherRegisteredConsumer>();

                    Configure configure =
                        host.HostingEnvironment.IsDevelopment()
                            ? MassTransitRabbitConfiguration.Configure
                            : MassTransitAzureConfiguration.Configure;

                    configure(host.Configuration, host.HostingEnvironment, services, mtCfg);
                });
            });
        }
    }
}
