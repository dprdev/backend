﻿using System;
using System.Text.Json;

using Finances.Application.Consumers.Registrations;

using GreenPipes;

using MassTransit;
using MassTransit.ExtensionsDependencyInjectionIntegration;

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Finances.Infrastructure.Masstransit.Configurations
{
    internal static class MassTransitRabbitConfiguration
    {
        internal static void Configure(
            IConfiguration cfg,
            IWebHostEnvironment env,
            IServiceCollection services,
            IServiceCollectionBusConfigurator mtCfg)
        {
            mtCfg.UsingRabbitMq((busCfg, ctx) =>
            {
                RabbitSettings settings =
                    JsonSerializer.Deserialize<RabbitSettings>(
                        cfg.GetValue<string>("rabbitSettings"),
                        new()
                        {
                            PropertyNamingPolicy = JsonNamingPolicy.CamelCase
                        });

                ctx.ReceiveEndpoint("identity.new-registration", endpoint =>
                {
                    endpoint.Consumer<StudentRegisteredConsumer>(busCfg, consumer =>
                    {
                        consumer.UseMessageRetry(e => e.Incremental(5, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1.75)));
                    });

                    endpoint.Consumer<TeacherRegisteredConsumer>(busCfg, consumer =>
                    {
                        consumer.UseMessageRetry(e => e.Incremental(5, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1.75)));
                    });
                });

                ctx.Host(settings.Host, settings.Port, "/", host =>
                {
                    host.Username(settings.User);
                    host.Password(settings.Password);
                });
            });
        }

        private sealed class RabbitSettings
        {
            public string Host { get; set; }
            public ushort Port { get; set; }
            public string User { get; set; }
            public string Password { get; set; }
        }
    }
}
