﻿using System.Diagnostics.CodeAnalysis;

using Finances.Domain.Entities;

using Microsoft.EntityFrameworkCore;

namespace Finances.Domain
{
    public sealed class DataContext : DbContext
    {
        public DataContext()
        {
        }

        public DataContext([NotNull] DbContextOptions<DataContext> options) : base(options)
        {
        }

        public DbSet<Wallet> Wallets { get; private set; }
        public DbSet<Operation> Operations { get; private set; }
    }
}
