﻿namespace Finances.Domain.Entities
{
    public enum WalletType
    {
        StudentWallet, 
        TeacherWallet
    }
}
