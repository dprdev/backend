﻿using System.Collections.Generic;
using System.Linq;

namespace Finances.Domain.Entities
{
    public class TeacherWallet : Wallet
    {
        protected TeacherWallet()
        {
        }

        public TeacherWallet(string id)
            : base(id, WalletType.TeacherWallet)
        {
        }

        public int GetWithdrawPendingBalance()
        {
            IEnumerable<int> ops = 
                from x in _operations
                where x.State == OperationState.Pending
                      && x.Type == OperationType.Withdraw
                select x.Value;

            return ops.Sum();
        }

        public static string GetKey(string userId)
        {
            return GetKey(userId, WalletType.TeacherWallet);
        }
    }
}
