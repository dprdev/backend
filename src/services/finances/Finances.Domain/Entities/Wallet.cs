﻿using System;
using System.Collections.Generic;

namespace Finances.Domain.Entities
{
    public abstract class Wallet
    {
        protected string _id;
        protected readonly List<Operation> _operations;

        protected Wallet()
        {
        }

        public Wallet(string userId, WalletType walletType)
        {
            if (string.IsNullOrEmpty(userId))
            {
                throw new ArgumentException($"'{nameof(userId)}' cannot be null or empty.", nameof(userId));
            }

            UserId = userId;
            WalletType = walletType;
        }

        public string Id =>
            _id ??= GetKey(UserId, WalletType);

        public string UserId { get; private set; }
        public DateTime CreatedDate { get; private set; }

        public int Balance { get; private set; }
        public WalletType WalletType { get; private set; }

        public IReadOnlyList<Operation> Operations => _operations;


        public static string GetKey(string userId, WalletType walletType)
        {
            return userId + "|" + walletType;
        }
    } 
}
