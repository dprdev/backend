﻿namespace Finances.Domain.Entities
{
    public enum OperationType
    {
        Deposit,
        Withdraw,
        LessonPayment,
        PackagePayment,
        Transfer
    }
}
