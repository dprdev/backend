﻿namespace Finances.Domain.Entities
{
    public enum OperationState
    {
        Pending,
        Completed,
        Failed
    }
}
