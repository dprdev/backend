﻿using System.Collections.Generic;
using System.Linq;

namespace Finances.Domain.Entities
{
    public class StudentWallet : Wallet
    {
        protected StudentWallet()
        {
        }

        public StudentWallet(string id)
            : base(id, WalletType.StudentWallet)
        {
        }

        public int GetReservedBalance()
        {
            IEnumerable<int> operations =
                from x in _operations
                where x.State == OperationState.Pending
                      && (x.Type == OperationType.PackagePayment
                          || string.IsNullOrEmpty(x.PackageId) && x.Type == OperationType.LessonPayment)
                select x.GetReservedBalance();

            return operations.Sum();

        }

        public static string GetKey(string userId)
        {
            return GetKey(userId, WalletType.StudentWallet);
        }
    }
}
