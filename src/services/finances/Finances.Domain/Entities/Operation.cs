﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Finances.Domain.Entities
{
    public class Operation
    {
        private readonly List<Operation> _operations;

        public string Id { get; private set; }
        public string OrderId { get; private set; }
        public string PackageId { get; private set; }
        public string WalletId { get; private set; }
        public string ParentId { get; private set; }

        public DateTime CreatedDate { get; private set; }
        public OperationType Type { get; private set; }
        public OperationState State { get; private set; }
        public int Value { get; private set; }

        public IReadOnlyList<Operation> Operations => _operations;

        public int GetReservedBalance()
        {
            if (State != OperationState.Pending)
            {
                return 0;
            }

            if (Type == OperationType.PackagePayment)
            {
                IEnumerable<int> ops = from x in _operations
                                       where x.State == OperationState.Completed
                                             && x.Type == OperationType.LessonPayment
                                       select x.Value;

                return Value - ops.Sum();
            }

            return 0;
        }
    }
}
