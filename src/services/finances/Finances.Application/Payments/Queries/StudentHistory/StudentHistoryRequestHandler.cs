﻿using System;
using System.Threading;
using System.Threading.Tasks;

using Finances.Domain;

using MediatR;

using Microsoft.Extensions.Logging;

namespace Finances.Application.Payments.Queries.StudentHistory
{
    internal sealed class StudentHistoryRequestHandler
        : IRequestHandler<StudentHistoryRequest, StudentHistoryResponse>
    {
        private readonly DataContext _context;
        private readonly ILogger<StudentHistoryRequestHandler> _logger;

        public StudentHistoryRequestHandler(
            DataContext context,
            ILogger<StudentHistoryRequestHandler> logger)
        {
            if (context is null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (logger is null)
            {
                throw new ArgumentNullException(nameof(logger));
            }

            _context = context;
            _logger = logger;
        }

        public Task<StudentHistoryResponse> Handle(
            StudentHistoryRequest request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
