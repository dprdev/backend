﻿using System;

using MediatR;

namespace Finances.Application.Payments.Queries.StudentHistory
{
    public sealed class StudentHistoryRequest : IRequest<StudentHistoryResponse>
    {
        public StudentHistoryRequest(string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                throw new ArgumentException($"'{nameof(userId)}' cannot be null or empty.", nameof(userId));
            }

            UserId = userId;
        }

        public string UserId { get; private set; }
    }
}
