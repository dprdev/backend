﻿using System;

using OneOf;

namespace Finances.Application.Payments.Queries.StudentHistory
{
    public sealed class StudentHistoryResponse : OneOfBase<object, Exception>
    {
        public StudentHistoryResponse(OneOf<object, Exception> input) : base(input)
        {
        }
    }
}
