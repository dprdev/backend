﻿using System;
using System.Threading;
using System.Threading.Tasks;

using Finances.Domain;

using MediatR;

using Microsoft.Extensions.Logging;

namespace Finances.Application.Payments.Queries.WithdrawHistory
{
    internal sealed class WithdrawHistoryRequestHandler
        : IRequestHandler<WithdrawHistoryRequest, WithdrawHistoryResponse>
    {
        private readonly DataContext _context;
        private readonly ILogger<WithdrawHistoryRequestHandler> _logger;

        public WithdrawHistoryRequestHandler(
            DataContext context,
            ILogger<WithdrawHistoryRequestHandler> logger)
        {
            if (context is null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (logger is null)
            {
                throw new ArgumentNullException(nameof(logger));
            }

            _context = context;
            _logger = logger;
        }

        public Task<WithdrawHistoryResponse> Handle(
            WithdrawHistoryRequest request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
