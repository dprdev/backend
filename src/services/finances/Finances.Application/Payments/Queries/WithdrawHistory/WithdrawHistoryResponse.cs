﻿using System;

using OneOf;

namespace Finances.Application.Payments.Queries.WithdrawHistory
{
    public sealed class WithdrawHistoryResponse : OneOfBase<object, Exception>
    {
        public WithdrawHistoryResponse(OneOf<object, Exception> input) : base(input)
        {
        }
    }
}
