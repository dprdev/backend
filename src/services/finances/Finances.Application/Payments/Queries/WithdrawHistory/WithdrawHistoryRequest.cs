﻿using System;

using MediatR;

namespace Finances.Application.Payments.Queries.WithdrawHistory
{
    public sealed class WithdrawHistoryRequest : IRequest<WithdrawHistoryResponse>
    {
        public WithdrawHistoryRequest(string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                throw new ArgumentException($"'{nameof(userId)}' cannot be null or empty.", nameof(userId));
            }

            UserId = userId;
        }

        public string UserId { get; private set; }
    }
}
