﻿using System;

using MediatR;

namespace Finances.Application.Payments.Queries.TeacherHistory
{
    public sealed class TeacherHistoryRequest : IRequest<TeacherHistoryResponse>
    {
        public TeacherHistoryRequest(string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                throw new ArgumentException($"'{nameof(userId)}' cannot be null or empty.", nameof(userId));
            }

            UserId = userId;
        }

        public string UserId { get; private set; }
    }
}
