﻿using System;
using System.Threading;
using System.Threading.Tasks;

using Finances.Domain;

using MediatR;

using Microsoft.Extensions.Logging;

namespace Finances.Application.Payments.Queries.TeacherHistory
{
    internal sealed class TeacherHistoryRequestHandler
        : IRequestHandler<TeacherHistoryRequest, TeacherHistoryResponse>
    {
        private readonly DataContext _context;
        private readonly ILogger<TeacherHistoryRequestHandler> _logger;

        public TeacherHistoryRequestHandler(
            DataContext context,
            ILogger<TeacherHistoryRequestHandler> logger)
        {
            if (context is null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (logger is null)
            {
                throw new ArgumentNullException(nameof(logger));
            }

            _context = context;
            _logger = logger;
        }

        public Task<TeacherHistoryResponse> Handle(
            TeacherHistoryRequest request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
