﻿using System;

using OneOf;

namespace Finances.Application.Payments.Queries.TeacherHistory
{
    public sealed class TeacherHistoryResponse : OneOfBase<object, Exception>
    {
        public TeacherHistoryResponse(OneOf<object, Exception> input) : base(input)
        {
        }
    }
}
