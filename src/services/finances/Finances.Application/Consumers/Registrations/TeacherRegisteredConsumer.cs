﻿using System;
using System.Threading.Tasks;

using Contracts.Events.Teachers;

using MassTransit;

namespace Finances.Application.Consumers.Registrations
{
    public class TeacherRegisteredConsumer : IConsumer<TeacherRegistered>
    {
        public Task Consume(ConsumeContext<TeacherRegistered> context)
        {
            throw new NotImplementedException();
        }
    }
}
