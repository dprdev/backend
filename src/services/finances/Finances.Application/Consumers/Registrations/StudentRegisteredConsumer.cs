﻿using System;
using System.Threading.Tasks;

using Contracts.Events.Students;

using MassTransit;

namespace Finances.Application.Consumers.Registrations
{
    public class StudentRegisteredConsumer : IConsumer<StudentRegistered>
    {
        public Task Consume(ConsumeContext<StudentRegistered> context)
        {
            throw new NotImplementedException();
        }
    }
}
