﻿
using System;

using MediatR;

namespace Finances.Application.Balance.Queries.TeacherBalance
{
    public sealed class TeacherBalanceRequest : IRequest<TeacherBalanceResponse>
    {
        public TeacherBalanceRequest(string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                throw new ArgumentException($"'{nameof(userId)}' cannot be null or empty.", nameof(userId));
            }

            UserId = userId;
        }

        public string UserId { get; private set; }
    }
}
