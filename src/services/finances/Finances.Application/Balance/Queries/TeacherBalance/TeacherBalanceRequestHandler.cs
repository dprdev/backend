﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Finances.Domain;
using Finances.Domain.Entities;

using MediatR;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Finances.Application.Balance.Queries.TeacherBalance
{
    internal sealed class TeacherBalanceRequestHandler
        : IRequestHandler<TeacherBalanceRequest, TeacherBalanceResponse>
    {

        private readonly DataContext _context;
        private readonly ILogger<TeacherBalanceRequestHandler> _logger;

        public TeacherBalanceRequestHandler(
            DataContext context,
            ILogger<TeacherBalanceRequestHandler> logger)
        {
            if (context is null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (logger is null)
            {
                throw new ArgumentNullException(nameof(logger));
            }

            _context = context;
            _logger = logger;
        }

        public async Task<TeacherBalanceResponse> Handle(
            TeacherBalanceRequest request, CancellationToken cancellationToken)
        {
            TeacherWallet wallet = await GetWallet(TeacherWallet.GetKey(request.UserId), cancellationToken);
            if (wallet is null)
            {
                // TODO: handle not found exception
            }

            return new(new TeacherBalance(wallet.Balance, wallet.GetWithdrawPendingBalance())); 
        }

        private Task<TeacherWallet> GetWallet(string id, CancellationToken cancellationToken)
        {
            return _context
                .Wallets
                .OfType<TeacherWallet>()
                .AsNoTrackingWithIdentityResolution()
                .Include(e => e.Operations.Where(e => true))
                    .ThenInclude(e => e.Operations)
                .FirstOrDefaultAsync(e => e.Id == id,
                                        cancellationToken: cancellationToken);
        }
    }
}
