﻿using System;

using OneOf;

namespace Finances.Application.Balance.Queries.TeacherBalance
{
    public sealed class TeacherBalanceResponse : OneOfBase<TeacherBalance, Exception>
    {
        public TeacherBalanceResponse(OneOf<TeacherBalance, Exception> input) : base(input)
        {
        }
    }
}
