﻿namespace Finances.Application.Balance.Queries.TeacherBalance
{
    public sealed class TeacherBalance
    {
        public TeacherBalance(
            int totalBalance,
            int withdrawPending)
        {
            Total = totalBalance;
            Reserved = withdrawPending;
        }

        public int Total { get; private set; }
        public int Reserved { get; private set; }
        public int Available => Total - Reserved;
    }
}
