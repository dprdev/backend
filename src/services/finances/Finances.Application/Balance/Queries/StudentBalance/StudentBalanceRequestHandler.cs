﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Finances.Domain;
using Finances.Domain.Entities;

using MediatR;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Finances.Application.Balance.Queries.StudentBalance
{
    internal sealed class StudentBalanceRequestHandler
        : IRequestHandler<StudentBalanceRequest, StudentBalanceResponse>
    {
        private readonly DataContext _context;
        private readonly ILogger<StudentBalanceRequestHandler> _logger;

        public StudentBalanceRequestHandler(
            DataContext context,
            ILogger<StudentBalanceRequestHandler> logger)
        {
            if (context is null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (logger is null)
            {
                throw new ArgumentNullException(nameof(logger));
            }

            _context = context;
            _logger = logger;
        }

        public async Task<StudentBalanceResponse> Handle(
            StudentBalanceRequest request, CancellationToken cancellationToken)
        {
            StudentWallet wallet = await GetWallet(StudentWallet.GetKey(request.UserId), cancellationToken);
            if (wallet is null)
            {
                // todo: handle error 
            }

            return new(new StudentBalance(wallet.Balance, wallet.GetReservedBalance()));
        }

        private Task<StudentWallet> GetWallet(string id, CancellationToken cancellationToken)
        {
            return _context
                .Wallets
                .OfType<StudentWallet>()
                .AsNoTrackingWithIdentityResolution()
                .Include(e => e.Operations.Where(e => e.State == OperationState.Pending
                                                      && (e.Type == OperationType.PackagePayment
                                                          || string.IsNullOrEmpty(e.ParentId) && e.Type == OperationType.LessonPayment)))
                    .ThenInclude(e => e.Operations)
                .FirstOrDefaultAsync(e => e.Id == id,
                                        cancellationToken: cancellationToken);
        }
    }
}
