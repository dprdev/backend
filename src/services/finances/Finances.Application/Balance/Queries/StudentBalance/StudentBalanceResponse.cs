﻿using System;

using OneOf;

namespace Finances.Application.Balance.Queries.StudentBalance
{
    public sealed class StudentBalanceResponse : OneOfBase<StudentBalance, Exception>
    {
        public StudentBalanceResponse(
            OneOf<StudentBalance, Exception> input) : base(input)
        {
        }
    }
}
