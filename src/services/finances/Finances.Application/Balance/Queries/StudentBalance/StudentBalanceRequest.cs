﻿
using System;

using MediatR;

namespace Finances.Application.Balance.Queries.StudentBalance
{
    public sealed class StudentBalanceRequest : IRequest<StudentBalanceResponse>
    {
        public StudentBalanceRequest(string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                throw new ArgumentException($"'{nameof(userId)}' cannot be null or empty.", nameof(userId));
            }

            UserId = userId;
        }

        public string UserId { get; private set; }
    }
}
