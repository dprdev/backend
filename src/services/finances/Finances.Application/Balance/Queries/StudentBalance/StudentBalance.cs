﻿namespace Finances.Application.Balance.Queries.StudentBalance
{
    public sealed class StudentBalance
    {
        public StudentBalance(
            int totalBalance,
            int reservedBalance)
        {
            Total = totalBalance;
            Reserved = reservedBalance;
        }

        public int Total { get; private set; }
        public int Reserved { get; private set; }
        public int Available => Total - Reserved;
    }
}
