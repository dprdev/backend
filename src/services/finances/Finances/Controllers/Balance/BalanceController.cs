﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;

using Finances.Application.Balance.Queries.StudentBalance;
using Finances.Application.Balance.Queries.TeacherBalance;

using MediatR;

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Finances.Controllers.Balance
{
    [ApiVerV1]
    [JwtAuthorize]
    [ApiController]
    [Route("api/v{version:apiVersion}/balance")]
    public class BalanceController : ApiController
    {
        private readonly IMediator _mediator;
        private readonly ILogger<BalanceController> _logger;

        public BalanceController(
            IMediator mediator,
            ILogger<BalanceController> logger)
        {
            if (mediator is null)
            {
                throw new ArgumentNullException(nameof(mediator));
            }

            if (logger is null)
            {
                throw new ArgumentNullException(nameof(logger));
            }

            _mediator = mediator;
            _logger = logger;
        }

        [HttpGet("teacher")]
        public async Task<IActionResult> TeacherOverview()
        {
            TeacherBalanceResponse res = await _mediator.Send(new TeacherBalanceRequest(User.GetId()));
            return res
                .Match<IActionResult>(
                    (result) => Json(result),
                    (exception) =>
                    {
                        // TODO: handle result
                        return Problem();
                    });
        }

        [HttpGet("student")]
        public async Task<IActionResult> StudentOverview()
        {
            StudentBalanceResponse res = await _mediator.Send(new StudentBalanceRequest(User.GetId()));
            return res
                .Match<IActionResult>(
                    (result) => Json(result),
                    (exception) =>
                    {
                        // TODO: handle result
                        return Problem();
                    });
        }
    }
}
