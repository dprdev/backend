﻿using System;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc;

namespace Finances.Controllers.Payments
{
    [ApiVerV1]
    [ApiController]
    [Route("api/v{version:apiVersion}/payments")]
    [JwtAuthorize]
    public sealed class PaymentsController : ApiController
    {
        [HttpGet("teacher/history")]
        public Task<IActionResult> GetTeacherHistory()
        {
            throw new NotImplementedException(nameof(GetTeacherHistory));
        }

        [HttpGet("student/history")]
        public Task<IActionResult> GetStudentHistory()
        {
            throw new NotImplementedException(nameof(GetStudentHistory));
        }
    }
}
