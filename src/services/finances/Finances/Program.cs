﻿using System.Reflection;

using Hellang.Middleware.ProblemDetails;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

[assembly: HostingStartup(typeof(Finances.Infrastructure.EfCore.HostingStartup))]
[assembly: HostingStartup(typeof(Finances.Infrastructure.AutoMapper.HostingStartup))]
[assembly: HostingStartup(typeof(Finances.Infrastructure.Masstransit.HostingStartup))]
[assembly: HostingStartup(typeof(Finances.Infrastructure.MediatR.HostingStartup))]

IHostBuilder builder = Host
    .CreateDefaultBuilder(args)
    .ConfigureWebHostDefaults(webBuilder =>
    {
        webBuilder.ConfigureServices((services) =>
        {
            services.AddProblemDetails();
            services.AddDefaultApiVersioning();
            services.AddControllers()
                .AddDefaultFluentValidation((cfg) =>
                {
                    cfg.RegisterValidatorsFromAssembly(Assembly.Load("Finances"));
                    cfg.RegisterValidatorsFromAssembly(Assembly.Load("Finances.Application"));
                    cfg.RegisterValidatorsFromAssembly(Assembly.Load("Finances.Domain"));
                });

            services.AddAuthorization();
            services.AddAuthentication();
        });

        webBuilder.Configure((app) =>
        {
            app.UseProblemDetails();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints((endpoints) =>
            {
                endpoints.MapControllers();
            });
        });
    });

await builder.StartAsync();
