﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using Yarp.ReverseProxy.Abstractions;

namespace ApiGw.Spa.Infrastructure.ReverseProxy
{
    public sealed class HostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((host, services) =>
            {
                services.AddReverseProxy()
                    .LoadFromMemory(GetRoutes(host.Configuration).ToArray(),
                                    GetClusters(host.Configuration).ToArray());
            });
        }

        // https://developers.google.com/identity/sign-in/web/sign-in
        // https://www.linkedin.com/pulse/securing-net-core-web-api-identityserver4-using-owner-dalvandi-1/?trk=mp-author-card
        private static IEnumerable<ProxyRoute> GetRoutes(IConfiguration _)
        {
            yield return new()
            {
                RouteId = "teachers",
                ClusterId = "teachers",
                Match = new RouteMatch
                {
                    Path = "/api/t/{**rest}"
                },
                Transforms = new IReadOnlyDictionary<string, string>[]
                {
                    new Dictionary<string, string>
                    {
                        { "PathPattern", "api/{rest}" }
                    }
                }
            }; 

            yield return new()
            {
                RouteId = "identity",
                ClusterId = "identity",
                Match = new RouteMatch
                {
                    Path = "/api/i/{**rest}"
                },
                Transforms = new IReadOnlyDictionary<string, string>[]
                {
                    new Dictionary<string, string>
                    {
                        { "PathPattern", "api/{rest}" }
                    }
                }
            };
        }

        private static IEnumerable<Cluster> GetClusters(IConfiguration cfg)
        {
            yield return new()
            {
                Id = "teachers",
                Destinations = new Dictionary<string, Destination>(StringComparer.OrdinalIgnoreCase)
                {
                    { "teachers/destination1", new() { Address = cfg.GetValue<string>("teachersUrl") } }
                }
            };

            yield return new()
            {
                Id = "identity",
                Destinations = new Dictionary<string, Destination>(StringComparer.OrdinalIgnoreCase)
                {
                    { "identity/destination1", new() { Address = cfg.GetValue<string>("identityUrl") } }
                }
            };
        }
    }
}
