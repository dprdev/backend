﻿using System.Collections.Generic;

using Microsoft.Extensions.DependencyInjection;

using Yarp.ReverseProxy.Abstractions;
using Yarp.ReverseProxy.Service;

namespace ApiGw.Spa.Infrastructure.ReverseProxy
{
    public static class InMemoryConfigProviderExtensions
    {
        public static IReverseProxyBuilder LoadFromMemory(
            this IReverseProxyBuilder builder, IReadOnlyList<ProxyRoute> routes, IReadOnlyList<Cluster> clusters)
        {
            builder.Services.AddSingleton<IProxyConfigProvider>(new InMemoryConfigProvider(routes, clusters));
            return builder;
        }
    }
}
