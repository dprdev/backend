﻿using System;
using System.Collections.Generic;
using System.Threading;

using Microsoft.Extensions.Primitives;

using Yarp.ReverseProxy.Abstractions;
using Yarp.ReverseProxy.Service;

namespace ApiGw.Spa.Infrastructure.ReverseProxy
{
    internal class InMemoryConfig : IProxyConfig
    {
        private readonly CancellationTokenSource _cts = new();

        public InMemoryConfig(
            IReadOnlyList<ProxyRoute> routes,
            IReadOnlyList<Cluster> clusters)
        {
            Routes = routes
                ?? throw new ArgumentNullException(nameof(routes));
            Clusters = clusters
                ?? throw new ArgumentNullException(nameof(clusters));

            ChangeToken = new CancellationChangeToken(_cts.Token);
        }

        public IReadOnlyList<ProxyRoute> Routes { get; }

        public IReadOnlyList<Cluster> Clusters { get; }

        public IChangeToken ChangeToken { get; }

        internal void SignalChange()
        {
            _cts.Cancel();
        }
    }
}
