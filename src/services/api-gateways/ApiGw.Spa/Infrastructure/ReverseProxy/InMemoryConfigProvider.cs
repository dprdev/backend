﻿using System;
using System.Collections.Generic;

using Yarp.ReverseProxy.Abstractions;
using Yarp.ReverseProxy.Service;

namespace ApiGw.Spa.Infrastructure.ReverseProxy
{
    internal class InMemoryConfigProvider : IProxyConfigProvider
    {
        private volatile InMemoryConfig _config;

        public InMemoryConfigProvider(
            IReadOnlyList<ProxyRoute> routes,
            IReadOnlyList<Cluster> clusters)
        {
            if (routes is null)
            {
                throw new ArgumentNullException(nameof(routes));
            }

            if (clusters is null)
            {
                throw new ArgumentNullException(nameof(clusters));
            }

            _config = new InMemoryConfig(routes, clusters);
        }

        public IProxyConfig GetConfig()
        {
            return _config;
        }

        public void Update(IReadOnlyList<ProxyRoute> routes, IReadOnlyList<Cluster> clusters)
        {
            InMemoryConfig oldConfig = _config;
            _config = new InMemoryConfig(routes, clusters);
            oldConfig.SignalChange();
        }
    }
}
