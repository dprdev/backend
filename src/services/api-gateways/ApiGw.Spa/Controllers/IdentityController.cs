﻿using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

using IdentityModel.Client;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiGw.Spa.Controllers
{
    [ApiController]
    [Route("api/v1/identity")]
    public sealed class IdentityController : ApiController
    {
        [Authorize]
        [HttpGet("profile")]
        public async Task<IActionResult> Profile([FromServices] IHttpClientFactory httpClientFactory)
        {
            HttpClient client = httpClientFactory.CreateClient("identity");
            HttpRequestMessage request = new(HttpMethod.Get, "api/v1/profile")
            {
                Headers =
                {
                    { "Authorization", HttpContext.Request.Headers["Authorization"][0] }
                }
            };

            HttpResponseMessage result = await client.SendAsync(request);
            try
            {
                result.EnsureSuccessStatusCode();
            }
            catch (HttpRequestException ex)
            {
                if (ex.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    ValidationProblemDetails details = 
                        await result.Content.ReadFromJsonAsync<ValidationProblemDetails>();

                    return ValidationProblem(details);
                }

                ProblemDetails problemDetails = await result.Content.ReadFromJsonAsync<ValidationProblemDetails>();
                return new ObjectResult(problemDetails)
                {
                    StatusCode = (int?)result.StatusCode
                };
            }

            return Json(await result.Content.ReadFromJsonAsync<object>());
        }

        [AllowAnonymous]
        [HttpPost("session")]
        public async Task<IActionResult> Session([FromServices] IHttpClientFactory httpClientFactory)
        {
            HttpClient client = httpClientFactory.CreateClient("identity");
            TokenResponse result = await client.RequestPasswordTokenAsync(new()
            {
                ClientId = "spa",
                ClientSecret = "spa-secret",
                Password = "",
                UserName = ""
            });

            if (result.IsError)
            {
            }

            return Json(new
            {
            });
        }
    }
}
