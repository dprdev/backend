﻿
using System;

using Hellang.Middleware.ProblemDetails;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

// TODO: add csrf/xsrf protection

[assembly: HostingStartup(typeof(ApiGw.Spa.Infrastructure.ReverseProxy.HostingStartup))]

IHostBuilder builder = Host
    .CreateDefaultBuilder(args)
    .ConfigureWebHostDefaults(webBuilder =>
    {
        webBuilder.ConfigureServices((s) =>
        {
            s.AddControllers(mvc =>
            {
                mvc.Filters.Insert(0, new AutoValidateAntiforgeryTokenAttribute());
            });

            s.AddDataProtection()
                .SetApplicationName("SharedCookieApp")
                .PersistKeysToFileSystem(new ("smth-path-to-directory")); 

            s.AddProblemDetails();
            s.AddHttpClient("identity")
                .ConfigureHttpClient((services, client) =>
                {
                    client.BaseAddress = new Uri(
                        services.GetService<IConfiguration>()
                            .GetValue<string>("identityUrl"));
                });

            //s.AddAntiforgery(options =>
            //{
            //    options.HeaderName = "x-xsrf-token"; 
            //});
        });

        webBuilder.Configure((host, app) =>
        {
            app.UseProblemDetails();
            app.UseCookiePolicy(new()
            {
                Secure = CookieSecurePolicy.Always,
                MinimumSameSitePolicy = SameSiteMode.Strict,
                OnAppendCookie = (cookieCtx) =>
                {

                }
            });

            app.Use((context, next) =>
            {
                context.Response.Headers.Add("X-Content-Type-Options", "nosniff");
                context.Response.Headers.Add("X-Xss-Protection", "1");
                context.Response.Headers.Add("X-Frame-Options", "DENY");

                return next();
            });

            //app.Use((context, next) =>
            //{
            //    IAntiforgery anfg = context.GetRequiredService<IAntiforgery>();
            //    AntiforgeryTokenSet tokens = anfg.GetAndStoreTokens(context);

            //    context.Response.Cookies.Append("x-csrf", tokens.RequestToken, new()
            //    {
            //        HttpOnly = false
            //    });

            //    return next();
            //});

            app.Use((context, next) =>
            {
                string token = context.Request.Cookies[".AspNetCore.Application.Id"];
                if (!string.IsNullOrEmpty(token))
                {
                    context.Request.Headers.Add("Authorization", $"Bearer {token}");
                }

                return next();
            });


            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints((endpoints) =>
            {
                endpoints.MapControllers();
                endpoints.MapReverseProxy((builder) =>
                {
                });
            });
        });
    });

await builder.StartAsync();
