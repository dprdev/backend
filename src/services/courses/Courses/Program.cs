﻿using System.Reflection;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace Courses
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            IHostBuilder builder = Host
                .CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });

            IHost host = builder.Build();
            await host.ExecuteStartupTask(
                assemblies: Assembly.GetExecutingAssembly()).StartAsync();
        }
    }
}
