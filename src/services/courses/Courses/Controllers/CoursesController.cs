﻿
using System;
using System.Security.Claims;
using System.Threading.Tasks;

using Courses.Application.Commands.CreateLesson;
using Courses.Application.Queries.GetLessonPriceView;
using Courses.Application.Queries.GetUserCourses;

using MediatR;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

// TODO: [courses] rename service with routes to lessons (or smth similar) 
namespace Courses.Controllers
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/courses")]
    public sealed class CoursesController : Controller
    {
        [HttpGet("{id}")]
        public Task<IActionResult> Get([FromServices] IMediator mediator, [FromRoute] string id)
        {
            throw new NotImplementedException(nameof(Get));
        }

        [HttpGet("{id}/price/{price}/view")]
        public async Task<IActionResult> GetCoursePriceView(
            [FromServices] IMediator mediator,
            [FromServices] ILogger<CoursesController> logger,
            [FromRoute] string id,
            [FromRoute] string price)
        {
            using IDisposable loggerScope = logger.BeginScope(nameof(GetCoursePriceView));
            try
            {
                GetLessonPriceViewResponse result =
                    await mediator.Send(new GetLessonPriceViewRequest(id, price));

                return Json(result);
            }
            catch (Exception)
            {
                throw;
            }
        }


        [HttpGet("my")]
        public async Task<IActionResult> GetMyCourses([FromServices] IMediator mediator)
        {
            GetUserCoursesResponse response = await mediator.Send(new GetUserCoursesRequest(User.GetId()));
            return Json(new
            {
                response.Items
            });
        }


        [HttpPost("my")]
        public async Task<IActionResult> CreateLesson(
            [FromServices] IMediator mediator,
            [FromBody] CreateLessonDto dto)
        {
            var req = new CreateLessonRequest(User.GetId(), dto);
            try
            {
                CreateLessonResponse res = await mediator.Send(req);

                return CreatedAtAction(
                    nameof(Get),
                    new { res.Id },
                    new
                    {
                    });
            }
            catch (Exception)
            {
                throw; // TODO: do smth with exception
            }
        }

        [HttpGet("teacher-courses/{user}")]
        public async Task<IActionResult> GetTeacherCourses([FromServices] IMediator mediator, [FromRoute] string user)
        {
            GetUserCoursesResponse response = await mediator.Send(new GetUserCoursesRequest(user));
            return Json(new
            {
                response.Items
            });
        }

        [HttpGet("settings/default-prices")]
        public IActionResult GetDefaultPriceSettings()
        {
            return Json(new
            {
                items = new[]
                {
                    new { duration = 30, price = 10, packageSize = 5, packagePrice = 40 },
                    new { duration = 45, price = 15, packageSize = 5, packagePrice = 60 },
                    new { duration = 60, price = 20, packageSize = 5, packagePrice = 80 },
                    new { duration = 90, price = 25, packageSize = 5, packagePrice = 100 },
                }
            });
        }
    }
}
