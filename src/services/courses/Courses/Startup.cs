﻿
using System;
using System.Reflection;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

[assembly: HostingStartup(typeof(Courses.Infrastructure.EfCore.HostingStartup))]
[assembly: HostingStartup(typeof(Courses.Infrastructure.Masstransit.HostingStartup))]
[assembly: HostingStartup(typeof(Courses.Infrastructure.MediatR.HostingStartup))]
namespace Courses
{
    public class Startup
    {
        public Startup(
            IConfiguration cfg,
            IWebHostEnvironment env)
        {
            if (cfg is null)
            {
                throw new ArgumentNullException(nameof(cfg));
            }

            if (env is null)
            {
                throw new ArgumentNullException(nameof(env));
            }

            Cfg = cfg;
            Env = env;
        }

        public IConfiguration Cfg { get; private set; }
        public IWebHostEnvironment Env { get; private set; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthorization();
            services.AddAuthentication();
            services.AddControllers()
                .AddDefaultFluentValidation(cfg =>
                {
                    cfg.RegisterValidatorsFromAssembly(Assembly.Load("Courses"));
                    cfg.RegisterValidatorsFromAssembly(Assembly.Load("Courses.Application"));
                    cfg.RegisterValidatorsFromAssembly(Assembly.Load("Courses.Domain"));
                });
        }

        public void Configure(IApplicationBuilder app)
        {
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
