﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Courses.Domain.Entities
{
    public class Lesson
    {
        private readonly List<LessonPrice> _prices;

        private Lesson()
        {
        }

        public Lesson(
            string name,
            string description,
            string language,
            string teacher,
            IEnumerable<LessonPrice> prices = null)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentException($"'{nameof(name)}' cannot be null or empty.", nameof(name));
            }

            if (string.IsNullOrEmpty(description))
            {
                throw new ArgumentException($"'{nameof(description)}' cannot be null or empty.", nameof(description));
            }

            if (string.IsNullOrEmpty(language))
            {
                throw new ArgumentException($"'{nameof(language)}' cannot be null or empty.", nameof(language));
            }

            if (string.IsNullOrEmpty(teacher))
            {
                throw new ArgumentException($"'{nameof(teacher)}' cannot be null or empty.", nameof(teacher));
            }

            Id = Guid
                .NewGuid()
                .ToString();

            Name = name;
            Description = description;
            Language = language;
            TeacherId = teacher;

            CreatedDate = DateTime.UtcNow;

            _prices =
                prices?.ToList()
                ?? new(0)
                {
                };
        }

        public string Id { get; private set; }
        public string Name { get; private set; }
        public string Description { get; private set; }
        public string Language { get; private set; }
        public string TeacherId { get; private set; }

        public DateTime CreatedDate { get; private set; }

        public IReadOnlyList<LessonPrice> Prices => _prices;


        public LessonPrice GetPrice(string priceId)
        {
            if (_prices is null)
            {
                throw new InvalidOperationException("Data is not loaded");
            }


            return _prices.FirstOrDefault(e => e.Id == priceId);
        }
    }
}
