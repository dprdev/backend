﻿using System;

namespace Courses.Domain.Entities
{
    public class LessonCategory
    {
        private LessonCategory()
        {
        }

        public LessonCategory(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentException($"'{nameof(name)}' cannot be null or empty.", nameof(name));
            }

            Id = Guid
                .NewGuid()
                .ToString();

            Name = name;
        }

        public string Id { get; private set; }
        public string Name { get; private set; }
        public string ParentId { get; private set; }
    }
}
