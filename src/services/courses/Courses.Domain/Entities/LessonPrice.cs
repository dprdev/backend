﻿using System;

namespace Courses.Domain.Entities
{
    public class LessonPrice
    {
        private LessonPrice()
        {
        }

        public LessonPrice(
            int price,
            int duration,
            int pSize = 0,
            int pPrice = 0,
            bool enabled = true)
        {
            if (price <= 0)
            {
                throw new ArgumentException("Invalid price amount", nameof(price));
            }

            if (duration < 30)
            {
                throw new ArgumentException("Invalid lesson duration", nameof(duration));
            }

            if (pSize > 0 && pPrice <= 0)
            {
                throw new ArgumentException("Package price must be greater then 0", nameof(pPrice));
            }

            Price = price;
            Duration = duration;
            PackageSize = pSize;
            PackagePrice = pPrice;
            Enabled = enabled;
        }


        public string Id { get; private set; }
        public string CourseId { get; private set; }

        public bool Enabled { get; private set; }

        public int PackageSize { get; private set; }
        public int PackagePrice { get; private set; }

        public int Price { get; private set; }
        public int Duration { get; private set; }

        public bool HasPackage =>
            PackageSize > 1 && PackagePrice > 0;
    }
}
