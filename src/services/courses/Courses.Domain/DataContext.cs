﻿
using Courses.Domain.Entities;

using Microsoft.EntityFrameworkCore;

namespace Courses.Domain
{
    public class DataContext : DbContext
    {
        public DataContext()
        {
        }

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        public virtual DbSet<Lesson> Lessons { get; private set; }
        public virtual DbSet<LessonCategory> Categories { get; private set; }
        public virtual DbSet<LessonPrice> Prices { get; private set; }
    }
}
