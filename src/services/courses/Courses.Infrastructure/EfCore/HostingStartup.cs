﻿
using Courses.Domain;

using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using Npgsql.EntityFrameworkCore.PostgreSQL.Infrastructure;

namespace Courses.Infrastructure.EfCore
{
    public sealed class HostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((host, services) =>
            {
                services.AddDbContext<DataContext>(ctxBuilder =>
                {
                    ctxBuilder.ReplaceService<IModelCustomizer, CustomModelCustomizer>();
                    ctxBuilder.ReplaceService<IMigrationsAssembly, CustomMigrationsAssembly>();
                    ctxBuilder.UseNpgsql(
                        host.Configuration.GetValue<string>("dbConnection"),
                        ConfigurePgsql);
                });
            });
        }

        private void ConfigurePgsql(NpgsqlDbContextOptionsBuilder options)
        {
            options.MigrationsAssembly("Courses.Infrastructure");
        }
    }
}
