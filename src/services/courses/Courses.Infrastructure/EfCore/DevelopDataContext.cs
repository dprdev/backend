﻿using Courses.Domain;

using Microsoft.EntityFrameworkCore;

namespace Courses.Infrastructure.EfCore
{
    internal sealed class DevelopDataContext : DataContext
    {
        public DevelopDataContext(
            DbContextOptions<DataContext> options) : base(options)
        {

        }
    }
}
