﻿
using Courses.Domain;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace Courses.Infrastructure.EfCore
{
    internal sealed class Factory : IDesignTimeDbContextFactory<DevelopDataContext>
    {
        public DevelopDataContext CreateDbContext(string[] args)
        {
            DbContextOptions<DataContext> o =
                new DbContextOptionsBuilder<DataContext>()
                    .UseNpgsql("localserver")
                    .ReplaceService<IModelCustomizer, CustomModelCustomizer>()
                    .Options;

            return new DevelopDataContext(o);
        }
    }
}
