﻿#pragma warning disable EF1001 // Internal api usage

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Migrations.Internal;

namespace Courses.Infrastructure.EfCore
{
    internal sealed class CustomMigrationsAssembly : MigrationsAssembly
    {

        private readonly Type _contextType;
        private IReadOnlyDictionary<string, TypeInfo> _migrations;

        public CustomMigrationsAssembly(
            [NotNull] ICurrentDbContext currentContext,
            [NotNull] IDbContextOptions options,
            [NotNull] IMigrationsIdGenerator idGenerator,
            [NotNull] IDiagnosticsLogger<DbLoggerCategory.Migrations> logger)
            : base(currentContext, options, idGenerator, logger)
        {
            _contextType = currentContext.Context.GetType();
        }


        public override IReadOnlyDictionary<string, TypeInfo> Migrations =>
            _migrations ??= GetMigrations();

        private IReadOnlyDictionary<string, TypeInfo> GetMigrations()
        {
            var result = new SortedList<string, TypeInfo>();

            IEnumerable<(string id, TypeInfo t)> items
                = from t in Assembly.DefinedTypes.Where(
                t => !t.IsAbstract
                    && !t.IsGenericTypeDefinition)
                  where t.IsSubclassOf(typeof(Migration))
                      && (t.GetCustomAttribute<DbContextAttribute>()?.ContextType.IsAssignableTo(_contextType) ?? false)
                  let id = t.GetCustomAttribute<MigrationAttribute>()?.Id
                  orderby id
                  select (id, t);

            foreach ((string id, TypeInfo t) in items)
            {
                if (id == null)
                {
                    continue;
                }

                result.Add(id, t);
            }

            return result;
        }
    }
}
