﻿
using Courses.Domain.Entities;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Courses.Infrastructure.EfCore.EntityTypeConfigurations
{
    internal sealed class LessonCategoryConfiguration : IEntityTypeConfiguration<LessonCategory>
    {
        public void Configure(
            EntityTypeBuilder<LessonCategory> builder)
        {
            builder.HasKey(e => e.Id);
            builder.HasIndex(e => e.ParentId);

            builder.Property(e => e.Id).HasMaxLength(256);
            builder.Property(e => e.ParentId).HasMaxLength(256);
            builder.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(256);
        }
    }
}
