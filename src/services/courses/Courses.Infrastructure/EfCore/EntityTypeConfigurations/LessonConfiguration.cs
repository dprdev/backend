﻿
using Courses.Domain.Entities;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Courses.Infrastructure.EfCore.EntityTypeConfigurations
{
    internal sealed class LessonConfiguration : IEntityTypeConfiguration<Lesson>
    {
        public void Configure(
            EntityTypeBuilder<Lesson> builder)
        {
            builder.HasKey(e => e.Id);
            builder.HasIndex(e => e.TeacherId);

            builder.Property(e => e.Id).HasMaxLength(256);
            builder.Property(e => e.TeacherId).HasMaxLength(256);

            builder.Property(e => e.Name).HasMaxLength(256);
            builder.Property(e => e.Description).HasMaxLength(1000);
            builder.Property(e => e.Language).HasMaxLength(2);

            builder.HasMany(e => e.Prices)
                .WithOne()
                .HasForeignKey(e => e.CourseId);
        }
    }
}
