﻿
using Courses.Domain.Entities;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Courses.Infrastructure.EfCore.EntityTypeConfigurations
{
    internal sealed class LessonPriceConfiguration : IEntityTypeConfiguration<LessonPrice>
    {
        public void Configure(
            EntityTypeBuilder<LessonPrice> builder)
        {
            builder.HasKey(e => e.Id);
            builder.HasIndex(e => e.CourseId);

            builder.Property(e => e.Id).HasMaxLength(256);
            builder.Property(e => e.CourseId).HasMaxLength(256);
        }
    }
}
