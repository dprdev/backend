﻿using System.Text.Json;

using MassTransit;
using MassTransit.ExtensionsDependencyInjectionIntegration;

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Courses.Infrastructure.Masstransit.Configurations
{
    internal static class MassTransitRabbitConfiguration
    {
        internal static void Configure(
            IConfiguration cfg,
            IWebHostEnvironment env,
            IServiceCollection services,
            IServiceCollectionBusConfigurator mtCfg)
        {
            mtCfg.UsingRabbitMq((busCfg, ctx) =>
            {
                RabbitSettings settings = GetSettings(cfg);
                ctx.Host(settings.Host, settings.Port, "/", host =>
                {
                    host.Username(settings.User);
                    host.Password(settings.Password);
                });
            });
        }

        private static RabbitSettings GetSettings(IConfiguration cfg)
        {
            try
            {
                return JsonSerializer.Deserialize<RabbitSettings>(
                    cfg.GetValue<string>("rabbitSettings"),
                    new()
                    {
                        PropertyNamingPolicy = JsonNamingPolicy.CamelCase
                    });
            }
            catch
            {
                return new RabbitSettings
                {
                    Port = 6379,
                    Host = "rabbitmq",
                    User = "medoge",
                    Password = "medoge343"
                };
            }
        }

        private sealed class RabbitSettings
        {
            public string Host { get; set; }
            public ushort Port { get; set; }
            public string User { get; set; }
            public string Password { get; set; }
        }
    }
}
