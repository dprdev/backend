﻿using System;

namespace Courses.Application.Commands.CreateLesson
{
    public sealed class CreateLessonResponse
    {
        public CreateLessonResponse(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentException($"'{nameof(id)}' cannot be null or empty.", nameof(id));
            }

            Id = id;
        }

        public string Id { get; }
    }
}
