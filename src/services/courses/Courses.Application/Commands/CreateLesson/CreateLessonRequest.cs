﻿
using System;

using MediatR;

namespace Courses.Application.Commands.CreateLesson
{
    public sealed class CreateLessonRequest : IRequest<CreateLessonResponse>
    {
        public CreateLessonRequest(string userId, CreateLessonDto lesson)
        {
            if (string.IsNullOrEmpty(userId))
            {
                throw new ArgumentException($"'{nameof(userId)}' cannot be null or empty.", nameof(userId));
            }

            if (lesson is null)
            {
                throw new ArgumentNullException(nameof(lesson));
            }

            UserId = userId;
            Lesson = lesson;
        }

        public string UserId { get; private set; }
        public CreateLessonDto Lesson { get; private set; }
    }
}
