﻿namespace Courses.Application.Commands.CreateLesson
{
    public class CreateLessonPriceDto
    {
        public int Price { get; private set; }
        public int Duration { get; private set; }
        public int PackageSize { get; private set; }
        public int PackagePrice { get; private set; }
        public bool Enabled { get; private set; }
    }
}
