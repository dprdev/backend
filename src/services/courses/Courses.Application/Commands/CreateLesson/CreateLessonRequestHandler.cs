﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Courses.Domain;
using Courses.Domain.Entities;

using MediatR;

using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Courses.Application.Commands.CreateLesson
{
    internal sealed class CreateLessonRequestHandler
        : IRequestHandler<CreateLessonRequest, CreateLessonResponse>
    {
        private readonly DataContext _context;

        public CreateLessonRequestHandler(DataContext context)
        {
            if (context is null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            _context = context;
        }

        // TODO: handle exception. 
        public async Task<CreateLessonResponse> Handle(
            CreateLessonRequest request, CancellationToken cancellationToken)
        {
            EntityEntry<Lesson> entry = _context.Lessons.Add(BuildLesson(request));
            try
            {
                await _context.SaveChangesAsync(cancellationToken);
            }
            catch (Exception)
            {
            }

            return new(entry.Entity.Id);
        }

        private static Lesson BuildLesson(CreateLessonRequest request)
        {
            string userId = request.UserId;
            CreateLessonDto lesson = request.Lesson;
            IEnumerable<LessonPrice> prices = lesson.Prices.Select(e =>
            {
                return new LessonPrice(e.Price, e.Duration, e.PackageSize, e.PackagePrice, e.Enabled);
            });

            return new(lesson.Name, lesson.Description, lesson.Language, userId, prices);
        }
    }
}
