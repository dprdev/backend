﻿using System.Collections.Generic;

namespace Courses.Application.Commands.CreateLesson
{
    public class CreateLessonDto
    {
        public string Name { get; private set; }
        public string Description { get; private set; }
        public string Language { get; private set; }
        public IEnumerable<CreateLessonPriceDto> Prices { get; private set; }
    }
}
