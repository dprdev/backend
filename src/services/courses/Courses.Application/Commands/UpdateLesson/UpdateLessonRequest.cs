﻿
using MediatR;

namespace Courses.Application.Commands.UpdateLesson
{
    public sealed class UpdateLessonRequest : IRequest<UpdateLessonResponse>
    {
    }
}
