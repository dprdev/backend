﻿using System;
using System.Threading;
using System.Threading.Tasks;

using Courses.Domain;

using MediatR;

namespace Courses.Application.Commands.UpdateLesson
{
    internal sealed class UpdateLessonRequestHandler
        : IRequestHandler<UpdateLessonRequest, UpdateLessonResponse>
    {
        private readonly DataContext _context;

        public UpdateLessonRequestHandler(DataContext context)
        {
            if (context is null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            _context = context;
        }

        public Task<UpdateLessonResponse> Handle(
            UpdateLessonRequest request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
