﻿using System;

using MediatR;

namespace Courses.Application.Queries.GetUserCourses
{
    public sealed class GetUserCoursesRequest : IRequest<GetUserCoursesResponse>
    {
        public GetUserCoursesRequest(string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                throw new ArgumentException($"'{nameof(userId)}' cannot be null or empty.", nameof(userId));
            }

            UserId = userId;
        }

        public string UserId { get; private set; }
    }
}
