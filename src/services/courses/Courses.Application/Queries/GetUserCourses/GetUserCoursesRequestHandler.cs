﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Courses.Domain;
using Courses.Domain.Entities;

using MediatR;

using Microsoft.EntityFrameworkCore;

namespace Courses.Application.Queries.GetUserCourses
{
    internal sealed class GetUserCoursesRequestHandler
        : IRequestHandler<GetUserCoursesRequest, GetUserCoursesResponse>
    {
        private readonly DataContext _context;


        public async Task<GetUserCoursesResponse> Handle(
            GetUserCoursesRequest request, CancellationToken cancellationToken)
        {
            IQueryable<Lesson> query = _context.Lessons.AsNoTrackingWithIdentityResolution();
            List<Lesson> items = await query
                .Where(e => e.TeacherId == request.UserId)
                .ToListAsync(cancellationToken: cancellationToken);

            return new GetUserCoursesResponse(items);
        }
    }
}
