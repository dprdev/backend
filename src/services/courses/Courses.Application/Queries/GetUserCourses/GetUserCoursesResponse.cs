﻿using System;
using System.Collections.Generic;

using Courses.Domain.Entities;

namespace Courses.Application.Queries.GetUserCourses
{
    public sealed class GetUserCoursesResponse
    {
        public GetUserCoursesResponse(List<Lesson> items)
        {
            if (items is null)
            {
                throw new ArgumentNullException(nameof(items));
            }

            Items = items;
        }

        public List<Lesson> Items { get; }
    }
}
