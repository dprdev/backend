﻿namespace Courses.Application.Queries.GetLessonPriceView
{
    public class LessonPrice
    {
        public string Id { get; init; }
        public int Amount { get; init; }
        public int Duration { get; init; }
        public int PackageSize { get; init; }
        public int PackageDiscount { get; init; }
        public bool HasPackage { get; init; }
    }
}
