﻿using System;

using MediatR;

namespace Courses.Application.Queries.GetLessonPriceView
{
    public sealed class GetLessonPriceViewRequest : IRequest<GetLessonPriceViewResponse>
    {
        public GetLessonPriceViewRequest(string courseId, string coursePriceId)
        {
            if (string.IsNullOrEmpty(courseId))
            {
                throw new ArgumentException($"'{nameof(courseId)}' cannot be null or empty.", nameof(courseId));
            }

            if (string.IsNullOrEmpty(coursePriceId))
            {
                throw new ArgumentException($"'{nameof(coursePriceId)}' cannot be null or empty.", nameof(coursePriceId));
            }

            CourseId = courseId;
            CoursePriceId = coursePriceId;
        }

        public string CourseId { get; }
        public string CoursePriceId { get; private set; }
    }
}
