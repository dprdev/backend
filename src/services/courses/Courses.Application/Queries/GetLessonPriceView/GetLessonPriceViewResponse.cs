﻿using System;

namespace Courses.Application.Queries.GetLessonPriceView
{
    public sealed class GetLessonPriceViewResponse
    {
        public GetLessonPriceViewResponse(LessonPriceView lessonPriceView)
        {
            if (lessonPriceView is null)
            {
                throw new ArgumentNullException(nameof(lessonPriceView));
            }

            LessonPriceView = lessonPriceView;
        }

        public LessonPriceView LessonPriceView { get; }
    }
}
