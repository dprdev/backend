﻿using System.Collections.Generic;

namespace Courses.Application.Queries.GetLessonPriceView
{
    public class LessonPriceView
    {
        public string Id { get; init; }
        public string Name { get; init; }
        public string Description { get; init; }
        public string Language { get; init; }
        public LessonPrice CoursePrice { get; init; }
        public string TeacherId { get; init; }
        public IReadOnlyList<string> Categories { get; init; }
    }
}
