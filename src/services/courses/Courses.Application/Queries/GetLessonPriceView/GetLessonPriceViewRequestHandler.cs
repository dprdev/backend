﻿using System;
using System.Threading;
using System.Threading.Tasks;

using Courses.Domain;
using Courses.Domain.Entities;

using MediatR;

using Microsoft.EntityFrameworkCore;

namespace Courses.Application.Queries.GetLessonPriceView
{
    // TODO: replace with operation result or smth else (remove throw exception)
    internal sealed class GetLessonPriceViewRequestHandler
        : IRequestHandler<GetLessonPriceViewRequest, GetLessonPriceViewResponse>
    {
        private readonly DataContext _context;

        public GetLessonPriceViewRequestHandler(DataContext context)
        {
            if (context is null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            _context = context;
        }

        public async Task<GetLessonPriceViewResponse> Handle(
            GetLessonPriceViewRequest request, CancellationToken cancellationToken)
        {
            Lesson lesson = await _context.Lessons.Include(e => e.Prices)
                                                  .AsNoTrackingWithIdentityResolution()
                                                  .FirstOrDefaultAsync(e => e.Id == request.CourseId,
                                                                       cancellationToken: cancellationToken);

            if (lesson is null)
            {
                throw new InvalidOperationException("Entity not found");
            }

            Domain.Entities.LessonPrice price = lesson.GetPrice(request.CoursePriceId);
            if (price is null)
            {
                throw new InvalidOperationException("Entity not found");
            }

            var view = new LessonPriceView
            {
                Id = lesson.Id,
                TeacherId = lesson.TeacherId,
                Description = lesson.Description,
                Name = lesson.Name,
                Language = lesson.Language,
                CoursePrice = new()
                {
                    Id = price.Id,
                    Amount = price.Price,
                    HasPackage = price.HasPackage,
                    Duration = price.Duration,
                    PackageDiscount = price.PackagePrice,
                    PackageSize = price.PackageSize
                }
            };

            return new(view);
        }
    }
}
