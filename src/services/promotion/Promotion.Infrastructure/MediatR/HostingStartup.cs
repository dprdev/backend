﻿using System.Reflection;

using MediatR;

using Microsoft.AspNetCore.Hosting;

namespace Promotion.Infrastructure.MediatR
{
    public sealed class HostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((host, services) =>
            {
                services.AddMediatR(GetAssemblies());
            });
        }

        private static Assembly[] GetAssemblies()
        {
            return new Assembly[]
            {
                Assembly.Load("Promotion.Domain"),
                Assembly.Load("Promotion.Application"),
            };
        }
    }
}
