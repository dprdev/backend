﻿
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.Extensions.DependencyInjection;

namespace Promotion.Infrastructure.Versioning
{
    public sealed class HostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((host, services) =>
            {
                services.AddApiVersioning((cfg) =>
                {
                    cfg.ApiVersionReader = new UrlSegmentApiVersionReader();
                }); 
            });
        } 
    }
}
