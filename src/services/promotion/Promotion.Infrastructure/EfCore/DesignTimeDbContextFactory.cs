﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Infrastructure;

using Promotion.Domain;

namespace Promotion.Infrastructure.EfCore
{
    internal sealed class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<DataContext>
    {
        DataContext IDesignTimeDbContextFactory<DataContext>.CreateDbContext(string[] args)
        {
            DbContextOptionsBuilder<DataContext> optBuilder
                = new DbContextOptionsBuilder<DataContext>()
                    .ReplaceService<IModelCustomizer, CustomModelCustomizer>()
                    .UseNpgsql((cfg) =>
                    {
                        cfg.MigrationsAssembly("Promotion.Infrastructure");
                    });

            return new DataContext(optBuilder.Options);
        }
    }
}
