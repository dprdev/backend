﻿using System.Threading.Tasks;

namespace Promotion.Infrastructure.EfCore.Seeders
{
    internal interface ISeeder
    {
        Task Seed();
    }
}
