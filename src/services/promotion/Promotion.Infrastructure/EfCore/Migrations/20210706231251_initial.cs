﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Promotion.Infrastructure.EfCore.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Promocodes",
                columns: table => new
                {
                    Id = table.Column<string>(type: "text", nullable: false),
                    Discount = table.Column<int>(type: "integer", nullable: false),
                    ValidTo = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Promocodes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PromocodeUsages",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "text", nullable: false),
                    PromocodeId = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PromocodeUsages", x => new { x.UserId, x.PromocodeId });
                    table.ForeignKey(
                        name: "FK_PromocodeUsages_Promocodes_PromocodeId",
                        column: x => x.PromocodeId,
                        principalTable: "Promocodes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PromocodeUsages_PromocodeId",
                table: "PromocodeUsages",
                column: "PromocodeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PromocodeUsages");

            migrationBuilder.DropTable(
                name: "Promocodes");
        }
    }
}
