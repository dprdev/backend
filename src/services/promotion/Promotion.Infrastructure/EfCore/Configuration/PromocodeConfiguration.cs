﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using Promotion.Domain.Entities;

namespace Promotion.Infrastructure.EfCore.Configuration
{
    internal sealed class PromocodeConfiguration
        : IEntityTypeConfiguration<Promocode>
        , IEntityTypeConfiguration<PromocodeUsage>
    {
        public void Configure(EntityTypeBuilder<Promocode> builder)
        {
            builder.HasKey(e => e.Id);
        }

        public void Configure(EntityTypeBuilder<PromocodeUsage> builder)
        {
            builder.HasKey(e => new { e.UserId, e.PromocodeId });
        }
    }
}
