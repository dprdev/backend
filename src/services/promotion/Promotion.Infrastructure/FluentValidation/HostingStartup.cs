﻿
using System.Reflection;

using Microsoft.AspNetCore.Hosting;

namespace Promotion.Infrastructure.FluentValidation
{
    public sealed class HostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((host, services) =>
            {
                // TODO: add fluent validation
                //services.AddDefaultFluentValidation((cfg) =>
                //{
                //    cfg.RegisterValidatorsFromAssembly(Assembly.Load("Promotion"));
                //    cfg.RegisterValidatorsFromAssembly(Assembly.Load("Promotion.Application"));
                //    cfg.RegisterValidatorsFromAssembly(Assembly.Load("Promotion.Domain"));
                //}); 
            });
        }
    }
}
