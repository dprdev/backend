﻿using System.Diagnostics.CodeAnalysis;

using Microsoft.EntityFrameworkCore;

using Promotion.Domain.Entities;

namespace Promotion.Domain
{
    public sealed class DataContext : DbContext
    {
        public DataContext()
        {
        }

        public DataContext([NotNull] DbContextOptions options) : base(options)
        {
        }


        public DbSet<Promocode> Promocodes { get; private set; }
        public DbSet<PromocodeUsage> PromocodeUsages { get; private set; }
    }
}
