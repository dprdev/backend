﻿using System;
using System.Collections.Generic;

namespace Promotion.Domain.Entities
{

    public class Promocode
    {
        private readonly List<PromocodeUsage> _usages;

        private Promocode()
        {
        }

        public Promocode(string id, int discount, DateTime validTo)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentException($"'{nameof(id)}' cannot be null or empty.", nameof(id));
            }

            if (discount <= 0)
            {
                throw new ArgumentException("Discount must be greater then 0", nameof(discount));
            }

            // TODO: or now ? shoud add any time to current date ?
            if (validTo <= DateTime.UtcNow)
            {
                throw new ArgumentException("Invalid expiration date", nameof(validTo));
            }

            Discount = discount;
            ValidTo = validTo;

            _usages = new()
            {
            }; 
        }

        public string Id { get; private set; }
        public int Discount { get; private set; }
        public DateTime ValidTo { get; private set; }

        public IReadOnlyList<PromocodeUsage> Usages => _usages;
    }
}
