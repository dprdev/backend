﻿namespace Promotion.Domain.Entities
{
    public class PromocodeUsage
    {
        public string UserId { get; private set; }
        public string PromocodeId { get; private set; }
    }
}
