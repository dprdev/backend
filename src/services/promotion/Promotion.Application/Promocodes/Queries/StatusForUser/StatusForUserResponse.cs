﻿using System;

using OneOf;

namespace Promotion.Application.Promocodes.Queries.StatusForUser
{
    public sealed class StatusForUserResponse : OneOfBase<object, Exception>
    {
        public StatusForUserResponse(OneOf<object, Exception> input) : base(input)
        {
        }
    }
}
