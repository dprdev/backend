﻿using System;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.Extensions.Logging;

using Promotion.Domain;

namespace Promotion.Application.Promocodes.Queries.StatusForUser
{
    internal sealed class StatusForUserRequestHandler
        : IRequestHandler<StatusForUserRequest, StatusForUserResponse>
    {
        private readonly DataContext _context;
        private readonly ILogger<StatusForUserRequestHandler> _logger;

        public StatusForUserRequestHandler(
            DataContext context,
            ILogger<StatusForUserRequestHandler> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<StatusForUserResponse> Handle(
            StatusForUserRequest request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException(nameof(Handle));
        }
    }
}
