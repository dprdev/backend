﻿
using MediatR;

namespace Promotion.Application.Promocodes.Queries.StatusForUser
{
    public sealed class StatusForUserRequest : IRequest<StatusForUserResponse>
    {
        public string UserId { get; private set; }
    }
}
