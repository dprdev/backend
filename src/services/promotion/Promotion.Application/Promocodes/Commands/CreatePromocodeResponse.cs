﻿using System;

using OneOf;

namespace Promotion.Application.Promocodes.Commands
{
    public sealed class CreatePromocodeResponse : OneOfBase<string, Exception>
    {
        public CreatePromocodeResponse(OneOf<string, Exception> input) : base(input)
        {
        }
    }
}
