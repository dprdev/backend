﻿
using System;

using MediatR;

namespace Promotion.Application.Promocodes.Commands
{
    public sealed class CreatePromocodeRequest : IRequest<CreatePromocodeResponse>
    {
        public CreatePromocodeRequest(int amount, DateTime validTo)
        {
            Amount = amount;
            ValidTo = validTo;
        }

        public int Amount { get; private set; }
        public DateTime ValidTo { get; private set; }
    }
}
