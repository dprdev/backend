﻿using System;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.Extensions.Logging;

using Promotion.Application.Services.Promocodes;
using Promotion.Domain;
using Promotion.Domain.Entities;

namespace Promotion.Application.Promocodes.Commands
{
    internal sealed class CreatePromocodeRequestHandler
        : IRequestHandler<CreatePromocodeRequest, CreatePromocodeResponse>
    {
        private readonly DataContext _context;
        private readonly ILogger<CreatePromocodeRequestHandler> _logger;
        private readonly IPromocodeGenerator _codeGenerator;

        public CreatePromocodeRequestHandler(
            DataContext context,
            IPromocodeGenerator codeGenerator,
            ILogger<CreatePromocodeRequestHandler> logger)
        {
            if (context is null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (codeGenerator is null)
            {
                throw new ArgumentNullException(nameof(codeGenerator));
            }

            if (logger is null)
            {
                throw new ArgumentNullException(nameof(logger));
            }

            _context = context;
            _logger = logger;
            _codeGenerator = codeGenerator;
        }

        public async Task<CreatePromocodeResponse> Handle(
            CreatePromocodeRequest request, CancellationToken cancellationToken)
        {
            var code = new Promocode(
                _codeGenerator.GenerateCode(),
                request.Amount,
                request.ValidTo);

            _context.Add(code);
            try
            {
                await _context.SaveChangesAsync(cancellationToken);
            }
            catch (Exception ex)
            {
                return new(ex);
            }

            return new(code.Id);
        }
    }
}
