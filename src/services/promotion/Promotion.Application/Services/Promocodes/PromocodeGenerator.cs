﻿using System;

namespace Promotion.Application.Services.Promocodes
{
    internal sealed class PromocodeGenerator : IPromocodeGenerator
    {
        // todo: make generated code more user-friendly
        public string GenerateCode()
        {
            return Guid.NewGuid().ToString(); 
        }
    }
}
