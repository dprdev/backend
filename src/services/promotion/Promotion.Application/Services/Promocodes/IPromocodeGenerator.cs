﻿namespace Promotion.Application.Services.Promocodes
{
    public interface IPromocodeGenerator
    {
        public string GenerateCode();
    }
}
