﻿
using Hellang.Middleware.ProblemDetails;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

[assembly: HostingStartup(typeof(Promotion.Infrastructure.Sentry.HostingStartup))]
[assembly: HostingStartup(typeof(Promotion.Infrastructure.MediatR.HostingStartup))]
[assembly: HostingStartup(typeof(Promotion.Infrastructure.Masstransit.HostingStartup))]
[assembly: HostingStartup(typeof(Promotion.Infrastructure.FluentValidation.HostingStartup))]
[assembly: HostingStartup(typeof(Promotion.Infrastructure.Authentication.HostingStartup))]
[assembly: HostingStartup(typeof(Promotion.Infrastructure.Versioning.HostingStartup))]
[assembly: HostingStartup(typeof(Promotion.Infrastructure.EfCore.HostingStartup))]

IHostBuilder builder = Host
    .CreateDefaultBuilder(args)
    .ConfigureWebHostDefaults(webBuilder =>
    {
        webBuilder.ConfigureServices((services) =>
        {
            services.AddControllers();
            services.AddProblemDetails(); 
        });

        webBuilder.Configure((app) =>
        {
            app.UseProblemDetails();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints((endpoints) =>
            {
                endpoints.MapControllers();
            });
        });
    });

await builder.StartAsync();
