﻿
using System;
using System.Threading.Tasks;

using MediatR;

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using Promotion.Application.Promocodes.Commands;

namespace Promotion.Controllers.Promocodes
{
    [ApiVerV1]
    [ApiController]
    [Route("api/v{version:apiVersion}/promocodes")]
    public sealed class PromocodesController : ApiController
    {
        private readonly IMediator _mediator;
        private readonly ILogger<PromocodesController> _logger;

        public PromocodesController(
            IMediator mediator,
            ILogger<PromocodesController> logger)
        {
            if (mediator is null)
            {
                throw new ArgumentNullException(nameof(mediator));
            }

            if (logger is null)
            {
                throw new ArgumentNullException(nameof(logger));
            }

            _mediator = mediator;
            _logger = logger;
        }

        [HttpGet("{id}")]
        public Task<IActionResult> Get(string id)
        {
            throw new NotImplementedException(nameof(Get));
        }

        [HttpGet]
        [JwtAuthorize]
        public async Task<IActionResult> Create([FromBody] NewPromocode pc)
        {
            // TODO: maybe we want to save expiration time instead date only
            CreatePromocodeResponse result = await _mediator.Send(
                new CreatePromocodeRequest(pc.Amount, pc.ValidTo.Date));

            return result
                .Match<IActionResult>(
                    (id) => CreatedAtAction(nameof(Get), new { id }, new { id }),
                    (exception) =>
                    {
                        // TODO: better exception handling
                        return Problem();
                    });
        }

        [HttpGet("me/{id}/status")]
        public Task<IActionResult> GetMyPcStatus(string id)
        {
            throw new NotImplementedException(nameof(GetMyPcStatus)); 
        }
    }

    public sealed class NewPromocode
    {
        public int Amount { get; init; }
        public DateTime ValidTo { get; init; }
    }
}
