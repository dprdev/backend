﻿
using Messages.Domain.Entities;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Messages.Infrastructure.EfCore.EntityTypeConfigurations
{
    internal sealed class MessageConfiguration : IEntityTypeConfiguration<Message>
    {
        public void Configure(
            EntityTypeBuilder<Message> builder)
        {
            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id).HasMaxLength(256);
            builder.Property(e => e.Text).HasMaxLength(750);


            builder.HasIndex(e => e.AuthorId);
        }
    }
}
