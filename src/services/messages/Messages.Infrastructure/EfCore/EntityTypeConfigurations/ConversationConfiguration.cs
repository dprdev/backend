﻿
using Messages.Domain.Entities;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Messages.Infrastructure.EfCore.EntityTypeConfigurations
{
    internal sealed class ConversationConfiguration : IEntityTypeConfiguration<Conversation>
    {
        public void Configure(
            EntityTypeBuilder<Conversation> builder)
        {
            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id).HasMaxLength(256);
            builder.Property(e => e.Participiants)
                .HasColumnType("varchar(256)[2]")
                .IsRequired();

            builder.HasIndex(e => e.Participiants);

            builder.HasMany(e => e.Messages)
                .WithOne()
                .HasForeignKey(e => e.ConversationId);
        }
    }
}
