﻿using System;
using System.Collections.Generic;

namespace Messages.Domain.Entities
{
    public class Conversation
    {
        private readonly List<Message> _messages;

        private Conversation()
        {
        }

        public Conversation(string[] participiants)
        {
            if (participiants is null)
            {
                throw new ArgumentNullException(nameof(participiants));
            }

            if (participiants.Length != 2)
            {
            }

            if (string.IsNullOrEmpty(participiants[0])
                || string.IsNullOrEmpty(participiants[1]))
            {
            }


            Id = Guid
                .NewGuid()
                .ToString();

            CreatedDate = DateTime.UtcNow;
            Participiants = participiants;

            _messages = new()
            {
            };
        }

        public string Id { get; private set; }

        public string[] Participiants { get; private set; }

        public DateTime CreatedDate { get; private set; }

        public IReadOnlyList<Message> Messages => _messages;
    }
}
