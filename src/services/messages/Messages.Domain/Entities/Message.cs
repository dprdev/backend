﻿using System;

namespace Messages.Domain.Entities
{
    public class Message
    {
        private Message()
        {
        }

        public Message(string authorId, string text, string conversationId)
        {
            if (string.IsNullOrEmpty(authorId))
            {
                throw new ArgumentException($"'{nameof(authorId)}' cannot be null or empty.", nameof(authorId));
            }

            if (string.IsNullOrEmpty(text))
            {
                throw new ArgumentException($"'{nameof(text)}' cannot be null or empty.", nameof(text));
            }

            if (string.IsNullOrEmpty(conversationId))
            {
                throw new ArgumentException($"'{nameof(conversationId)}' cannot be null or empty.", nameof(conversationId));
            }

            Text = text;
            ConversationId = conversationId;
            AuthorId = authorId;
        }

        public string Id { get; private set; }
        public string AuthorId { get; private set; }
        public string ConversationId { get; private set; }
        public bool Unread { get; private set; }
        public string Text { get; private set; }
        public DateTime CreatedDate { get; private set; }
    }
}
