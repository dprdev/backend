﻿using System.Diagnostics.CodeAnalysis;

using Messages.Domain.Entities;

using Microsoft.EntityFrameworkCore;

namespace Messages.Domain
{
    public class DataContext : DbContext
    {
        protected DataContext()
        {
        }

        public DataContext([NotNull] DbContextOptions<DataContext> options) : base(options)
        {
        }

        public virtual DbSet<Message> Messages { get; private set; }
        public virtual DbSet<Conversation> Conversations { get; private set; }
    }
}
