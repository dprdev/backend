﻿using System;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Messages.Domain;
using Messages.Domain.Entities;

namespace Messages.Application.Conversations.Commands.Create
{
    internal sealed class CreateConversationRequestHandler
        : IRequestHandler<CreateConversationRequest, CreateConversationResponse>
    {
        private readonly DataContext _context;

        public CreateConversationRequestHandler(DataContext context)
        {
            if (context is null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            _context = context;
        }

        public async Task<CreateConversationResponse> Handle(
            CreateConversationRequest request, CancellationToken cancellationToken)
        {
            Conversation conversation = _context.Conversations.Add(new(request.Participiants)).Entity;
            try
            {
                await _context.SaveChangesAsync(cancellationToken);
            }
            catch (Exception e)
            {
                return new(e);
            }

            return new(new ConversationInfo(conversation.Id, conversation.Participiants));
        }
    }
}
