﻿using System;

using MediatR;

namespace Messages.Application.Conversations.Commands.Create
{
    public sealed class CreateConversationRequest : IRequest<CreateConversationResponse>
    {
        public CreateConversationRequest(string[] participiants)
        {
            if (participiants is null)
            {
                throw new ArgumentNullException(nameof(participiants));
            }

            Participiants = participiants;
        }

        public string[] Participiants { get; private set; }
    }
}
