﻿using System;

using OneOf;

namespace Messages.Application.Conversations.Commands.Create
{
    public sealed class CreateConversationResponse : OneOfBase<ConversationInfo, Exception>
    {
        public CreateConversationResponse(
            OneOf<ConversationInfo, Exception> input) : base(input)
        {
        }
    }
}
