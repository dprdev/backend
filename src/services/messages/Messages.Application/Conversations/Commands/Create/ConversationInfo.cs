﻿using System;

namespace Messages.Application.Conversations.Commands.Create
{
    public sealed class ConversationInfo
    {
        public ConversationInfo(string id, string[] participiants)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentException($"'{nameof(id)}' cannot be null or empty.", nameof(id));
            }

            if (participiants is null)
            {
                throw new ArgumentNullException(nameof(participiants));
            }

            Id = id;
            Participiants = participiants;
        }

        public string Id { get; init; }
        public string[] Participiants { get; init; }
    }
}
