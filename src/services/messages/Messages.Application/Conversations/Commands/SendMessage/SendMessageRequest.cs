﻿
using MediatR;

namespace Messages.Application.Conversations.Commands.SendMessage
{
    public sealed class SendMessageRequest : IRequest<SendMessageResponse>
    {
    }
}
