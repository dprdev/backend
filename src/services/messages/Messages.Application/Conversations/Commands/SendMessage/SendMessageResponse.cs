﻿using System;

using MediatR;

using OneOf;

namespace Messages.Application.Conversations.Commands.SendMessage
{
    public sealed class SendMessageResponse : OneOfBase<Unit, Exception>
    {
        public SendMessageResponse(OneOf<Unit, Exception> input) : base(input)
        {
        }
    }
}
