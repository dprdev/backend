﻿using System;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Messages.Domain;

namespace Messages.Application.Conversations.Commands.SendMessage
{
    internal sealed class SendMessageRequestHandler
        : IRequestHandler<SendMessageRequest, SendMessageResponse>
    {
        private readonly DataContext _context;

        public SendMessageRequestHandler(DataContext context)
        {
            if (context is null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            _context = context;
        }

        public Task<SendMessageResponse> Handle(
            SendMessageRequest request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
