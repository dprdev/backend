﻿using System;

using MediatR;

using OneOf;

namespace Messages.Application.Conversations.Commands.ReadMessage
{
    public sealed class ReadMessageResponse : OneOfBase<Unit, Exception>
    {
        public ReadMessageResponse(OneOf<Unit, Exception> input) : base(input)
        {
        }
    }
}
