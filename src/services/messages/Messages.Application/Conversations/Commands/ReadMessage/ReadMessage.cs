﻿
using MediatR;

namespace Messages.Application.Conversations.Commands.ReadMessage
{
    public sealed class ReadMessageRequest : IRequest<ReadMessageResponse>
    {
    }
}
