﻿using System;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Messages.Domain;

namespace Messages.Application.Conversations.Commands.ReadMessage
{
    internal sealed class ReadMessageRequestHandler : IRequestHandler<ReadMessageRequest, ReadMessageResponse>
    {
        private readonly DataContext _context;

        public ReadMessageRequestHandler(DataContext context)
        {
            if (context is null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            _context = context;
        }

        public Task<ReadMessageResponse> Handle(
            ReadMessageRequest request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
