﻿
using Hellang.Middleware.ProblemDetails;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

[assembly: HostingStartup(typeof(Messages.Infrastructure.EfCore.HostingStartup))]
[assembly: HostingStartup(typeof(Messages.Infrastructure.MediatR.HostingStartup))]
[assembly: HostingStartup(typeof(Messages.Infrastructure.Sentry.HostingStartup))]

IHostBuilder builder = Host
    .CreateDefaultBuilder(args)
    .ConfigureWebHostDefaults(webBuilder =>
    {
        webBuilder.ConfigureServices((host, services) =>
        {
            services.AddAuthorization();
            services.AddAuthentication();
            services.AddControllers()
                .AddDefaultFluentValidation((cfg) => { });

            services.AddDefaultApiVersioning();
            services.AddProblemDetails();
        });

        webBuilder.Configure((host, app) =>
        {
            app.UseProblemDetails();

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        });
    });

await builder.StartAsync();
