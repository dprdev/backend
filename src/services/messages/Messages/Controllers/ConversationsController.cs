﻿using System;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;

using MediatR;

using Messages.Application.Conversations.Commands.Create;

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc;

namespace Messages.Controllers
{
    [JwtAuthorize]
    [ApiController]
    [Route("api/v{version:apiVersion}/conversations")]
    [ApiVersion("1.0")]
    public sealed class ConversationsController : Controller
    {
        [HttpPost("with/{user}")]
        public async Task<IActionResult> Create([FromServices] IMediator mediator, [FromRoute] string user)
        {
            CreateConversationResponse result = await mediator.Send(new CreateConversationRequest(new string[]
            {
                user,
                User.GetId()
            }));

            return result.Match<IActionResult>(
                (conversation) => Json(conversation),
                (exception) => exception switch
                {
                    _ => Problem(statusCode: (int)HttpStatusCode.InternalServerError)
                });
        }

        [HttpPost("{conversation}/message")]
        public Task<IActionResult> SendMessage(
            [FromServices] IMediator mediator)
        {
            throw new NotImplementedException(nameof(SendMessage));
        }

        [HttpPost("{conversation}/message/{message}/read")]
        public Task<IActionResult> ReadMessage(
            [FromServices] IMediator mediator,
            [FromRoute] string message,
            [FromRoute] string conversation)
        {
            throw new NotImplementedException(nameof(ReadMessage));
        }
    }
}
