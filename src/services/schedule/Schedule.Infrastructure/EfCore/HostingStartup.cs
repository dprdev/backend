﻿
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using Schedule.Domain;

namespace Schedule.Infrastructure.EfCore
{
    public sealed class HostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((host, services) =>
            {
                services.AddHostedService<DatabaseMigrationHostedService>();
                services.AddDbContextPool<DataContext>(opt =>
                {
                    ConfigurePgsql(opt, host);
                });
            });
        }

        private static void ConfigurePgsql(DbContextOptionsBuilder options, WebHostBuilderContext host)
        {
            options.ReplaceService<IModelCustomizer, CustomModelCustomizer>();
            options.UseNpgsql(
                host.Configuration.GetValue<string>("dbConnection"),
                npgsqlOptions =>
                {
                    npgsqlOptions.MigrationsAssembly("Schedule.Infrastructure");
                });
        }
    }
}
