﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using Schedule.Domain.Entities;

namespace Schedule.Infrastructure.EfCore.EntityTypeConfigurations
{
    internal sealed class TimeScheduleConfiguration : IEntityTypeConfiguration<TimeSchedule>
    {
        public void Configure(
            EntityTypeBuilder<TimeSchedule> builder)
        {
            builder.HasKey(e => e.UserId);
            builder.Property(e => e.Schedules)
                .HasColumnType("jsonb");

            builder.Property(e => e.UserId).HasMaxLength(256);
            builder.Property(e => e.Timezone).HasMaxLength(256);
        }
    }
}
