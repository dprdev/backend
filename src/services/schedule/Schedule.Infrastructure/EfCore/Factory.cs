﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Infrastructure;

using Schedule.Domain;

namespace Schedule.Infrastructure.EfCore
{
    internal sealed class Factory : IDesignTimeDbContextFactory<DataContext>
    {
        public DataContext CreateDbContext(string[] args)
        {
            DbContextOptions<DataContext> o =
                new DbContextOptionsBuilder<DataContext>()
                    .UseNpgsql(cfg =>
                    {
                        cfg.MigrationsAssembly("Schedule.Infrastructure");
                    })
                    .ReplaceService<IModelCustomizer, CustomModelCustomizer>()
                    .Options;

            return new DataContext(o);
        }
    }
}
