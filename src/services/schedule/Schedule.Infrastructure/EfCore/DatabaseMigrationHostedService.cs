﻿
using System;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using Polly;
using Polly.Retry;

using Schedule.Domain;

namespace Schedule.Infrastructure.EfCore
{
    internal sealed class DatabaseMigrationHostedService : IHostedService
    {
        private readonly IServiceProvider _provider;
        private readonly IWebHostEnvironment _environment;

        public DatabaseMigrationHostedService(IServiceProvider provider, IWebHostEnvironment environment)
        {
            if (provider is null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            if (environment is null)
            {
                throw new ArgumentNullException(nameof(environment));
            }

            _provider = provider;
            _environment = environment;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            using IServiceScope scope = _provider.CreateScope();

            DataContext db = scope.ServiceProvider.GetService<DataContext>();
            RetryPolicy policy = Policy
                .Handle<Exception>()
                .WaitAndRetry(5, (count) =>
                {
                    return TimeSpan.FromSeconds(count + count * 0.5);
                });

            policy.Execute(() =>
            {
                db.Database.Migrate();
            });

            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}
