﻿using System;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;

using MediatR;

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc;

using Schedule.Application.Schedule.Commands.EditSchedule;

namespace Schedule.Controllers.Schedule
{
    [ApiController]
    [Route("api/v{version:apiVersion}/schedule")]
    public sealed class ScheduleController : Controller
    {
        [HttpGet("{schedule}")]
        public Task<IActionResult> GetSchedule(
            [FromServices] IMediator mediator,
            [FromRoute] string schedule)
        {
            throw new NotImplementedException(nameof(GetSchedule));
        }

        [HttpPut("me")]
        [HttpPost("me")] 
        public async Task<IActionResult> EditSchedule(
            [FromServices] IMediator mediator, [FromBody] TimeScheduleEditModel schedule)
        {
            EditScheduleResponse result = await mediator.Send(new EditScheduleRequest(User.GetId(), schedule));

            return result.Match<IActionResult>(
                (_) => NoContent(),
                (exception) => exception switch
                {
                    _ => Problem(statusCode: (int)HttpStatusCode.InternalServerError)
                });
        }
    }
}
