﻿using System.Reflection;

using Hellang.Middleware.ProblemDetails;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

[assembly: HostingStartup(typeof(Schedule.Infrastructure.EfCore.HostingStartup))]
[assembly: HostingStartup(typeof(Schedule.Infrastructure.MediatR.HostingStartup))]

IHostBuilder builder = Host
    .CreateDefaultBuilder(args)
    .ConfigureWebHostDefaults(webBuilder =>
    {
        webBuilder.ConfigureServices((host, services) =>
        {
            services.AddAuthorization();
            services.AddAuthentication();
            services.AddProblemDetails();
            services.AddDefaultApiVersioning();
            services.AddControllers()
                .AddDefaultFluentValidation(cfg =>
                {
                    cfg.RegisterValidatorsFromAssembly(Assembly.Load("Schedule"));
                    cfg.RegisterValidatorsFromAssembly(Assembly.Load("Schedule.Application"));
                    cfg.RegisterValidatorsFromAssembly(Assembly.Load("Schedule.Domain"));
                });
        });


        webBuilder.Configure((host, app) =>
        {
            app.UseProblemDetails();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        });
    });

await builder.StartAsync();
