﻿using System;

using MediatR;

using OneOf;

namespace Schedule.Application.Schedule.Commands.EditSchedule
{
    public sealed class EditScheduleResponse : OneOfBase<Unit, Exception>
    {
        public EditScheduleResponse(OneOf<Unit, Exception> input) : base(input)
        {
        }
    }
}
