﻿using System;

namespace Schedule.Application.Schedule.Commands.EditSchedule
{
    public sealed class TimeSlotEditModel
    {
        public TimeSpan To { get; init; }
        public TimeSpan From { get; init; }
    }
}
