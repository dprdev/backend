﻿using System;

namespace Schedule.Application.Schedule.Commands.EditSchedule
{
    public sealed class DaySettingsEditModel
    {
        public DateTime[] Dates { get; init; }
        public TimeSlotEditModel[] TimeSlots { get; init; }
    }
}
