﻿namespace Schedule.Application.Schedule.Commands.EditSchedule
{
    public sealed class TimeScheduleEditModel
    {
        public string Timezone { get; init; }
        public DayScheduleEditModel[] Schedules { get; init; }
    }
}
