﻿using System;

namespace Schedule.Application.Schedule.Commands.EditSchedule
{
    public sealed class DayScheduleEditModel
    {
        public DayOfWeek Day { get; init; }
        public TimeSlotEditModel[] TimeSlots { get; init; }
        public DaySettingsEditModel DaySettings { get; init; }
    }
}
