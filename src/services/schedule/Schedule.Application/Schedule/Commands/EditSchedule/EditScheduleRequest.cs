﻿using System;

using MediatR;

namespace Schedule.Application.Schedule.Commands.EditSchedule
{
    public sealed class EditScheduleRequest : IRequest<EditScheduleResponse>
    {
        public EditScheduleRequest(string userId, TimeScheduleEditModel timeSchedule)
        {
            if (string.IsNullOrEmpty(userId))
            {
                throw new ArgumentException($"'{nameof(userId)}' cannot be null or empty.", nameof(userId));
            }

            if (timeSchedule is null)
            {
                throw new ArgumentNullException(nameof(timeSchedule));
            }

            UserId = userId;
            TimeSchedule = timeSchedule;
        }

        public string UserId { get; private set; }
        public TimeScheduleEditModel TimeSchedule { get; private set; }
    }
}
