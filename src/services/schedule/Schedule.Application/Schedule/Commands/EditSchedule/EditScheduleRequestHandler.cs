﻿using System;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Schedule.Domain;
using Schedule.Domain.Entities;

namespace Schedule.Application.Schedule.Commands.EditSchedule
{
    internal sealed class EditScheduleRequestHandler
        : IRequestHandler<EditScheduleRequest, EditScheduleResponse>
    {
        private readonly DataContext _context;

        public EditScheduleRequestHandler(DataContext context)
        {
            if (context is null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            _context = context;
        }

        public async Task<EditScheduleResponse> Handle(
            EditScheduleRequest request, CancellationToken cancellationToken)
        {
            TimeSchedule schedule = await GetSchedule(request.UserId, cancellationToken);
            if (schedule is null)
            {
            }
            else
            {
            }

            try
            {
                await _context.SaveChangesAsync(cancellationToken);
            }
            catch (Exception ex)
            {
                if (ex is DbUpdateConcurrencyException concurrencyException)
                {
                    // TODO: handle concurrency exception
                }

                return new(ex);
            }

            return new(new Unit());
        }

        private Task<TimeSchedule> GetSchedule(string id, CancellationToken cancellationToken)
        {
            return _context.Schedules.FirstOrDefaultAsync((e) => e.UserId == id, cancellationToken: cancellationToken);
        }
    }
}
