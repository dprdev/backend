﻿
using Microsoft.EntityFrameworkCore;

using Schedule.Domain.Entities;

namespace Schedule.Domain
{
    public class DataContext : DbContext
    {
        public DataContext()
        {
        }

        public DataContext(DbContextOptions options) : base(options)
        {
        }

        public virtual DbSet<TimeSchedule> Schedules { get; private set; }
    }
}
