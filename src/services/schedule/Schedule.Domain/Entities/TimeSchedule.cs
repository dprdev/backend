﻿using System;
using System.Collections.Generic;

namespace Schedule.Domain.Entities
{
    public class TimeSchedule
    {
        private readonly List<DaySchedule> _schedules;

        protected TimeSchedule()
        {
        }

        public TimeSchedule(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentException($"'{nameof(id)}' cannot be null or empty.", nameof(id));
            }

            UserId = id;
            _schedules = new()
            {
            };
        }


        public string UserId { get; private set; }
        public string Timezone { get; private set; }
        public IReadOnlyList<DaySchedule> Schedules => _schedules;
    }
}
