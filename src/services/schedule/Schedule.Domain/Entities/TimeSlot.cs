﻿using System;
using System.Diagnostics;

namespace Schedule.Domain.Entities
{
    [DebuggerDisplay("{From}-{To}")]
    public class TimeSlot
    {
        private TimeSlot()
        {
        }


        public TimeSlot(
            TimeSpan to,
            TimeSpan from)
        {
            To = to;
            From = from;
        }

        public TimeSpan To { get; private set; }
        public TimeSpan From { get; private set; }
    }
}
