﻿using System;
using System.Collections.Generic;

namespace Schedule.Domain.Entities
{
    public class DaySettings
    {
        private readonly List<TimeSlot> _slots;
        private readonly HashSet<DateTime> _dates;

        private DaySettings()
        {
        }

        public DaySettings(
            IEnumerable<DateTime> dates,
            IEnumerable<TimeSlot> slots)
        {
            if (dates is null)
            {
                throw new ArgumentNullException(nameof(dates));
            }

            if (slots is null)
            {
                throw new ArgumentNullException(nameof(slots));
            }

            _dates = new(dates);
            _slots = new(slots);
        }

        public IReadOnlySet<DateTime> Dates => _dates;
        public IReadOnlyList<TimeSlot> TimeSlots => _slots;
    }
}
