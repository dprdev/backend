﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Schedule.Domain.Entities
{
    [DebuggerDisplay("{Day}")]
    public class DaySchedule
    {
        private readonly List<TimeSlot> _timeSlots;
        private readonly DaySettings _daySettings;

        private DaySchedule()
        {
        }

        public DaySchedule(
            DayOfWeek day,
            IEnumerable<TimeSlot> timeSlots,
            DaySettings daySettings)
        {
            Day = day;

            _daySettings = daySettings;
            _timeSlots = new(timeSlots)
            {
            };
        }

        public DayOfWeek Day { get; private set; }
        public DaySettings DaySettings => _daySettings;
        public IReadOnlyList<TimeSlot> TimeSlots => _timeSlots;
    }
}
