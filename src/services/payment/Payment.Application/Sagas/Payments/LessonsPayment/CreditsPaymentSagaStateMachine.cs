﻿
using System;

using Automatonymous;

namespace Payment.Application.Sagas.Payments.LessonsPayment
{
    public class LessonsPaymentSagaState : SagaStateMachineInstance
    {
        public Guid CorrelationId { get; set; }
    }

    public class LessonsPaymentSagaStateMachine
        : MassTransitStateMachine<LessonsPaymentSagaState>
    {
        public LessonsPaymentSagaStateMachine()
        {
            SetCompletedWhenFinalized();
        }
    }
}
