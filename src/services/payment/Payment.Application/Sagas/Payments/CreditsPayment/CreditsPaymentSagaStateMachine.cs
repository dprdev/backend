﻿
using System;

using Automatonymous;

namespace Payment.Application.Sagas.Payments.CreditsPayment
{
    public class CreditsPaymentSagaState : SagaStateMachineInstance
    {
        public Guid CorrelationId { get; set; }
    }

    public class CreditsPaymentSagaStateMachine
        : MassTransitStateMachine<CreditsPaymentSagaState>
    {
        public CreditsPaymentSagaStateMachine()
        { 
            SetCompletedWhenFinalized(); 
        }        
    }
}
