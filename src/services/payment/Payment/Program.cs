﻿
using Hellang.Middleware.ProblemDetails;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

[assembly: HostingStartup(typeof(Payment.Infrastructure.Sentry.HostingStartup))]
[assembly: HostingStartup(typeof(Payment.Infrastructure.Authentication.HostingStartup))]
[assembly: HostingStartup(typeof(Payment.Infrastructure.Masstransit.HostingStartup))] 

IHostBuilder builder = Host
    .CreateDefaultBuilder(args)
    .ConfigureWebHostDefaults(webBuilder =>
    {
        webBuilder.ConfigureServices((host, services) =>
        {
            services.AddProblemDetails(); 
        });

        webBuilder.Configure((host, app) =>
        {
            app.UseProblemDetails(); 
            app.UseAuthentication();
            app.UseAuthorization(); 
        });
    });

await builder.StartAsync();
