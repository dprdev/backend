﻿
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace Payment.Infrastructure.Sentry
{
    /// <summary>
    /// Configure sentry
    /// </summary>
    public sealed class HostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            if (builder.CanUseSentry())
            {
                builder.UseSentry((host, o) =>
                {
                    o.Dsn = host.Configuration.GetValue<string>("sentry"); 
                });
            } 
        }
    }
}
