﻿
using Microsoft.AspNetCore.Hosting;

namespace Payment.Infrastructure.Authentication
{
    public sealed class HostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((host, services) =>
            { 
            });
        } 
    }
}
