﻿using System.Reflection;

using MediatR;

using Microsoft.AspNetCore.Hosting;

namespace Payment.Infrastructure.MediatR
{
    public sealed class HostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((services) => services.AddMediatR(GetAssemblies()));
        }

        private static Assembly[] GetAssemblies()
        {
            return new Assembly[]
            {
                Assembly.Load("Payment"),
                Assembly.Load("Payment.Application"),
                Assembly.Load("Payment.Domain"),
                Assembly.Load("Payment.Infrastructure"),
            };
        }
    }
}
