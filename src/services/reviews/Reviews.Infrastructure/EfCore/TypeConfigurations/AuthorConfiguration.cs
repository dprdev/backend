﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using Reviews.Domain.Entities;

namespace Reviews.Infrastructure.EfCore.TypeConfigurations
{
    internal sealed class AuthorConfiguration : IEntityTypeConfiguration<Author>
    {
        public void Configure(EntityTypeBuilder<Author> builder)
        {
            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id).HasMaxLength(256);
            builder.Property(e => e.PhotoUrl).HasMaxLength(256);
            builder.Property(e => e.Name).HasMaxLength(128);
        }
    }
}
