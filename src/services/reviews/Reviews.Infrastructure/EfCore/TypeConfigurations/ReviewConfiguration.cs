﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using Reviews.Domain.Entities;

namespace Reviews.Infrastructure.EfCore.TypeConfigurations
{
    internal sealed class ReviewConfiguration : IEntityTypeConfiguration<Review>
    {
        public void Configure(EntityTypeBuilder<Review> builder)
        {
            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id).HasMaxLength(256);
            builder.Property(e => e.Text).HasMaxLength(500);

            builder.HasOne(e => e.Author)
                .WithMany()
                .HasForeignKey(e => e.AuthorId);
        }
    }
}
