﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

using Reviews.Domain;

namespace Reviews.Infrastructure.EfCore
{
    internal sealed class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<DataContext>
    {
        DataContext IDesignTimeDbContextFactory<DataContext>.CreateDbContext(string[] args)
        {
            DbContextOptionsBuilder<DataContext> optBuilder
                = new DbContextOptionsBuilder<DataContext>()
                    .UseNpgsql(cfg =>
                    {
                        cfg.MigrationsAssembly("Reviews.Infrastructure");
                    });

            return new DataContext(optBuilder.Options);
        }
    }
}
