﻿
using System;

using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.DependencyInjection;

using Polly;
using Polly.Retry;

using Reviews.Domain;

using Sentry;

namespace Reviews.Infrastructure.EfCore
{
    public sealed class HostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((host, services) =>
            {
                services.AddDbContextPool<DataContext>((opt) => ConfigureContext(opt, host));
            });

            builder.Configure((host, builder) =>
            {
                using IServiceScope scope = builder.ApplicationServices.CreateScope();
                DataContext db = scope.ServiceProvider.GetService<DataContext>();
                RetryPolicy policy = Policy
                    .Handle<Exception>()
                    .WaitAndRetry(5, (count) =>
                    {
                        return TimeSpan.FromSeconds(count + count * 0.5);
                    });

                try
                {
                    policy.Execute(() => db.Database.Migrate());
                }
                catch (Exception e)
                {
                    SentrySdk.CaptureException(e);
                    throw;
                }
            });
        }

        private static void ConfigureContext(DbContextOptionsBuilder options, WebHostBuilderContext host)
        {
            options.ReplaceService<IModelCustomizer, CustomModelCustomizer>();
            options.UseNpgsql(
                host.GetParameter<string>("db"),
                opt =>
                {
                    opt.MigrationsAssembly("Reviews.Infrastructure");
                });
        }
    }
}
