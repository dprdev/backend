﻿
using MediatR;

namespace Reviews.Application.Users.Queries.GetUserReviews
{
    public sealed class GetUserReviewsRequest : IRequest<GetUserReviewsResponse>
    {
    }
}
