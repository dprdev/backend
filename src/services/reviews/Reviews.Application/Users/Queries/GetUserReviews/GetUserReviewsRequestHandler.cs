﻿using System;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

using Reviews.Domain;

namespace Reviews.Application.Users.Queries.GetUserReviews
{
    internal sealed class GetUserReviewsRequestHandler
        : IRequestHandler<GetUserReviewsRequest, GetUserReviewsResponse>
    {
        private readonly DataContext _context;

        public GetUserReviewsRequestHandler(DataContext context)
        {
            if (context is null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            _context = context;
        }

        public Task<GetUserReviewsResponse> Handle(
            GetUserReviewsRequest request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
