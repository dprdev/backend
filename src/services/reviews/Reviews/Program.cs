﻿
using System.Reflection;

using Hellang.Middleware.ProblemDetails;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

[assembly: HostingStartup(typeof(Reviews.Infrastructure.EfCore.HostingStartup))]

IHostBuilder builder = Host
    .CreateDefaultBuilder(args)
    .ConfigureWebHostDefaults(webBuilder =>
    {
        webBuilder.ConfigureServices((s) =>
        {
            s.AddProblemDetails();
            s.AddDefaultApiVersioning();
            s.AddControllers()
                .AddDefaultFluentValidation(cfg =>
                {
                    cfg.RegisterValidatorsFromAssembly(Assembly.Load("Reviews"));
                    cfg.RegisterValidatorsFromAssembly(Assembly.Load("Reviews.Application"));
                    cfg.RegisterValidatorsFromAssembly(Assembly.Load("Reviews.Domain"));
                });
        });

        webBuilder.Configure((host, app) =>
        {
            app.UseProblemDetails();


            app.UseRouting();
            app.UseEndpoints((endpoints) =>
            {
                endpoints.MapControllers();
            });
        });
    });

await builder.StartAsync();
