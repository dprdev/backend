﻿
using Microsoft.AspNetCore.Mvc;

namespace Reviews.Controllers.Teachers
{
    [ApiController]
    [Route("api/v{version:apiVersion}/users")]
    [ApiVersion("1.0")]
    public sealed class UsersController : Controller
    {
    }
}
