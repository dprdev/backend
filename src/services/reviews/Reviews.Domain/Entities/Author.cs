﻿namespace Reviews.Domain.Entities
{
    public class Author
    {
        private Author()
        {
        }

        public Author(string id, string name, string photoUrl)
        {
        }


        public string Id { get; private set; }
        public string Name { get; private set; }
        public string PhotoUrl { get; private set; }
    }
}
