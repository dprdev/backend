﻿using System;

namespace Reviews.Domain.Entities
{
    public class Review
    {
        private Review()
        {
        }

        public Review(string text, sbyte rating, string authorId)
        {
            if (string.IsNullOrEmpty(text))
            {
                throw new ArgumentException($"'{nameof(text)}' cannot be null or empty.", nameof(text));
            }

            if (string.IsNullOrEmpty(authorId))
            {
                throw new ArgumentException($"'{nameof(authorId)}' cannot be null or empty.", nameof(authorId));
            }
            Text = text;
            Rating = rating;
            AuthorId = authorId;
        }

        public string Id { get; private set; }
        public string Text { get; private set; }
        public DateTime CreatedDate { get; private set; }
        public sbyte Rating { get; private set; }


        public Author Author { get; private set; }
        public string AuthorId { get; private set; }
    }
}
