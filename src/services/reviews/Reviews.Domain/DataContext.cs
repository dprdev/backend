﻿using System.Diagnostics.CodeAnalysis;

using Microsoft.EntityFrameworkCore;

using Reviews.Domain.Entities;

namespace Reviews.Domain
{
    public class DataContext : DbContext
    {
        protected DataContext()
        {
        }

        public DataContext([NotNull] DbContextOptions options) : base(options)
        {
        }

        public virtual DbSet<Review> Reviews { get; private set; }
    }
}
