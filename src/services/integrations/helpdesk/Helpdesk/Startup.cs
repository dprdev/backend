﻿using System;
using System.Reflection;

using Hellang.Middleware.ProblemDetails;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Helpdesk
{
    public class Startup
    {
        public Startup(
            IConfiguration cfg,
            IWebHostEnvironment env)
        {
            Cfg = cfg ?? throw new ArgumentNullException(nameof(cfg));
            Env = env ?? throw new ArgumentNullException(nameof(env));
        }

        public IConfiguration Cfg { get; }
        public IWebHostEnvironment Env { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthorization();
            services.AddJwtAuthentication();
            services.AddDefaultApiVersioning();
            services.AddProblemDetails();
            services.AddControllers()
                .AddDefaultFluentValidation(cfg =>
                {
                    cfg.RegisterValidatorsFromAssembly(Assembly.GetExecutingAssembly());
                });
        }

        public void Configure(IApplicationBuilder app)
        {
            app.UseProblemDetails();

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }

}
