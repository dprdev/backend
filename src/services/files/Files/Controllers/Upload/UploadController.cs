﻿using System;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;

using Domain.Common.Exceptions;

using Files.Application.Upload.Commands.CreateSession;
using Files.Application.Upload.Queries.GetSession;

using MediatR;

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Files.Controllers.Upload
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/upload")]
    public sealed class UploadController : Controller
    {
        [HttpPost]
        public Task<IActionResult> Upload(
            [FromServices] IMediator mediator,
            [FromBody] IFormFile file)
        {
            throw new NotImplementedException(nameof(Upload));
        }


        [ConsumesJson]
        [HttpPost("session")]
        public async Task<IActionResult> CreateSession(
            [FromServices] IMediator mediator, [FromBody] CreateSessionCommand command)
        {
            CreateSessionResponse response =
                await mediator.Send(command.ToRequest(HttpContext));

            return response.Match<IActionResult>(
                (session) => CreatedAtAction(nameof(GetSession), new { session.Id }, session),
                (exception) => exception switch
                {
                    _ => Problem(statusCode: (int)HttpStatusCode.InternalServerError)
                });
        }

        [JwtAuthorize]
        [HttpGet("session/{id}")]
        public async Task<IActionResult> GetSession(
            [FromServices] IMediator mediator, [FromRoute] string id)
        {
            GetSessionResponse response =
                await mediator.Send(new GetSessionRequest(User.GetId(), id));

            return response.Match<IActionResult>(
                (session) => Json(session),
                (exception) => exception switch
                {
                    AccessDeniedException denied =>
                        Problem(statusCode: (int)HttpStatusCode.Forbidden),

                    EntityNotFoundException found =>
                        Problem(statusCode: (int)HttpStatusCode.NotFound),

                    _ => Problem(statusCode: (int)HttpStatusCode.InternalServerError)
                });
        }

        [HttpPost("session/{id}/chunk")]
        public Task<IActionResult> UploadChunk([FromServices] IMediator mediator)
        {
            throw new NotImplementedException(nameof(UploadChunk));
        }

        public sealed class CreateSessionCommand
        {
            public int TotalSize { get; init; }
            public int ChunksCount { get; init; }
            public int ChunkSize { get; init; }
            public string Extension { get; init; }

            public CreateSessionRequest ToRequest(HttpContext context)
            {
                return new(context.User.GetId(), Extension, TotalSize, ChunksCount, ChunkSize);
            }
        }
    }
}
