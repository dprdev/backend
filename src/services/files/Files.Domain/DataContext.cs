﻿
using System.Diagnostics.CodeAnalysis;

using Files.Domain.Entities;

using Microsoft.EntityFrameworkCore;

namespace Files.Domain
{
    public class DataContext : DbContext
    {
        protected DataContext()
        {
        }

        public DataContext([NotNull] DbContextOptions options) : base(options)
        {
        }


        public virtual DbSet<File> Files { get; private set; }
        public virtual DbSet<Session> Sessions { get; private set; }
        public virtual DbSet<FileChunk> FileChunks { get; private set; }
    }
}
