﻿using System;
using System.Collections.Generic;

namespace Files.Domain.Entities
{
    public class Session
    {
        private readonly List<FileChunk> _chunks;

        private Session()
        {
        }

        public Session(
            string ownerId,
            string extension,
            int totalSize,
            int chunkSize,
            int chunksCount)
        {
            if (string.IsNullOrEmpty(ownerId))
            {
                throw new ArgumentException($"'{nameof(ownerId)}' cannot be null or empty.", nameof(ownerId));
            }

            if (string.IsNullOrEmpty(extension))
            {
                throw new ArgumentException($"'{nameof(extension)}' cannot be null or empty.", nameof(extension));
            }

            OwnerId = ownerId;
            Extension = extension;
            TotalSize = totalSize;
            ChunkSize = chunkSize;
            ChunksCount = chunksCount;
            CreatedDate = DateTime.UtcNow;

            _chunks = new()
            {
            };
        }


        public string Id { get; private set; }
        public string OwnerId { get; private set; }
        public string Extension { get; private set; }

        public int TotalSize { get; private set; }
        public int ChunkSize { get; private set; }
        public int ChunksCount { get; private set; }
        public int UploadedChunksCount { get; private set; }

        public DateTime CreatedDate { get; private set; }

        public IReadOnlyList<FileChunk> Chunks => _chunks;
    }
}
