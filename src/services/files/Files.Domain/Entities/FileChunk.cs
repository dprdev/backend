﻿using System;

namespace Files.Domain.Entities
{
    public class FileChunk
    {
        private FileChunk()
        {
        }

        public FileChunk(byte[] bytes, int number)
        {
            Id = Guid
                .NewGuid()
                .ToString();

            Bytes = bytes;
            ChunkNumber = number;

            CreatedDate = DateTime.UtcNow;
        }

        public string Id { get; private set; }
        public byte[] Bytes { get; private set; }
        public int ChunkNumber { get; private set; }
        public DateTime CreatedDate { get; private set; }

        public Session Sessoin { get; private set; }
        public string SessoinId { get; set; }
    }
}
