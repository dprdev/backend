﻿using System;

namespace Files.Domain.Entities
{
    public class File
    {
        private File()
        {
        }


        public File(string ownerId, string url, string ext, string name = null)
        {
            if (string.IsNullOrEmpty(ownerId))
            {
                throw new ArgumentException($"'{nameof(ownerId)}' cannot be null or empty.", nameof(ownerId));
            }

            if (string.IsNullOrEmpty(url))
            {
                throw new ArgumentException($"'{nameof(url)}' cannot be null or empty.", nameof(url));
            }

            if (string.IsNullOrEmpty(ext))
            {
                throw new ArgumentException($"'{nameof(ext)}' cannot be null or empty.", nameof(ext));
            }

            Id = Guid
                .NewGuid()
                .ToString();

            OwnerId = ownerId;
            Url = url;
            Ext = ext;
            Name = name;

            CreatedDate = DateTime.UtcNow;
        }
        public string Id { get; private set; }
        public string OwnerId { get; private set; }
        public string Url { get; private set; }
        public string Ext { get; private set; }
        public string Name { get; private set; }
        public DateTime CreatedDate { get; private set; }
    }
}
