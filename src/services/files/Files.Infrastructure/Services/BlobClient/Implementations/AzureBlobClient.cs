﻿using System;

using Azure.Storage.Blobs;

using Files.Application.Services;

using Microsoft.Extensions.Configuration;

namespace Files.Infrastructure.Services.BlobClient.Implementations
{
    internal sealed class AzureBlobClient : IBlobClient
    {
        private readonly BlobServiceClient _client;

        public AzureBlobClient(IConfiguration configuration)
        {
            if (configuration is null)
            {
                throw new ArgumentNullException(nameof(configuration));
            }

            _client = new BlobServiceClient(configuration.GetValue<string>("storageConnection"));
        }
    }
}
