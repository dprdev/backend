﻿using System;

using Amazon.S3;

using Files.Application.Services;

using Microsoft.Extensions.Configuration;

namespace Files.Infrastructure.Services.BlobClient.Implementations
{
    internal sealed class S3BlobClient : IBlobClient
    {
        private readonly AmazonS3Client _client;
        public S3BlobClient(IConfiguration configuration)
        {
            if (configuration is null)
            {
                throw new ArgumentNullException(nameof(configuration));
            }

            _client = new(new AmazonS3Config
            {
                ServiceURL = configuration.GetValue<string>("storageConnection")
            });
        }
    }
}
