﻿
using System;
using System.Collections.Generic;

using Files.Application.Services;

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Files.Infrastructure.Services.BlobClient
{
    using Implementations;

    internal static class BlobClientWebHostExtensions
    {
        private static readonly Dictionary<BlobClientProvider, Type> _providers = new()
        {
            { BlobClientProvider.Azurite, typeof(AzureBlobClient) }
        };

        public static IWebHostBuilder UseBlobClient(this IWebHostBuilder builder)
        {
            return builder.ConfigureServices((host, services) =>
            {
                if (Enum.TryParse(
                    host.Configuration.GetValue<string>("storage"),
                    ignoreCase: true,
                    out BlobClientProvider provider))
                {
                    if (_providers.TryGetValue(provider, out Type implementation))
                    {
                        services.AddSingleton(typeof(IBlobClient), implementation);
                    }
                    else
                    {
                        throw new InvalidOperationException($"The {provider} IBlobClient not implemented.");
                    }
                }
                else
                {
                    throw new InvalidOperationException("Storage provider does not specified.");
                }
            });
        }
    }
}
