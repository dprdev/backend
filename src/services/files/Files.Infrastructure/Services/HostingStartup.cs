﻿using Files.Infrastructure.Services.BlobClient;

using Microsoft.AspNetCore.Hosting;

namespace Files.Infrastructure.Services
{
    public sealed class HostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.UseBlobClient();
        }
    }
}
