﻿
using Files.Domain;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Files.Infrastructure.EfCore
{
    internal sealed class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<DataContext>
    {
        DataContext IDesignTimeDbContextFactory<DataContext>.CreateDbContext(string[] args)
        {
            DbContextOptionsBuilder<DataContext> optBuilder
                = new DbContextOptionsBuilder<DataContext>()
                    .UseNpgsql(cfg =>
                    {
                        cfg.MigrationsAssembly("Files.Infrastructure");
                    });

            return new DataContext(optBuilder.Options);
        }
    }
}
