﻿
using Files.Domain.Entities;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Files.Infrastructure.EfCore.EntityTypeConfigurations
{
    internal sealed class FileChunkConfiguration : IEntityTypeConfiguration<FileChunk>
    {
        public void Configure(EntityTypeBuilder<FileChunk> builder)
        {
            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id).HasMaxLength(256);
            builder.Property(e => e.SessoinId).HasMaxLength(256);
            builder.Property(e => e.Bytes);
        }
    }
}
