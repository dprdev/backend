﻿
using Files.Domain.Entities;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Files.Infrastructure.EfCore.EntityTypeConfigurations
{
    internal sealed class SessionConfiguration : IEntityTypeConfiguration<Session>
    {
        public void Configure(EntityTypeBuilder<Session> builder)
        {
            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id).HasMaxLength(256);
            builder.Property(e => e.OwnerId).HasMaxLength(256);

            builder.Property(e => e.UploadedChunksCount)
                .IsConcurrencyToken();

            builder.HasMany(e => e.Chunks)
                .WithOne(e => e.Sessoin)
                .HasForeignKey(e => e.SessoinId);
        }
    }
}
