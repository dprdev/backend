﻿
using Files.Domain.Entities;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Files.Infrastructure.EfCore.EntityTypeConfigurations
{
    internal sealed class FileConfiguration : IEntityTypeConfiguration<File>
    {
        public void Configure(EntityTypeBuilder<File> builder)
        {
            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id).HasMaxLength(256);
            builder.Property(e => e.Name).HasMaxLength(256);
            builder.Property(e => e.OwnerId).HasMaxLength(256);
            builder.Property(e => e.Url).HasMaxLength(256);
            builder.Property(e => e.Ext).HasMaxLength(32);
        }
    }
}
