﻿
using Files.Domain;

using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.DependencyInjection;

namespace Files.Infrastructure.EfCore
{

    public sealed class HostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((host, services) =>
            {
                services.AddHostedService<DatabaseMigrationHostedService>();
                services.AddDbContextPool<DataContext>((opt) =>
                {
                    ConfigureContext(opt, host);
                });
            });
        }

        private static void ConfigureContext(DbContextOptionsBuilder options, WebHostBuilderContext host)
        {
            options.ReplaceService<IModelCustomizer, CustomModelCustomizer>();
            options.UseNpgsql(
                host.GetParameter<string>("db"),
                opt =>
                {
                    opt.MigrationsAssembly("Files.Infrastructure");
                });
        }
    }
}
