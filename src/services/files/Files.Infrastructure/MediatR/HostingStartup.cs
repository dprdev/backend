﻿using System.Reflection;

using MediatR;

using Microsoft.AspNetCore.Hosting;

namespace Files.Infrastructure.MediatR
{
    public sealed class HostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((services) => services.AddMediatR(GetAssemblies()));
        }

        private static Assembly[] GetAssemblies()
        {
            return new Assembly[]
            {
                Assembly.Load("Files.Domain"),
                Assembly.Load("Files.Application"),
            };
        }
    }
}
