﻿using System;

using MediatR;

namespace Files.Application.Upload.Queries.GetSession
{
    public sealed class GetSessionRequest : IRequest<GetSessionResponse>
    {
        public GetSessionRequest(string userId, string sessionId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                throw new ArgumentException($"'{nameof(userId)}' cannot be null or empty.", nameof(userId));
            }

            if (string.IsNullOrEmpty(sessionId))
            {
                throw new ArgumentException($"'{nameof(sessionId)}' cannot be null or empty.", nameof(sessionId));
            }

            UserId = userId;
            SessionId = sessionId;
        }

        public string UserId { get; private set; }
        public string SessionId { get; private set; }
    }
}
