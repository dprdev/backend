﻿using System;

using OneOf;

namespace Files.Application.Upload.Queries.GetSession
{
    public sealed class GetSessionResponse : OneOfBase<UploadSession, Exception>
    {
        public GetSessionResponse(OneOf<UploadSession, Exception> input) : base(input)
        {
        }
    }
}
