﻿using System;

namespace Files.Application.Upload.Queries.GetSession
{
    public sealed class UploadSession
    {
        public string Id { get; init; }
        public string OwnerId { get; init; }
        public string Extension { get; init; }
        public int TotalSize { get; init; }
        public int ChunkSize { get; init; }
        public int ChunksCount { get; init; }
        public int UploadedChunksCount { get; init; }
        public DateTime CreatedDate { get; init; }
    }
}
