﻿using System;
using System.Threading;
using System.Threading.Tasks;

using Domain.Common.Exceptions;

using Files.Domain;
using Files.Domain.Entities;

using MediatR;

using Microsoft.EntityFrameworkCore;

namespace Files.Application.Upload.Queries.GetSession
{
    internal sealed class GetSessionRequestHandler
        : IRequestHandler<GetSessionRequest, GetSessionResponse>
    {
        private readonly DataContext _context;

        public GetSessionRequestHandler(DataContext context)
        {
            if (context is null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            _context = context;
        }

        public async Task<GetSessionResponse> Handle(
            GetSessionRequest request, CancellationToken cancellationToken)
        {
            Session session = await GetSession(request, cancellationToken);
            if (session is null)
            {
                return new(new EntityNotFoundException($"Session ({request.SessionId}) does not exists"));
            }

            if (session.OwnerId != request.UserId)
            {
                return new(new AccessDeniedException("User does not have access to the current resource"));
            }

            var dto = new UploadSession
            {
                Id = session.Id,
                ChunksCount = session.ChunksCount,
                ChunkSize = session.ChunkSize,
                TotalSize = session.TotalSize,
                CreatedDate = session.CreatedDate,
                Extension = session.Extension,
                OwnerId = session.OwnerId,
                UploadedChunksCount = session.UploadedChunksCount
            };

            return new(dto);
        }

        private Task<Session> GetSession(GetSessionRequest request, CancellationToken cancellationToken)
        {
            return _context.Sessions.AsNoTracking()
                .FirstOrDefaultAsync(e => e.Id == request.SessionId,
                                     cancellationToken: cancellationToken);
        }
    }
}
