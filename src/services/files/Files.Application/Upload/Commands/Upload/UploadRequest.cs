﻿
using MediatR;

namespace Files.Application.Upload.Commands.Upload
{
    public sealed class UploadRequest : IRequest<UploadResponse>
    {
    }
}
