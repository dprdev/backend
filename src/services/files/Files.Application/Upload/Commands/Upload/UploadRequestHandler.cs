﻿using System;
using System.Threading;
using System.Threading.Tasks;

using Files.Application.Services;

using MediatR;

namespace Files.Application.Upload.Commands.Upload
{
    internal sealed class UploadRequestHandler : IRequestHandler<UploadRequest, UploadResponse>
    {
        private readonly IBlobClient _blobClient;

        public Task<UploadResponse> Handle(
            UploadRequest request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
