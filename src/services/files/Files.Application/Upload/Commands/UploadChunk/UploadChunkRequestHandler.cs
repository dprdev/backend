﻿using System;
using System.Threading;
using System.Threading.Tasks;

using MediatR;

namespace Files.Application.Upload.Commands.UploadChunk
{
    internal sealed class UploadChunkRequestHandler : IRequestHandler<UploadChunkRequest, UploadChunkResponse>
    {
        public Task<UploadChunkResponse> Handle(UploadChunkRequest request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
