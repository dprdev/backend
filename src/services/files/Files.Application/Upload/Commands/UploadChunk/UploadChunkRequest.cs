﻿
using MediatR;

namespace Files.Application.Upload.Commands.UploadChunk
{
    public sealed class UploadChunkRequest : IRequest<UploadChunkResponse>
    {
    }
}
