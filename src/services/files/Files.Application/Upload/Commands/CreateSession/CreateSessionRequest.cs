﻿
using System;

using MediatR;

namespace Files.Application.Upload.Commands.CreateSession
{
    public sealed class CreateSessionRequest : IRequest<CreateSessionResponse>
    {
        public CreateSessionRequest(
            string userId,
            string extension,
            int totalSize,
            int chunksCount,
            int chunkSize)
        {
            if (string.IsNullOrEmpty(userId))
            {
                throw new ArgumentException($"'{nameof(userId)}' cannot be null or empty.", nameof(userId));
            }

            if (string.IsNullOrEmpty(extension))
            {
                throw new ArgumentException($"'{nameof(extension)}' cannot be null or empty.", nameof(extension));
            }

            UserId = userId;
            Extension = extension;
            TotalSize = totalSize;
            ChunksCount = chunksCount;
            ChunkSize = chunkSize;
        }

        public string UserId { get; private set; }
        public string Extension { get; private set; }
        public int TotalSize { get; private set; }
        public int ChunksCount { get; private set; }
        public int ChunkSize { get; private set; }
    }
}
