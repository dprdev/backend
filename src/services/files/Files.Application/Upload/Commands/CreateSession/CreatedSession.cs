﻿using System;

namespace Files.Application.Upload.Commands.CreateSession
{
    public sealed class CreatedSession
    {
        public string Id { get; init; }
        public DateTime CreatedDate { get; init; }
    }
}
