﻿using System;

using OneOf;

namespace Files.Application.Upload.Commands.CreateSession
{
    public sealed class CreateSessionResponse : OneOfBase<CreatedSession, Exception>
    {
        public CreateSessionResponse(OneOf<CreatedSession, Exception> input) : base(input)
        {
        }
    }
}
