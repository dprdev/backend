﻿using System;
using System.Threading;
using System.Threading.Tasks;

using Files.Domain;
using Files.Domain.Entities;

using MediatR;

using Polly;
using Polly.Retry;

using Sentry;

namespace Files.Application.Upload.Commands.CreateSession
{
    internal sealed class CreateSessionRequestHandler
        : IRequestHandler<CreateSessionRequest, CreateSessionResponse>
    {
        private readonly IHub _hub;
        private readonly DataContext _context;

        public CreateSessionRequestHandler(DataContext context, IHub hub)
        {
            if (context is null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (hub is null)
            {
                throw new ArgumentNullException(nameof(hub));
            }

            _context = context;
            _hub = hub;
        }

        public static AsyncRetryPolicy RetryPolicy { get; }
            = Policy.Handle<Exception>()
                    .WaitAndRetryAsync(3, (count) =>
                    {
                        return TimeSpan.FromSeconds(0.5 + count * 0.5);
                    });

        public async Task<CreateSessionResponse> Handle(
            CreateSessionRequest request, CancellationToken cancellationToken)
        {
            Domain.Entities.Session model = _context.Sessions.Add(CreateSessionFromRequest(request)).Entity;

            try
            {
                await RetryPolicy.ExecuteAsync(
                    () => _context.SaveChangesAsync(cancellationToken));
            }
            catch (Exception e) when (Log(e))
            {
                return new CreateSessionResponse(e);
            }

            var input = new CreatedSession
            {
                Id = model.Id,
                CreatedDate = model.CreatedDate
            };

            return new CreateSessionResponse(input);
        }

        private static Domain.Entities.Session CreateSessionFromRequest(CreateSessionRequest request)
        {
            return new(request.UserId, request.Extension, request.TotalSize, request.ChunkSize, request.ChunksCount);
        }

        private bool Log(Exception e)
        {
            _hub.CaptureException(e);
            return true;
        }
    }
}
