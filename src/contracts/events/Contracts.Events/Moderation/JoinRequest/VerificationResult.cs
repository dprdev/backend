﻿namespace Contracts.Events.Moderation.JoinRequest
{
    public enum VerificationResult
    {
        Success,
        Failure
    }
}
