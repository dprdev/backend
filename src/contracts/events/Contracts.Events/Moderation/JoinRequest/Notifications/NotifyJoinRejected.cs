﻿namespace Contracts.Events.Moderation.JoinRequest.Notifications
{
    public class NotifyJoinRejected
    {
        public string Id { get; init; }
    }
}
