﻿namespace Contracts.Events.Moderation.JoinRequest.Notifications
{
    public class NotifyIdentityVerificationRejected
    {
        public string Id { get; init; }
    }
}
