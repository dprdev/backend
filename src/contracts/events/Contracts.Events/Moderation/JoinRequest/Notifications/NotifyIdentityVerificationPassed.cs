﻿namespace Contracts.Events.Moderation.JoinRequest.Notifications
{
    public class NotifyIdentityVerificationPassed
    {
        public string Id { get; init; }
    }
}
