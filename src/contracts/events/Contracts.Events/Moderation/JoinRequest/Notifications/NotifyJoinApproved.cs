﻿namespace Contracts.Events.Moderation.JoinRequest.Notifications
{
    public class NotifyJoinApproved
    {
        public string Id { get; init; }
    }
}
