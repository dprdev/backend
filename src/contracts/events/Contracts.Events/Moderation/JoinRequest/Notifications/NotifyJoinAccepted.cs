﻿namespace Contracts.Events.Moderation.JoinRequest.Notifications
{
    public class NotifyJoinAccepted
    {
        public string Id { get; init; }
    }
}
