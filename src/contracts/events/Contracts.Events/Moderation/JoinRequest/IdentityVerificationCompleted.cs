﻿namespace Contracts.Events.Moderation.JoinRequest
{
    public class IdentityVerificationCompleted
    {
        public string Id { get; init; }
    }
}
