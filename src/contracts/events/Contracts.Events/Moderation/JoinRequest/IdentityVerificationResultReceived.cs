﻿namespace Contracts.Events.Moderation.JoinRequest
{
    public class IdentityVerificationResultReceived
    {
        public string Id { get; init; }
        public VerificationResult Result { get; init; }

        public bool IsSuccess()
        {
            return Result == VerificationResult.Success;
        }
    }
}
