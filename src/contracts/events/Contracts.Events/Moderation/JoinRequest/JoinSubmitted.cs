﻿namespace Contracts.Events.Moderation.JoinRequest
{
    public class JoinSubmitted
    {
        public string Id { get; init; }
        public string Email { get; init; }
        public string Username { get; init; }
        public string Photo { get; init; }
    }
}
