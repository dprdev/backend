﻿namespace Contracts.Events.Moderation.JoinRequest
{
    public class JoinSubmissionAccepted
    {
        public string Id { get; init; }
    }
}
