﻿namespace Contracts.Events.Moderation.JoinRequest
{
    public class JoinApproved
    {
        public string Id { get; init; }
    }
}
