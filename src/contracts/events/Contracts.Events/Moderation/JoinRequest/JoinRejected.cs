﻿namespace Contracts.Events.Moderation.JoinRequest
{
    public class JoinRejected
    {
        public string Id { get; init; }
    }
}
