﻿namespace Contracts.Events.Reservations
{
    public sealed class NotifyLessonCanceled
    {
        public string Lesson { get; init; }
    }
}
