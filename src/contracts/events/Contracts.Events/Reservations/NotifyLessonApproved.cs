﻿namespace Contracts.Events.Reservations
{
    public sealed class NotifyLessonApproved
    {
        public string Lesson { get; init; }
    }
}
