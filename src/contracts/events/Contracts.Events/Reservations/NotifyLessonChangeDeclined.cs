﻿namespace Contracts.Events.Reservations
{
    public sealed class NotifyLessonChangeDeclined
    {
        public string Lesson { get; init; }
    }
}
