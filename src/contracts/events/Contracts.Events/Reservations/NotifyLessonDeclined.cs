﻿namespace Contracts.Events.Reservations
{
    public sealed class NotifyLessonDeclined
    {
        public string Lesson { get; init; }
    }
}
