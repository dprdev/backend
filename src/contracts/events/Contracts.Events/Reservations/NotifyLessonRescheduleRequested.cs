﻿namespace Contracts.Events.Reservations
{
    public sealed class NotifyLessonRescheduleRequested
    {
        public string Lesson { get; init; }
    }
}
