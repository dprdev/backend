﻿namespace Contracts.Events.Reservations
{
    public sealed class NotifyLessonChangeApproved
    {
        public string Lesson { get; init; }
    }
}
