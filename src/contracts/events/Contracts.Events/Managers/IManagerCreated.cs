﻿namespace Contracts.Events.Managers
{
    public interface IManagerCreated
    {
        string Id { get; set; }
        string Email { get; set; }
        string[] Roles { get; set; }
    }
}
