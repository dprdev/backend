﻿namespace Contracts.Events.Managers
{
    public sealed class NotifyManagerCreated
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string[] Roles { get; set; }
    }
}
