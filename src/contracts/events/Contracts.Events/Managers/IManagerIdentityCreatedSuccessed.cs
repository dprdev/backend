﻿namespace Contracts.Events.Managers
{
    public interface IManagerIdentityCreatedSuccessed
    {
        string Id { get; set; }
    }
}
