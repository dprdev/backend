﻿namespace Contracts.Events.Managers
{
    public interface IManagerBlocked
    {
        public string Id { get; set; }
    }
}
