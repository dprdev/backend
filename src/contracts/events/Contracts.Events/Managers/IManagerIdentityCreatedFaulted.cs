﻿namespace Contracts.Events.Managers
{
    public interface IManagerIdentityCreatedFaulted
    {
        string Id { get; set; }
    }
}
