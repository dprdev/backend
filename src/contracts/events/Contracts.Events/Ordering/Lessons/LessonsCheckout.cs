﻿namespace Contracts.Events.Ordering.Lessons
{
    public sealed class LessonsCheckout
    {
        public string OrderId { get; init; } 
    }
}
