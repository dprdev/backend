﻿namespace Contracts.Events.Ordering.Credits
{
    public sealed class CreditsCheckout
    {
        public string OrderId { get; init; } 
    }
}
