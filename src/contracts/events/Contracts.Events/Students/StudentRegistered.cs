﻿namespace Contracts.Events.Students
{
    public class StudentRegistered
    {
        public string Id { get; set; }
        public string Email { get; set; }
    }
}
