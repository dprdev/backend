﻿namespace Contracts.Events.Students.StudentBlocked
{

    public interface ITeacherBlocked
    {
        string Id { get; set; }
        BlockingReason Reason { get; set; }
    }
}
