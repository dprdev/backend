﻿
using Automatonymous;

using GreenPipes;

namespace Contracts.Events.Masstransit
{
    public abstract class BaseActivity : Activity
    {
        public BaseActivity(string scopeName)
        {
            ScopeName = scopeName;
        }

        public string ScopeName { get; }

        public virtual void Accept(StateMachineVisitor visitor)
        {
            visitor.Visit(this);
        }

        public virtual void Probe(ProbeContext context)
        {
            context.CreateScope(ScopeName);
        }
    }
}
