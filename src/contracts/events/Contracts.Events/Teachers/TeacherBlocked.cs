﻿namespace Contracts.Events.Teachers
{

    public class TeacherBlocked
    {
        public string Id { get; init; }
        public BlockingReason Reason { get; init; }
    }
}
