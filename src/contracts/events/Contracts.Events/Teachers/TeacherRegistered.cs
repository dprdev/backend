﻿namespace Contracts.Events.Teachers
{
    public class TeacherRegistered
    {
        public string Id { get; init; }
        public string Email { get; init; }
    }
}
